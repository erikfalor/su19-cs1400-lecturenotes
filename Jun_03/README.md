# CS1400 - Mon Jun 03

# Topics:

* Assignment #9 Recap
* Scope (6.9)
* Direct Debugging in the IDE
* Super Target Demo



----------------------------------------------------------------------------
# Assignment #9 Recap

Discussion of challenges faced in Assignment #9 

----------------------------------------------------------------------------
# Scope (6.9)

Refers to the areas of code where a variable can be used.

So far we know that:

* Variables must be defined before they're used
* Variables are only visible within the function they're created in
* Variables are only visible within the module they're created in
* Two functions/modules may each use the same name for a variable
* Changing a value within a function doesn't affect a variable on the outside
    * "What happens in Vegas, stays in Vegas"


Let's add to this knowlege

## Local variables

* Variables declared within a function
* Are only accessible inside the function
* Scope begins when it is created and ends when the function completes
* *Note:* variables created inside loops and if-else are local to the function

Using the name of a variable whose scope is outside of a function leads to one
of two outcomes:

1. *Reading*: this will create a new local variable of the same name
2. *Writing*: results in `UnboundLocalError`, an error similar to `NameError`


## Global variables

Globals are variables declared outside of any function but which are accessible
to functions within the same file.

Because of the two outcomes described above, you must tell Python when you
intend to access a global variable by using the keyword `global`.  This *binds*
the local name to refer to the same value as the global, effectively making
them one and the same.

Now, changes to the global leave their effect after the function returns.

`src/globals.py`


--------------------------------------------------------------------------------
# Direct Debugging in the IDE

> Everyone knows that debugging is twice as hard as writing a program in the
> first place.  So if you're as clever as you can be when you write it, how
> will you ever debug it?
>
>    -- Brian Kernighan

[Debugging with PyCharm](https://www.jetbrains.com/help/pycharm/2019.1/part-1-debugging-python-code.html)

Use the debugger in PyCharm to walk through your code and observe what's going
on.  I recommend that you spend some time becoming familiar with the debugger
so that you can use it to its full advantage.

The debugger, once mastered, helps you to visualize your data, empowering you
to prove facts about your programs instead of making assumptions.

I have a program with a few problems in .`src/quadratic.py`.  Let's practice
using the debugger on this program and see if it can help us find the bug(s).

1.   Running your code vs. Debugging your code
2.   Setting breakpoints
3.   Inline display of debugging data
4.   Stepping
    *   Stepping over
    *   Stepping into
    *   Stepping into my code
    *   Stepping out of
    *   Run to cursor
5.   Frames of the Call Stack
6.   Watching the value of Varibles
7.   REPL


## Rubber Duck Debugging


			   ,-.
		   ,--' ~.).
		 ,'         `.
		; (((__   __)))
		;  ( (#) ( (#)
		|   \_/___\_/|
	   ,"  ,-'    `__".
	  (   ( ._   ____`.)--._        _
	   `._ `-.`-' \(`-'  _  `-. _,-' `-/`.
		,')   `.`._))  ,' `.   `.  ,','  ;
	  .'   .    `--'  /     ).   `.      ;
	 ;      `-       /     '  )         ;
	 \                       ')       ,'
	  \                     ,'       ;
	   \               `~~~'       ,'
		`.                      _,'
	hjw   `.                ,--'
			`-._________,--'

	http://www.ascii-art.de/ascii/pqr/rubber_duck.txt

https://rubberduckdebugging.com/

https://rubberduckdebugging.com/cyberduck/



----------------------------------------------------------------------------
# Super Target Demo

Let's write a program similar in spirit to [Assignment 10](https://usu.instructure.com/courses/537470/assignments/2672580)
