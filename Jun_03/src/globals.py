variable = 7
print(format("before, variable @", ">24s"), id(variable))

def function():
    # Replace this declaration of the local variable with a global declaration
    #variable = 13
    # Please don't do this!
    global variable
    print(format("within, pre increment @", ">24s"), id(variable))
    variable += 1
    print(format("within, post increment @", ">24s"), id(variable))


function()
print(format("after, variable @", ">24s"), id(variable))
