import math


def firstRoot(a, b, disc):
    return (-b + disc) / (2 * a)  # <======= PLACE A BREAKPOINT HERE


def secondRoot(a, b, disc):
    return (-b - disc) / (2 * a)


def quadratic(a, b, c):
    d = b ** 2 - 4 * a * c
    if d > 0:                     # <======= PLACE A BREAKPOINT HERE
        disc = math.sqrt(d)
        root1 = firstRoot(a, b, disc)
        root2 = secondRoot(a, b, disc)
        return root1, root2
    elif d == 0:
        return firstRoot(a, b, 0)
    else:
        return "This equation has no roots"



while True:
    a = input("Enter numeric term for 'a', or just hit Enter to quit: ")
    if a == "":
        break
    else:
        a = float(a)

    b = input("Enter numeric term for 'b', or just hit Enter to quit: ")
    if b == "":
        break
    else:
        b = float(b)

    c = input("Enter numeric term for 'c', or just hit Enter to quit: ")
    if c == "":
        break
    else:
        c = float(c)

    result = quadratic(a, b, c)   # <======= PLACE A BREAKPOINT HERE
    print(result)
