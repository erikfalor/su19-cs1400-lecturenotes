# CS1400 - Tue Jun 04



# Topics:
* Super Target Demo
* Object-Oriented Programming Overview (7.1)
* Classes and Objects (7.2)
* UML Class Diagrams (7.3)
* Defining Classes (7.2.1)
* Creating Objects: Instances of classes (7.2.2)




----------------------------------------------------------------------------
# Super Target Demo

Let's write a program similar in spirit to [Assignment 10](https://usu.instructure.com/courses/537470/assignments/2672580).

* Canvas size is 1000x800
* Turtle should move at max speed
* Targets rings alternate in color
* The default target has three rings
* The default target has a bullseye with radius of 50px
* "Super Target" mode is to draw *N* random targets all about the screen


I already have a driver program in `src/superTargetMain.py` that gives us clues
about how the Super Target module is to be used.



--------------------------------------------------------------------------------
# Object-Oriented Programming Overview (7.1)

As a tool of abstraction, objects are similar to functions

* They both allow us to reuse code
    - Code reuse is a huge deal
* They both help us to organize code
* They both help us to simplify code

Object-Oriented Programming (OOP) is foundational for modern, large-scale
software development.


## Objects

Objects combine *data* with *behavior* in one variable.  In Python *everything*
is an object.  This means that you have been doing OOP all along!

We have used objects of these types:
* `float`
* `int`
* `str`
* `turtle`


*   An object is a single software entity, tied to an identifier
*   Each object has a type
    -   Find with type()
    -   Many different objects can be the same type
*   Each object has a unique...
    1.   identity
        -   `id()` is different for each object
        -   identifier is different for each, although two identifiers can reference the same object
    2.   state
        -   properties/attributes/data fields
    3.   behavior
        -   functions
        -   capabilities


### Demo: Program
`src/distinctTime.py`



## Functions vs. Methods

*   Objects have capabilities that are implemented as functions
    -  Objects are collections of data that can *do* things
*   Functions that are part of objects are called methods
*   Their implementation is the same, except that a method's first argument has
    a special purpose
*   Usage is the same, except that a method is always invoked from the name of
    its object
*   It's the relationship between this function and its object that makes it a
    method




--------------------------------------------------------------------------------
# Classes and Objects (7.2)

Classes and objects are directly related and are easily confused.  Think of the
relationship between a blueprint and a house built from that blueprint.

## Class
*   A blueprint/template
*   Design for a an object
*   Called the type of the object
*   A class does not do/execute anything
*   A class describes the form that its objects may take


## Object
*   What the class describes
*   Think of it like a widget that was made from a blueprint
*   Called an *instance* of a class


## Example: Circle class vs. circle objects

![Circle Class and Circle objects](Circle_Class.png)


--------------------------------------------------------------------------------
# UML Class Diagrams (7.3)

The Unified Modeling Language is a standard way to graphically communicate
classes in object-oriented programming languages.  In upcoming assignments you
will be required to create a UML diagram describing the classes in your
solution.

![Circle Class UML Diagram](UML_Class_Diagram.png)

You can draw these diagrams in any drawing tool that you please, though it's
helpful to find one which has UML-specific features.

[draw.io](https://www.draw.io) is an online tool that is adequate for simple
UML diagrams.

1. Open draw.io
2. Create a new diagram (where you store it doesn't matter to me)
3. Choose a "Blank" diagram; don't use one of the prepared UML templates
4. In the toolbar on the left of the screen scroll down to and expand the UML
   accordion to see all of the UML tools.  Use the tool labeled "Class", *not*
   the one one labeled "Class2".


## Example: Model a TV set in UML

![UML Diagram for a Television Set](TV_UML.png)



--------------------------------------------------------------------------------
# Defining Classes (7.2.1)

*Defining a class* means to create the blueprint for a piece of software

## Class Naming Convention
*   UpperCamelCase
*   The name should be a Noun (we're going to make a thing)



## Class Syntax
```
class <ClassName>:
    <initializer>

    <methods>
```
*Note:* We'll add variables in a bit



## Modules and classes
Classes are most often delivered as a module of code, one class per module.
However, many relatead classes may be defined within a single module.  It is
bad practice to write unrelated classes in the same module.

*   Create a new file and create a class in it
*   The file is still a module, the class is something in the module
    -   Just like a function was in the module we created before

*   Naming convention for modules
    -   `all_lowercase` (use underscores if they improves readability)
    -   *Note:* page 218 of the book discusses naming conventions. The Python
        Standard Library itself is not consistent, but we will follow PEP8, not
        the book.


## Object Initializer
This is a special method that initializes an object.  It must have a particular
name because the Python Language calls this method for you, automatically.  If
you give it the wrong name, Python won't know what code to run.

Syntax:
```
def __init__(self, <other parameters>):
    <statements>
```

It is a rule to use two underscores before and after the name `init`.  The
Python language uses lots of methods with names of this form.

* These functions are called *dunders*
*   Double underscore
    -   There are other dunders
    -   These are special methods associated with a class that we can define
    -   It is not a good idea for you to make your own methods with *dunder*
        names; a future version of Python may begin using that name and your
        code will break.


Example: `src/circle.py`


--------------------------------------------------------------------------------
# Creating Objects (instances of classes) 7.2.2


Objects are constructed from the blueprint by means of a special function
called the *constructor*.

*   A constructor is a special method that creates an instances of a class
*   This takes the blueprint and makes a new unique object/instance

Syntax:
```
ClassName(<parameters>)
```

* `self` is not part of the actual parameters, only the formal parameters
* Python creates and passes the value of `self` into the constructor for you
* The constructor creates a generic object of the type `ClassName` in memory
* Python then calls `__init__()` for you to setup data that is unique to this
  instance of the class

