import math


class Circle:
    def __init__(self, radius=1, units="in"):
        self.radius = radius
        self.units = units

    def area(self):
        return str(math.pi * self.radius * self.radius) + " " + self.units

    def perimeter(self):
        return str(math.pi * self.radius * 2.0) + " " + self.units



## Create some circles
unitCircle = Circle()
twoCircle = Circle(2, "ft")
bigCircle = Circle(10000000, "ly")


## Use the circles
print("The radius of the unit circle is", unitCircle.radius)
print("The area of the 2 circle is", twoCircle.area())
print("The perimeter of the big circle is", bigCircle.perimeter())

