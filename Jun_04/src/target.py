import random
import turtle


SCREEN_WIDTH = 1000
SCREEN_HEIGHT = 800

def setup():
    """Prepare our turtle/canvas"""
    turtle.screensize(SCREEN_WIDTH, SCREEN_HEIGHT)
    turtle.speed(0)


def drawSuperTarget(num):
    """Draw num random targets"""
    for t in range(num):
        # pick a random location
        centerX = random.randint(0, SCREEN_WIDTH) - (SCREEN_WIDTH / 2)
        centerY = random.randint(0, SCREEN_HEIGHT) - (SCREEN_HEIGHT / 2)
        # pick a random radius
        radius = random.randint(1, 100)
        # pick a random number of rings
        rings = random.randint(1, 17)
        drawTarget(centerX, centerY, radius, rings)



def drawTarget(centerX, centerY, radius, rings):
    """Draw a single target according to the parameters"""
    for ring in range(rings, 0, -1):
        turtle.penup()
        if ring % 2 == 0:
            turtle.color('blue')
        else:
            turtle.color('green')
        bottom = centerY - (radius * ring)
        turtle.goto(centerX, bottom)
        turtle.pendown()
        turtle.begin_fill()
        turtle.circle(radius * ring)
        turtle.end_fill()



def reset():
    """clear the canvas"""
    turtle.reset()
    turtle.speed(0)


def done():
    """Hold the window open for us"""
    turtle.done()
