# CS1400 - Wed Jun 05


# Topics:
* Accessing Members of an Object (7.2.3)
* The 'self' parameter (7.2.4)
* Convert SuperTarget into OOP
* Anonymous Objects
* Instance vs. local variables



--------------------------------------------------------------------------------
# Accessing Members of an Object (7.2.3)

*   Members of an object
    -   Data and methods that are part of the object
*   Instance
    -   Objects are called instances of a specific class
    -   Therefore
        -   Variables
            -   often called instance variables
        -   Methods
            -   often called instance methods
    -   Variables and Methods can only be accessed through the instance (object)
*   Accessing
    -   Dot operator `.`
    -   Aka: access operator
    -   Syntax:
        -   `<Object>.<instance_variable>`
        -   `<Object>.<instance_method>()`

## Example
```
cir1 = Circle(5)
cir1.getPerimeter()
cir1.radius
```


--------------------------------------------------------------------------------
# The 'self' parameter (7.2.4)
The name `self` is used by default, but it is not required.  Anything would
work.  Using the identifier `self` is a convention, not a rule. No good reason
not to

`self` is the way that an object can reference itself. `self` is an object.

Accessing object properties through `self` using the dot operator
```
self.width
self.calcArea()
```

* All instance methods must be defined with `self` as the first parameter
* Assigning to a variable with `self.<variable_name>` creates an *instance*
  variable
    - Now any method in that object can access that variable
    - The value in an *instance* variable is accessible only within that
      *instance* of the object; other objects of that type are unaffected




--------------------------------------------------------------------------------
# Convert SuperTarget into OOP

1. Identify the objects in this system
    * Where will the data need to go?
    * What functionality can be made into methods?


2. Draft a UML diagram describing the classes involved
    * What will you name each class?
    * What data members are needed, and what are their types?
    * What methods are needed, and what will their parameters be?

![Book class UML Diagram](Book_Class_Diagram.png)

3. We'll translate your design into code together



--------------------------------------------------------------------------------
# Anonymous Objects

*Anonymous* objects have no name (identifier).  They are not stored into a variable.  This means they cannot be accessed later

`Circle(5)`

This line of code created an object.  Because it is not assigned to a variable,
it can't be used later.  This line of code is not very useful.


`print(Circle(5).getPerimeter())`

* This uses an anonymous object immediately
* Just like a `return` from a function, the first part is a circle object
* So we can use the dot operator to access its members
    * But we cannot access anything after since it is anonymous



--------------------------------------------------------------------------------
# Instance vs. local variables


*   Local variables have scope only within a function or method
*   They are created the same way we have done it before
    -   `var1 = 5`
*   After the method returns, the local variable is no longer available
*   Instance variables persist because they are made as a member of `self`
    -   `self.var1 = 5`
*   Since self is available to all methods, the var1 variable is as well
*   Instance variables have scope to everything in the object

*Draw this in our memory diagram*

*   Instances are in the heap
*   Locals are in the activation record in the stack


