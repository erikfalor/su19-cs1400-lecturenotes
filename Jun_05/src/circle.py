import math


class Circle:
    def __init__(self, radius=1, units="in"):
        self.radius = radius
        self.units = units

    def getArea(self):
        return str(math.pi * self.radius * self.radius) + " " + self.units

    def getPerimeter(self):
        return str(math.pi * self.radius * 2.0) + " " + self.units

    def getRadius(self):
        return self.radius

    def setRadius(self, radius):
        self.radius = radius



## Use the circles

unitCircle = Circle()

twoCircle = Circle(2, "ft")

bigCircle = Circle(10000000, "ly")

print("The area of the unit circle is", unitCircle.area())
print("The area of the 2 circle is", twoCircle.area())
print("The perimeter of the big circle is", bigCircle.perimeter())

