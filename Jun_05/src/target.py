import random
import turtle


class Application:
    def __init__(self):
        self.SCREEN_WIDTH = 1000
        self.SCREEN_HEIGHT = 800
        turtle.screensize(self.SCREEN_WIDTH, self.SCREEN_HEIGHT)
        turtle.speed(0)

    def screenWidth(self):
        """Accessor for screen width"""
        return self.SCREEN_WIDTH

    def screenHeight(self):
        """Accessor for screen height"""
        return self.SCREEN_HEIGHT

    def reset(self):
        """clear the canvas"""
        turtle.reset()
        turtle.speed(0)

    def done(self):
        """Hold the window open for us"""
        turtle.done()


class SuperTarget:
    def __init__(self, num, app):
        self.num = num
        self.app = app

    def draw(self):
        """Draw num random targets"""
        for i in range(self.num):
            # pick a random location
            centerX = random.randint(0, self.app.screenWidth()) - (self.app.screenWidth() / 2)
            centerY = random.randint(0, self.app.screenHeight()) - (self.app.screenHeight() / 2)
            # pick a random radius
            radius = random.randint(1, 100)
            # pick a random number of rings
            rings = random.randint(1, 17)
            Target(centerX, centerY, radius, rings).draw()


class Target:
    def __init__(self, centerX, centerY, radius=50, rings=3):
        self.centerX = centerX
        self.centerY = centerY
        self.radius = radius
        self.rings = rings

    def draw(self):
        """Draw a single target according to the parameters"""
        for ring in range(self.rings, 0, -1):
            turtle.penup()
            if ring % 2 == 0:
                turtle.color('blue')
            else:
                turtle.color('green')
            bottom = self.centerY - (self.radius * ring)
            turtle.goto(self.centerX, bottom)
            turtle.pendown()
            turtle.begin_fill()
            turtle.circle(self.radius * ring)
            turtle.end_fill()



