# CS1400 - Thu Jun 06

# Announcements

# Topics:
* Mutable vs. Immutable (7.4)
* Hiding Data Fields (7.5)
* Class Abstraction and Encapsulation (7.6)
* Object-Oriented Thinking (7.7)

--------------------------------------------------------------------------------
# Mutable vs. Immutable (7.4)


Identifiers to objects are *references* to the object in memory.
Two identifiers can reference the *same* object.

```
cir1 = Circle(5)
cir2 = cir1
print("cir1 is ", id(cir1))
print("cir2 is ", id(cir2))
cir2.setRadius(10)
print(cir1.getRadius())  # This prints 10 now!
```


* The objects we create with classes are *mutable*, meaning they can change
    -   Different than immutable numbers and strings
* A mutable object used as a parameter can be changed inside a method/function

```
def change(cir):
    cir.setRadius(cir.getRadius() * 2.0)

cir1 = Circle(5)
change(cir1)
print(cir1.getRadius())
```



--------------------------------------------------------------------------------
# Hiding Data Fields (7.5)

"Hiding" data is also called "Data Encapsulation"

Direct access to your objects' data is not usually a good thing to give to other developers

1. Data can be changed in unexpected ways or given invalid values
2. Classes becomes difficult to maintain and vulnerable to bugs in the same way
   that global variables cause problems

*Example:* create a Circle class which can be given an impossible radius

* What is wrong with this Circle?
* If we use the constructor to ensure that the radius value is correct, is
  there any other way to subvert this protection?


## Data hiding in Python

Data fields and methods can be marked as "private".  "Private" means that the
data/method can only be directly accessed from inside the class.

In Python we can make direct access to data fields difficult; the Python
language doesn't truly make fields private.  It changes the name of the field,
but a clever programmer who knows how to use the REPL can figure out how to
find the data.

Other OO programming languages such as Java, C#, and C++ truly protect private
data.


## How to make data (and methods) private in Python


Name the private field or method with two underscores at the front.

```
def __init__(self, radius):
    self.__radius = radius

def __perimeter(self):
    return self.__radius * self.__radius  * math.pi
```

*Important:* do not end the name with two underscores.  That makes the field a
dunder.  Dunders aren't private, and we must avoid dunder names which are
reserved for Python language development. 



## How to access hidden data from without

Provide methods to control access to private variables.  If users shouldn't
even know about a field's existence, don't provide a method.  Otherwise,
provide read-only or restricted read-write access through non-hidden (public)
methods.

### Reading data with a method

These methods are called *getters* or *accessors*.  Getters simply return the
value of a private variable.  The naming convention is to begin these with the
word *get* or *is*, and include the name of the property.

* Non-boolean getter `def getPropertyName(self)`
* Boolean getter `def isPropertyName(self)`

### Changing data with a method

These methods are called *setters* or *mutators*

The naming convention is as above, but incorporate the word *set*.

* `def setPropertyName(self, newValue)`



--------------------------------------------------------------------------------
# Class Abstraction and Encapsulation (7.6)

* Separation of class implementation from the use of a class
* The implementation details are hidden from the user
* The user knows what the result is, but not how it happens
* Related to method/function abstraction and data encapsulation
* A class is like a black box (same as a function)
* Class has a contract (the way it's used)
    -   aka. Interface, api
*   Class implementation is like a black box hidden from the clients Class
    Class Contract (Signatures of public methods and public constants) Clients
    use the class through the contract of the class 


--------------------------------------------------------------------------------
# Object-Oriented Thinking (7.7)

*   Procedural Programming
    -   Focus on functions
    -   Data and functions are separate
    -   We send data to functions to perform operations

*   Object Oriented Programming
    -   Couples data and methods(functions) together
    -   Information and behavior is in a single entity (object)
    -   Focus on objects and operations on objects
