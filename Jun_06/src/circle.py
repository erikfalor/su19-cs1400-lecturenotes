import math

VERSION = "1.0"

class Circle:
    def __init__(self, radius=1, units="in"):
        if radius <= 0:
            print("You can't give a circle a radius of", radius)
            print("Creating a unit circle instead")
            self.__radius = 1
        else:
            self.__radius = radius
        self.units = units

    def getArea(self):
        """Return the computed area"""
        return str(math.pi * self.__radius * self.__radius) + " " + self.units

    def getPerimeter(self):
        """Return the computed perimeter"""
        return str(math.pi * self.__radius * 2.0) + " " + self.units

    def getRadius(self):
        """Getter for the circle's __radius field"""
        return self.__radius

    def setRadius(self, radius):
        """
        Setter for the circle's __radius field
        Prevent the user from setting an impossible radius value
        """
        if radius > 0:
            self.__radius = radius
        else:
            print("You can't give a circle a radius of", radius)
