from circle import Circle, VERSION

print("You are using Circle version", VERSION)

cir1 = Circle(5)
cir2 = cir1

print("cir1 is ", id(cir1))
print("cir2 is ", id(cir2))
cir2.setRadius(10)
print(cir1.getRadius())  # This prints 10 now!


badCircle = Circle(-10)

badCircle.setRadius(0)
badCircle.__radius = 0

print("\nThe bad circle is:")
print(badCircle.getRadius())
print(badCircle.getPerimeter())

