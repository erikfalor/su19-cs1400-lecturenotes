# Assn10
# This is the main function of Assn10, importing functions from pattern.py

from pattern import Game, Circle, Rectangle


def main():
    # Setup pattern
    g = Game()

    # Play again loop
    playAgain = True

    while playAgain:
        # Present a menu to the user
        # Let them select 'Super' mode or 'Single' mode
        print("Choose a mode")
        print("1) Rectangle Pattern")
        print("2) Circle Pattern")
        print("3) Super Pattern")
        mode = eval(input("Which mode do you want to play? 1, 2 or 3: "))

        # If they choose 'Rectangle Patterns'
        if mode == 1:
            #### Add Input Statement(s) as needed ####
            centerX = int(input("Enter your centerX coordinate: "))
            centerY = int(input("Enter your centerY coordinate: "))
            offset = int(input("Enter the amount of offset from center: "))
            width = int(input("Enter the width of the rectangles: "))
            height = int(input("Enter the height of the rectangles: "))
            count = int(input("Enter the number of rectangles in the pattern (count): "))
            rotation = int(input("Enter the amount of rotation for the rectangles: "))
            #### End Add Inputs Statement(s) ####

            # Draw the rectangle pattern
            r = Rectangle(centerX, centerY, offset, width, height, count)
            r.draw()

        # If they choose 'Circle Patterns'
        elif mode == 2:
            #### Add Input Statement(s) as needed ####
            centerX = int(input("Enter your centerX coordinate: "))
            centerY = int(input("Enter your centerY coordinate: "))
            offset = int(input("Enter the amount of offset from center: "))
            radius = int(input("Enter the radius of the circles: "))
            count = int(input("Enter the number of circles in the pattern (count): "))
            #### End Add Inputs Statement(s) ####

            # Draw the circle pattern
            c = Circle(centerX, centerY, offset, radius, count)
            c.draw()

        # If they choose 'Super Patterns'
        elif mode == 3:
            #### Add Input Statement(s) as needed ####
            num = str(input("Enter the number of patters you want to draw: "))
            #### End Add Inputs Statement(s) ####
            if num == "":
                g.drawSuperPattern()
            else:
                g.drawSuperPattern(eval(num))

        # Play again?
        print("Do you want to play again?")
        print("1) Yes, and keep drawings")
        print("2) Yes, and clear drawings")
        print("3) No, I am all done")
        response = eval(input("Choose 1, 2, or 3: "))

        #### Add Statement(s) to clear drawings and play again ####
        if response == 1:
            playAgain = True
        elif response == 2:
            g.reset()
            playAgain = True
        elif response == 3:
            playAgain = False

        #### End Add Inputs Statement(s) ####

    # print a message saying thank you
            print("Thanks for playing!")
            g.done()


main()
