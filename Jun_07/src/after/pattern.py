# Assn10
# This is imported module for Assn10

import turtle
from random import randint


class Screen:
    def __init__(self, width=1000, height=800):
        self.__width  = width
        self.__height = width
        turtle.speed(0)
        turtle.setup(self.__width, self.__height)

    def reset(self):
        turtle.reset()
        turtle.speed(0)

    def getWidth(self):
        return self.__width

    def getHeight(self):
        return self.__height


class Game:
    def __init__(self):
        self.__screen = Screen()

    def drawSuperPattern(self, num=3):
        circlePatterns = randint(0, num)
        rectanglePatterns = (num - circlePatterns)

        for r in range(0, rectanglePatterns):
            centerX = randint(0, self.__screen.getWidth()) - self.__screen.getWidth() / 2
            centerY = randint(0, self.__screen.getHeight()) - self.__screen.getHeight() / 2
            offset = randint(-100, 100)
            width = randint(1, 100)
            height = randint(1, 100)
            count = randint(1, 100)

            r = Rectangle(centerX, centerY, offset, width, height, count)
            r.draw()

        for c in range(0, circlePatterns):
            centerX = randint(0, self.__screen.getWidth()) - self.__screen.getWidth() / 2
            centerY = randint(0, self.__screen.getHeight()) - self.__screen.getHeight() / 2
            offset = randint(-100, 100)
            radius = randint(1, 100)
            count = randint(1, 100)
            c = Circle(centerX, centerY, offset, radius, count)
            c.draw()

    def done(self):
        turtle.done()


class Rectangle:
    def __init__(self, centerX, centerY, offset, width, height, count):
        self.__centerX = centerX
        self.__centerY = centerY
        self.__offset = offset
        self.__width = width
        self.__height = height
        self.__count = count

    def drawRectangle(self):
        self.setRandomColor()
        turtle.pendown()
        for i in range(1, 3):
            turtle.forward(self.__width)
            turtle.left(90)
            turtle.forward(self.__height)
            turtle.left(90)

    def draw(self):
        rotateRectangle = (360 / self.__count)
        angleRect = 0.0
        for i in range(0, self.__count):
            turtle.penup()
            turtle.goto(self.__centerX, self.__centerY)
            turtle.setheading(angleRect)
            turtle.forward(self.__offset)
            turtle.left(angleRect)
            self.drawRectangle()
            angleRect += rotateRectangle

    def setRandomColor(self):
        color = randint(1, 5)
        if color == 1:
            color = 'red'
        elif color == 2:
            color = 'green'
        elif color == 3:
            color = 'yellow'
        elif color == 4:
            color = 'blue'
        elif color == 5:
            color = 'purple'
        turtle.color(color)




class Circle:
    def __init__(self, centerX, centerY, offset, radius, count):
        self.__centerX = centerX
        self.__centerY = centerY
        self.__offset = offset
        self.__radius = radius
        self.__count = count

    def drawCircle(self):
        self.setRandomColor()
        turtle.pendown()
        turtle.circle(self.__radius)


    def draw(self):
        rotateCircle = (360 / self.__count)
        angleCircle = 0.0
        for i in range(0, self.__count):
            turtle.penup()
            turtle.goto(self.__centerX, self.__centerY)
            turtle.setheading(angleCircle)
            turtle.forward(self.__offset)
            turtle.right(90)
            self.drawCircle()
            angleCircle += rotateCircle

    def setRandomColor(self):
        color = randint(1, 5)
        if color == 1:
            color = 'red'
        elif color == 2:
            color = 'green'
        elif color == 3:
            color = 'yellow'
        elif color == 4:
            color = 'blue'
        elif color == 5:
            color = 'purple'
        turtle.color(color)



