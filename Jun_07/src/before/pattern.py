# Assn10
# This is imported module for Assn10


def setup():
    import turtle
    turtle.speed(0)
    turtle.setup(1000, 800)


def reset():
    import turtle
    turtle.reset()
    turtle.speed(0)

def done():
    import turtle
    turtle.done()


def setRandomColor():
    import turtle
    from random import randint
    color = randint(1, 5)
    if color == 1:
        color = 'red'
    elif color == 2:
        color = 'green'
    elif color == 3:
        color = 'yellow'
    elif color == 4:
        color = 'blue'
    elif color == 5:
        color = 'purple'
    turtle.color(color)

def drawRectangle(width, height):
    import turtle
    from random import randint
    setRandomColor()
    turtle.pendown()
    for i in range(1, 3):
        turtle.forward(width)
        turtle.left(90)
        turtle.forward(height)
        turtle.left(90)


def drawCircle(radius):
    import turtle
    from random import randint
    setRandomColor()
    turtle.pendown()
    turtle.circle(radius)


def drawRectanglePattern(centerX, centerY, offset, width, height, count, rotation=0):
    import turtle
    rotateRectangle = (360 / count)
    angleRect = rotateRectangle
    print(angleRect)
    for i in range(0, count):
        turtle.penup()
        turtle.goto(centerX, centerY)
        turtle.setheading(0)
        turtle.setheading(angleRect)
        turtle.forward(offset)
        turtle.left(rotation)
        drawRectangle(width, height)
        angleRect += rotateRectangle


def drawCirclePattern(centerX, centerY, offset, radius, count):
    import turtle
    rotateCircle = (360 / count)
    angleCircle = rotateCircle
    print(angleCircle)
    for i in range(0, count):
        turtle.penup()
        turtle.goto(centerX, centerY)
        turtle.setheading(0)
        turtle.setheading(angleCircle)
        turtle.forward(offset)
        turtle.right(90)
        drawCircle(radius)
        angleCircle += rotateCircle


def drawSuperPattern(num = 3):
    import turtle
    from random import randint
    circlePatterns = randint(0, num)
    rectanglePatterns = (num - circlePatterns)
    for r in range(0, rectanglePatterns):
        drawRectanglePattern(centerX=randint(-300, 300), centerY=randint(-200, 200),
                             offset=randint(-100, 100), width=randint(1, 100), height=randint(1, 100),
                             count=randint(1, 100), rotation=randint(1, 360))

    for c in range(0, circlePatterns):
        drawCirclePattern(centerX=randint(-300, 300), centerY=randint(-200, 200), offset=randint(-100, 100),
                          radius=randint(1, 100), count=randint(1, 100))
