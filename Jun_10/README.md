# CS1400 - Mon Jun 10

# Topics:
* Creating Strings (8.2.1)
* Functions on Strings (8.2.2)
* String Operators
* Comparing Strings (8.2.7)
* Iterating over a String (8.2.8)
* Testing Strings (8.2.9)
* Searching for Substrings (8.2.10)


--------------------------------------------------------------------------------
# Creating Strings (8.2.1)

Strings are objects, so we can create them like objects

```
s1 = str("Hello")
s2 = str()
```

These are the same as creating with string literals
```
s1 = "Hello"
s2 = ""
```


## Immutability and Interning
* Strings cannot change; they are *immutable*
* Immutability gives the Python interpreter good memory management

*Interning* happens when two or more immutable objects with same content are
actually a single immutable object that is shared by all associated
identifiers.

Python maintains an *interning list* which keeps track of interned objects.
Accessing a variable returns the reference value of the existing of object, or
creates a new one if it doesn't exist.  This prevents the interpreter from
making several copies of identical values, reducing memory use.

```
s1 = "Hello"
s2 = "Hello"
print(id(s1) == id(s2))  # prints `True` in Python 3.7.2



s1 = "A longer string"
s2 = "A longer string"
print(id(s1) == id(s2))  # prints `False` in Python 3.7.2
```


### When does Python intern values?
* There are rules in the interpreter to decide when to intern
    - It may or may not happen (although it usually does)
    - Interning usually happens at compile time instead of runtime
    - Depending upon which version of Python you're running, your results will vary 

* We need to be aware so we can compare values the correct way
    - The `==` operator compares the *values*
    - The `is` operator compares the *address* the value is stored at
        - `x is y` is shorthand for `id(x) == id(y)`
        - Comparing addresses of strings is *much* faster than comparing contents
    
* The behavior of interning is encapsulated within the Python interpreter
    - We don't really need to know what's going on as a developer
    - As a computer scientist, we should learn what's going on and why
    - Interning is all about maximizing performance


**Demo:** `src/comparison.py`


--------------------------------------------------------------------------------
# Functions on Strings (8.2.2)

*   There are many built-in functions that we can use on strings
*   A string is the parameter for the function
    * `len(string)` - returns how many characters there are in the string
    * `max(string)` - returns the highest ASCII value character
    * `min(string)` - returns the lowest ASCII value character

```
s1 = "Hello World"
print(len(s1))       # => 11
print(max(s1))       # => 'r'
print(min(s1))       # => ' ' (the space character)
```


--------------------------------------------------------------------------------
# String Operators

These are the operators with behavior defined for string operands:


* `+` Concatenation operator (8.2.5)

* `*` Repeat operator (8.2.5)
    - Needs one string operand and one integer operand
    - It repeats the string some number of times
    - `"Hi" * 3` results in `"HiHiHi"`

* `[ ]` Index operator (8.2.3)
    - Needs one string operand and one integer operand
    - Gives the character at a certain position in the string
    - Python uses 0-based indexing; this means the first character is at index 0
    - The index `len(string) - 1` is the last character
    - Supplying a negative index selects characters counting from the *end*

```
s1 = "Welcome"
print(s1[2])   # => 'l'
print(s1[-2])  # => 'm'
```

You cannot change an individual character this way because strings are immutable.

```
s1 = "Welcome"
s1[1] = "Q"  # => TypeError: 'str' object does not support item assignment
```


* `[ : ]` Slicing operator (8.2.4)
    - Syntax
        -   `<string>[ start : end ]`
    - Takes one string operand and zero, one or two integer operands
    - The *slice* operator looks like the subscript operator, except it also contains a colon `:`
    - Returns a copy of the characters in the string from indices `start` to `end-1`
        - When this copy is smaller than the original string we call it a *substring*
    -   You can leave off `start` and `end`
        -   Empty start
            -   `<string>[ : end]`
            -   Same as `<string>[ 0 : end ]`
        -   Empty end
            -   `<string>[ start : ]`
            -   Same as `<string>[ start : len(s) ]`
    -   Negative indices work the same as before
    -   When `start` is > `end` this returns the empty string

```
s1 = "Welcome"
print(s1[1:4])  # => "elc"
print(s1[4:1])  # => ""
```



* `in` substring detection operator (8.2.6)
    -   Takes two string operands
    -   Checks to see if one string is contained within another string
```
s1 = "Welcome"
print("e" in s1)   # => True
print("x" in s1)   # => False
print("elc" in s1) # => True
```

* `not in` (8.2.6)
    -   Takes two string operands
    -   Negates `in`, determines that a substring is not contained within a string
```
s1 = "Welcome"
print("e" not in s1)   # => False
print("x" not in s1)   # => True
print("elc" not in s1) # => False
```


--------------------------------------------------------------------------------
# Comparing Strings (8.2.7)

*   `==`
    -   Compares the contents of strings
    -   It does not compare the id (Note: Java is different!)
    -   With interning == and id() == id() is normally the same, but might not be
*   `<`, `<=`, `>`, `>=`, `!=`
    -   These work too!
    -   Compares strings alphabetically by ASCII value order
        - If the first characters are the same, then compare 2nd character, etc.
        - ASCII order is `0-9 < A-Z < a-z`, but with punctuation characters
          sprinkled throughout
*   `is`
    -   This is another way to compare objects
    -   It compares the `id()`
    -   It shouldn't be used normally
    -   Once exception is None
    -   None is when an object doesn't exist.
    -   There is only one None object, so all Nones are the same `id()`

Example:
```
if myFunction() is None:
    print("Sorry, myFunction() has failed you :(")
else:
    # continue as normal...
```


--------------------------------------------------------------------------------
# Iterating over a String (8.2.8)

Strings are iterable.  This means we can iteratate (go one by one) over a
string a character at a time using a `for` loop, just as we were able to
iterate over each number in a `range()`.

At each iteration of the loop, the loop index variable contains one letter of
the string.

```
# print each character in "Welcome" on its own line
s1 = "Welcome"
for ch in s1:
    print(ch)
```



--------------------------------------------------------------------------------
# Testing Strings (8.2.9)

## Predicate
A function or method which returns either `True` or `False`

There are a number of predicates in the `str` class which answer questions
about the characters contained within a string.  Every single character in the
string must pass the test for these methods to return `True`.  In other words,
if a single character in the string fails the predicate, the entire string
fails.

*   `str.isalnum()`
    -  `str` is alpha numeric (i.e. 0-9A-Za-z)
*   `str.isalpha()`
    -  `str` is alphabetic (i.e. A-Za-z)
*   `str.isdigit()`
    -  `str` is only made of digits (no other characters)
*   `str.isidentifier()`
    -  `str` is the string is a valid Python identifier?
*   `str.islower()`
    -  are all characters in the string lowercase letters?
*   `str.isupper()`
    -  are all characters in the string uppercase letters?
*   `str.isspace()`
    -  are all characters in the string whitespace? (i.e. `" "`, `"\n"`,  `"\t"`


--------------------------------------------------------------------------------
# Searching for Substrings (8.2.10)

There are methods in `str` that allow us to search it

*   `str.endswith(s1)`
    -   `True` if string ends with `s1`
*   `str.startswith(s1)`
    -   `True` if string starts with `s1`
*   `str.find(s1)`
    -   Returns an integer: the lowest index number where s1 starts. `-1` if not found
*   `str.rfind(s1)`
    -   Returns an integer: the highest index number where s1 starts. `-1` if not found
*   `str.count(s1)`
    -   Returns the number of non-overlapping occurrences of `s1`
