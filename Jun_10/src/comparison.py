def comparison(explanation, left, right):
    print(explanation)
    print(format(str(left == right), "<6s"), "Does left == right?")
    print(format(str(id(left) == id(right)), "<6s"), "Does id(left) == id(right)?")
    print(format(str(left is right), "<6s"), "left is right?")
    print("\n")


str1 = "Hello"
str2 = "Hello"
comparison("Comparing two copies of literal 'Hello'", str1, str2)


str1 = "Hello" * 819
str2 = "Hello" * 819
comparison("Comparing two copies of literal 'Hello' * 819", str1, str2)

str1 = "Hello" * 820
str2 = "Hello" * 820
comparison("Comparing two copies of literal 'Hello' * 820", str1, str2)

f = 5
str1 = "Hello" * f
str2 = "Hello" * f
comparison("Comparing two copies of literal 'Hello' * 5, where '5' is a variable", str1, str2)


num1 = int('256')
num2 = int('256')
comparison("Comparing two integers created from int('256')", num1, num2)


num1 = int('257')
num2 = int('257')
comparison("Comparing two integers created from int('257')", num1, num2)
