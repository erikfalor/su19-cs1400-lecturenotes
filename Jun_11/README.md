# CS1400 - Tue Jun 11


# Topics:
* Palindrome detector
* Operator Overloading & Special Methods (8.5)
* Case Study: The Rational Class (8.6)



--------------------------------------------------------------------------------
# Palindrome detector

1. Write a function that can detect whether a string is a Palindrome.  A
   Palindrome is a string which reads the same forward and backward (e.g.
   "level", "civic", "tacocat")
2. Create a class around this function á là our Password Strength Checker from
   Assignment #12
3. Use the built-in functionality of the `str` class to help our Palindrome
   class recognize this string as a Palindrome:
   `"A man, a plan, a canal, Panama"`


--------------------------------------------------------------------------------
# Operator Overloading & Special Methods (8.5)

*   Operator Overloading means we customize the meaning of an operator
*   `+` is an overloaded operator
    -   It really means add
    -   For strings it's defined to mean concatenate
*   Python allows us to overload operators by defining them for an object


## Dunders
*   Short for *D*ouble *UNDER*score
*   Aka. "Magic" methods
*   We can redefine dunder methods to overload operators
*   __init__ is the first dunder we've used
*   Remember, even though they begin with `__` dunders are *not* private
*   Dunder to operator mapping
    -   There are dunders that map to an operator
    -   This means that we can define a dunder and it will be called when an operator is used
    -   Doing so overloads (redefines) the operator
    -   This is done in a class, so the operator is overloaded for an object of the class type



### Common points of confusion with operator overloading
*   Overloading only changes the meaning of the operator for *this* operand
    type; other uses of the operator are unaffected
    -   `+` still adds numbers
    -   `+` still concatenates strings
*   Only need to define these if we want to use them
    -   Not all objects need/can use overloaded operators



## Dunders for Special Functions

Yesterday we talked about how Python's `len()` builtin function would be a
method in other programming languages.  It turns out that Python has it both
ways: the `len()` builtin function simply calls a method on the object you're
querying the length of.


Special Function | Dunder Method    | Description
-----------------|------------------|------------
`len()`          | `__len__(self)`  | number of elements *Note: This is called when the object is the parameter of the `len()` function*
`str()`          | `__str__(self)`  | string representation *Note: This is called automatically with the object name. ex: print(obj)*
`repr()`         | `__repr__(self)` | "Official" string representation of an object.  Also used if `__str__()` is not defined


**TODO:** Enhance the `Palindrome` class to respond to the `len()` and `str()`
builtin functions.


## Dunders for Operators

Operator  | Dunder Method | Description
----------|---------------|------------
`+`       | `__add__(self, other)`      | addition
`*`       | `__mul__(self, other)`      | multiplication
`-`       | `__sub__(self, other)`      | subtraction 
`/`       | `__truediv__(self, other)`  | division
`//`      | `__floordiv__(self, other)` | floor division
`%`       | `__mod__(self, other)`      | remainder
`<`       | `__lt__(self, other)`       | less than
`<=`      | `__le__(self, other)`       | less than or equal to
`>`       | `__gt__(self, other)`       | greater than
`>=`      | `__ge__(self, other)`       | greater than or equal to
`==`      | `__eq__(self, other)`       | equal to
`!=`      | `__ne__(self, other)`       | not equal to
`[index]` | `__getitem__(self, index)`  | index operator
`in`      | `__contains__(self, value)` | check membership




## Using Overloaded Operators

1. Define the dunder in a class to overload that operator
2. For the object of the class type the dunder method will be called when an operator is used
3. `other` parameter is the object that comes after the operator
4. `value` parameter is the object that comes before the operator
5. `index` parameter is inside the `[ ]`
6. Return the value from the dunder that should be the result of the operator

**Example:** See `src/dunders.py`


--------------------------------------------------------------------------------
# Case Study: The Rational Class (8.6)

As an example of overloaded operators in action see `src/TestRationalClass.py`
and `src/Rational.py`
