# make a predicate that detects when its input is a palindrome

class Palindrome:
    def __init__(self, s):
        self.__original = s
        self.__massage()

    def __massage(self):
        self.__massaged = self.__original.lower().replace(',', '').replace(' ', '')

    def getString(self):
        return self.__original

    def __str__(self):
        if self.isPalindrome():
            return self.getString() + " is a palindrome"
        else:
            return self.getString() + " is NOT a palindrome"

    def isPalindrome(self):
        """Input data is a string such as 'tacocat'"""

        # Initialize our pointers
        left = 0
        right = len(self.__massaged) - 1
        while left < right:
            if self.__massaged[left] != self.__massaged[right]:
                return False
            left += 1
            right -= 1
        return True


p = Palindrome("A man, a plan, a canal, Panama")
print(p)


print(Palindrome("tacocat"))
print(Palindrome("This is not a palindrome!"))