# CS1400 - Wed Jun 12

# Topics:

* List Overview 10.1
* Creating a list 10.2.1
* List Methods 10.2.10


--------------------------------------------------------------------------------
# List Overview 10.1

*   Store many pieces of data within a single variable
*   This collection of data may be any size
*   A list is an object with capabilities in the form of methods

## Example Problem

For a set of numbers we want to

1. Print their sum
2. Calculate their average
3. Report the highest value
4. Report the lowest value


--------------------------------------------------------------------------------
# Creating a list 10.2.1

Using the `list()` constructor:

```
# An empty list
list1 = list()


# Create a list with the numbers 5 - 15
list2 = list([5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])

# Create a list with the numbers 5 - 15 from a range
list3 = list(range(5, 16))

# Notice: the call to list() surrounding a call to range() in the example above

# This next example looks like it should create a list, but it actually results
# in a 'range' object.  While the range object will work like a list of numbers
# in many contexts, understand that it's actually a different thing.
range1 = range(5, 16)

# Create a list containing strings
list4 = ["apple", "banana", "cantaloupe"]

# Create a list of letters from a string.
# This list contains the letters "A", "B", "C", "D"
list5 = list("ABCD")
```

Using syntax `[ ]`

```
# An empty list
list6 = []


# Create a list with the numbers 5 - 15
list7 = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]


# Lists may contain data of different types
list8 = [1, "apple", 15.2, True, None]
```

* We access individual items from the list using the `[]` operator with an
  integer index.
* Like strings the first item is at index `0`.
* As with strings, negative indexes access items counting backward from the end of the list
* We can take slices of lists with the slice operator `[ : ]`
* Unlike strings, lists are *mutable*


```
# An empty list
list6 = []
```
This is an empty list.  There is nothing to access and no index is valid to use on it.


## Lists and operators

`+` concatenates two lists together

```
list9 = [0, 1, 2] + list("abc")
```
    

`*` repeat operator creates a list consisting of N copies of a list concatenated together

```
# A list of 10 None's
list10 = [None] * 10

# A list of 20 1's and 0's
list11 = [0, 1] * 10
```



## Memory
-   Draw list in memory
-   It's a list of references to objects in memory



--------------------------------------------------------------------------------
# List Methods 10.2.10

*   `list.append(object)`
    -   Add object to the end of the list

*   `list.count(object)`
    -   Returns an int of the number of times the object appears in the list

*   `list.extend(list)`
    -   Appends the parameter list to the calling list

*   `list.index(object)`
    -   Returns the index value of the first occurrence of the object in the list

*   `list.insert(int,` object)
    -   Inserts the object at the int position in the list

*   `list.pop(int)`
    -   Removes the element at the int position and returns it
    -   If no value is given it pops the last element

*   `list.remove(object)`
    -   Removes the first occurrence of the object in the list
    -   No return value

*   `list.reverse()`
    -   Reverses the order of the list
    -   Modifies the list in-place
    -   Does not return a new list

*   `list.sort()`
    -   Sorts the list in ascending order
    -   Modifies the list in-place
    -   Does not return a new list
    -   The list needs to be sortable, and comparison operators for items
        contained in the list must be overloaded
