# a set of numbers

n0 = -2
n1 = -23
n2 = 65
n3 = 71
n4 = 32
n5 = -96
n6 = -46
n7 = 48
n8 = 66
n9 = -58
n10 = 44
n11 = -35
n12 = 15
n13 = 92
n14 = 58


# Print their sum
sumTotal = n0 + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + n10 + n11 + n12 + n13 + n14
print("the sum total of my numbers is", sumTotal)

# Calculate their average
average = sumTotal / 15
print("the average of my numbers is", average)

# Report the highest value
highest = n0

if n1 > highest:
    highest = n1

if n2 > highest:
    highest = n2

if n3 > highest:
    highest = n3

if n4 > highest:
    highest = n4

if n5 > highest:
    highest = n5

if n6 > highest:
    highest = n6

if n7 > highest:
    highest = n7

if n8 > highest:
    highest = n8

if n9 > highest:
    highest = n9

if n10 > highest:
    highest = n10

if n11 > highest:
    highest = n11

if n12 > highest:
    highest = n12

if n13 > highest:
    highest = n13

if n14 > highest:
    highest = n14

print("The highest number is", highest)



# Report the lowest value
lowest = n0

if n1 < lowest:
    lowest = n1

if n2 < lowest:
    lowest = n2

if n3 < lowest:
    lowest = n3

if n4 < lowest:
    lowest = n4

if n5 < lowest:
    lowest = n5

if n6 < lowest:
    lowest = n6

if n7 < lowest:
    lowest = n7

if n8 < lowest:
    lowest = n8

if n9 < lowest:
    lowest = n9

if n10 < lowest:
    lowest = n10

if n11 < lowest:
    lowest = n11

if n12 < lowest:
    lowest = n12

if n13 < lowest:
    lowest = n13

if n14 < lowest:
    lowest = n14

print("The lowest number is", lowest)
