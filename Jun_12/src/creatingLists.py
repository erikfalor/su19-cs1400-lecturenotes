# An empty list
list1 = list()


# Create a list with the numbers 5 - 15
list2 = list([5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])


# Create a list with the numbers 5 - 15 from a range
list3 = list(range(5, 16))

# Notice: the call to list() surrounding a call to range() in the example above

# This next example looks like it should create a list, but it actually results
# in a 'range' object.  While the range object will work like a list of numbers
# in many contexts, understand that it's actually a different thing.
range1 = range(5, 16)


# Create a list containing strings
list4 = ["apple", "banana", "cantaloupe"]


# Create a list of letters from a string.
# This list contains the letters "A", "B", "C", "D"
list5 = list("ABCD")


# An empty list
list6 = []


# Create a list with the numbers 5 - 15
list7 = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]


# Lists may contain data of different types
list8 = [1, "apple", 15.2, True, None]


# + concatenates two lists together
list9 = [0, 1, 2] + list("abc")


# * repeat operator creates a list consisting of N copies of a list concatenated together
# A list of 10 None's
list10 = [None] * 10


# A list of 20 1's and 0's
list11 = [0, 1] * 10
