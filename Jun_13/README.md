# CS1400 - Thu Jun 13

# Announcements:

## The last lecture is next Thursday

We will review the final exam that day.


## Final Exam

It has been pointed out to me that the Testing Center is not open on Saturdays
during the summer and that it closes early on Friday, at 5pm.

I have adjusted the dates for the final exam to Thu Jun 20 - Fri June 21



# Topics:
* Assignment #13 Overview
* Sequence Type 10.2.2
* Index operator 10.2.4
* Slice operator 10.2.5
* in and not in 10.2.6
* Traversing a list 10.2.7
* Comparisons 10.2.8
* Split method 10.2.11
* Coding Challenges


--------------------------------------------------------------------------------
# Assignment #13 Overview

[Assignment 13](https://usu.instructure.com/courses/537470/assignments/2672583)

## Task 1: Polygon class

The overloaded operators deal with the number of sides that the Polygon possesses.

* `len()` returns the number of sides of that polygon 
* `>`, `<` and `==` compare numbers of sides
* `+` and `-` result in new Polygons whose number of sides is derived from
  those of the operands


## Task 2: Random number count

*Hint:* If you find a bug in this program use `random.seed()` while testing to
help you to know when you've fixed it

*Hint:* What should the total sum of the counts come out to?


## Task 3: Draw a list of shapes

*Hint:* Consider all of the handy list methods we've studied this week.  During
the System Design phase make a note of which methods will be helpful at each
step of your algorithm



--------------------------------------------------------------------------------
# Sequence Type 10.2.2

*   A string is a sequence type
*   A list is a sequence type
*   Broadly speaking they both support the same operations
*   Methods & operators for sequences
    -   `in`
    -   `not in`
    -   `+` (concatenation)
    -   `*` (repetition)
    -   `seq[index]`
    -   `seq[start:end]`
    -   `seq[start:end:step]`
    -   `len(seq)`
    -   `min(seq)`
    -   `max(seq)`
    -   `for` loop
    -   `sum(seq)`
        -   This is a new one
        -   Works for lists of numbers
    -   `random.shuffle(list)`
        -   Shuffles the elements in the list
    -   Lexicographic comparisons: `<`, `<=`, `==`, `!=`, `>`, `>=`


--------------------------------------------------------------------------------
# Index operator 10.2.4

*   `seq[index]`
*   Works the same as for a string
*   Indices are 0 based
    -   From 0
    -   To len(s) - 1
*   s[index] can be used just like a variable
*   Similar to a return value from a function


--------------------------------------------------------------------------------
# Slice operator 10.2.5

This operator works identically on lists and strings

*   `seq[a:b:c]`
*   Returns from `a` to `b-1` with an *optional* step of `c`



The one-line Palindrome Detector:

```
seq == seq[::-1]
```


--------------------------------------------------------------------------------
# in and not in 10.2.6

*   Returns `True` if an element is in the list
*   Returns `False` if an element is not in the list

```
list1 = [1, 3, 5, 7]
print(2 in list) # => False
print(1 in list) # => True
```


--------------------------------------------------------------------------------
# Traversing a list 10.2.7

Use a `for` loop.
There are two approaches you might take depending on your needs

**1.** No need to modify data or access other elements in the list;  iterate
over each item in the list

```
for i in list:
    do something with i
```

**2.**   Need to modify data or access other elements within the list; iterate
over the indices of the list

```
for i in range(len(list)):
    print(list[i])                   # accesses data
    list[i] = list[i] + 3            # change data
    print(list[i - 1] == list[i])    # access or change data in another element
```


--------------------------------------------------------------------------------
# Comparisons 10.2.8

*   `<`, `<=`, `==`, `!=`, `>`, `>=`
*   Just like comparing strings alphabetically
    -   Compares the first items
    -   If same compares 2nd items
*   Lexicographic (a.k.a. alphabetical) ordering imposes an ordering upon
    sequences based upon the ordering of the constituent elements
    -   Comparisons between two lists happens element-by-element from front to
        back
*   Each pair of elements in the same position of the list must be comparable
    -   Must have operators overloaded to do comparisons


## Lexicographic Comparison Example
```
# This works
l1 = [0, 'one', True]
l2 = [0, 'one', False]

l1 < l2  # => False

# Crash and burn: 
l1 = [0, 1, 2]
l2 = [0, 1, '2']

l1 < l2  # => TypeError: '<' not supported between instances of 'int' and 'str'
```



--------------------------------------------------------------------------------
# Split method 10.2.11

*   `.split()` is a method that can be called on a string
*   It splits one string into a list of strings when a certain substring is encountered
*   By default, splitting happens on whitespace, yielding a list of words

```
name = "My name is Erik"
list1 = name.split()
print(list1)  # =>   ['My', 'name', 'is', 'Erik']
```


*   You can split a string on another character

```
str1 = "6/13/2018"
list1 = str1.split("/")
print(list1)  # => ['6', '13', '2018']

# Note these are strings, not numbers
```



--------------------------------------------------------------------------------
# Coding Challenges

* 10.2 - Reverse numbers entered: in a while loop ask user for integers until
  they hit Enter, then print numbers back out in reverse order
* 10.3 - Count occurrences of user-entered numbers 0-100
* 10.4 - given a list of scores, compute the average and report # are below & above average
* 10.11 - Pick N distinct random numbers in a range
* 10.17 - Anagram detector
    - "A gentleman" = "Elegant man"
    - "Debit card" = "Bad credit"
    - "Eleven plus two" = "Twelve plus one"
    - "Vacation time" = "I am not active"
    - "The eyes" = "They see"
    - "The country side" = "No city dust here"
    - "The Detectives" = "Detect thieves"
    - "A decimal point" = "I'm a dot in place"
    - "Astronomers" = "No more stars"
