# Repeatedly ask user for numbers
# when they hit ENTER quit
# print the numbers in reverse order


l = []
while True:
    num = input("Gimme a number (hit ENTER to quit): ")
    if num == "":
        break
    l.append(num)

print()
for num in l[::-1]:
    print(num)


print()
#while len(l) > 0:
while l != []:
    print(l.pop())

print(l)
