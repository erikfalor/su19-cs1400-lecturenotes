# a set of numbers
numbers = [ -2, -23, 65, 71, 32, -96, -46, 48, 66, -58, 44, -35, 15, 92, 58 ]


# Print their sum
sumTotal = sum(numbers)
print("the sum total of my numbers is", sumTotal)

# Calculate their average
average = sumTotal / len(numbers)
print("the average of my numbers is", average)

# Report the highest value
highest = max(numbers)
print("The highest number is", highest)

# Report the lowest value
lowest = min(numbers)
print("The lowest number is", lowest)
