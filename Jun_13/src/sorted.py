l = [2, 7, 17, 8, 14, 23]

def ascendingOrder(lst):
    for i in range(1, len(lst)):
        if lst[i-1] > lst[i]:
            return False
    return True


print(ascendingOrder(l))


def squareAList(lst):
    for i in range(len(lst)):
        lst[i] = lst[i] * lst[i]

print("Before: ", l)
squareAList(l)
print("After:  ", l)
