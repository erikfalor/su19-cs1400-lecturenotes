# CS1400 - Fri Jun 14

# Announcements

## Final Exam

It has been pointed out to me that the Testing Center is not open on Saturdays
during the summer and that it closes early on Friday, at 5pm.

I have adjusted the dates for the final exam to Thu Jun 20 - Fri June 21



## Final Recitation Schedule

Because the final exam is shifted up a day, students who attend recitation on
Thursday may instead join the Wednesday recitation next week.  If everybody who
attends the Thursday recitation chooses to do this, we can cancel the Thursday
recitation.

Please RSVP with Kuan if you *need* to attend the Thursday recitation.



# Topics:
* List Comprehensions 10.2.9
* Getting input from users into a list 10.2.12
* Exercise: Deck of cards



--------------------------------------------------------------------------------
# List Comprehensions 10.2.9

List Comprehensions are a concise way to create a sequential list of items.

Syntactically, list comprehensions combine the square brackets of a list
literal `[`, `]' with loop and conditional keywords in such a way as to
describe a list's contents instead of explicitly stating them.


## Syntax

### for

```
list1 = [<expression> for <id> in <list>]
```

This is the same as

```
list1 = []
for i in <list>:
    list1.append(i)
```

The `<expression>` above can be as simple as evaluating the variable `<id>`
itself, or it can be a complicated Python expression.



### if

List comprehensions may include an if statement, which will only include items
from the for loop that meet the statement condition

```
list1 = [<expression> for <id> in <list> if <conditional>]
```

This is the same as
```
list1 = []
for i in <list>:
    if <conditional>:
        list1.append(i)
```



## List Comprehension Examples

```
# A list list from 0 to 9
list1 = [x for x in range(10)]

# Using an expression, create a list of values which are 50% of the corresponding values from list1
list2 = [0.5 * x for x in list1]


# Collect into list3 all elements of list2 that are less than 3.0
list3 = [x for x in list2 if x < 3.0]
```



--------------------------------------------------------------------------------
# Getting input from users into a list 10.2.12

Here are two approaches you can use:


## Use a loop
Read items one-by-one, appending each to a list

```
numbers = []
while True:
    num = input("Input a number or hit ENTER to quit: ")
    if num == "":
        break
    numbers.append(int(num))

print(numbers)
```



## Split a single input string on whitespace
Read a whole line of input in one shot

```
numbersStr = input("Enter numbers separated by a space: ")
numbers = [eval(n) for n in numbersStr.split() ]
print(numbers)
```


Simplify your code by using a user's numeric input as an index into a list of
words that can be selected:

```
months = ["January", "February", "March", "April", "May", "June", "July",
    "August", "September", "October", "November", "December"]
num = eval(input("Enter a Month (1-12): "))
print("You entered " + months[num - 1])
```

* What happens when the user enters a number that is out of range?
* How can we fix it?



### Example: use lists to simplify the Chinese Zodiac program

Remember this program?  It looks too long.
Let's see if we can use our newfound list powers to simplify.

`src/ChineseZodiac.py`



--------------------------------------------------------------------------------
# Deck of Cards

Let's design a program to simulate two players each drawing one card from a
standard deck of 52 playing cards and deciding who holds the high card.


## Requirements:

1. Cards are of four suits: Spades, Hearts, Diamonds, Clubs
2. Card names are displayed as words: "Ace of Clubs", "Six of Hearts", "Jack of
   Diamonds", etc.
3. Suit values are Spades > Hearts > Diamonds > Clubs
4. Card values are "Ace" = 1, "Two" = 2, "Three" = 3, ... up to "King" = 13
5. Cards may be compared to each other by suit and then by value; an "Ace of
   Diamonds" is greater-than a "King of Clubs"
6. Each card must exist in the deck once and only once
7. The deck supports a `.draw()` method which returns the card on the top of
   the deck and reduces the length of the deck by 1.
8. The deck supports a `.shuffle()` method which restores the deck to 52 cards
   in a random order


The implementation of the Card class we created today is in `src/Card.py`.
