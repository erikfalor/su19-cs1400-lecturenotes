# Find the user's Chineses Zodiac sign based on their birth year. This is a 12 year cycle starting with year 0.


while True:
    # Input: year the user was born
    year = input("Enter your birth year (or ENTER to quit): ")
    if year == "":
        break

    # Output: One of these animals
    signs = [ "Monkey", "Rooster", "Dog", "Pig", "Rat", "Ox", "Tiger", "Rabbit", "Dragon", "Snake", "Horse", "Sheep", ]
    print(signs[int(year) % len(signs)])

