# The old-fashioned way
numbersStr = input("Enter numbers separated by a space: ")
numbers = []
for n in numbersStr.split():
    numbers.append(eval(n))
print(numbers)


# The new-fangled way using list comprehension:
numbersStr = input("Enter numbers separated by a space: ")
numbers = [eval(n) for n in numbersStr.split()]
print(numbers)