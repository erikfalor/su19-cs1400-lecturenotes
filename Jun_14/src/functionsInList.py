# You can place functions into a list

def hello():
    print("Hello World")

def goodbye():
    print("Goodbye World")

def tacocat():
    print("'TacocaT' is a palindrome")

functions = [hello, goodbye, tacocat]

while True:
    which = input("which function do you want to run (1 - 3)? (press ENTER to quit): ")
    if which == '':
        break
    # To call a function that's in a list, write the function invocation
    # operator `()` after you use the index operator `[]` to retrieve it from
    # the  list
    functions[int(which) - 1]()
