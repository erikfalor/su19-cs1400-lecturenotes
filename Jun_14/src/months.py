# Subtract 1 from the user's input because there is no such thing as month 0
months = [, "January", "February", "March", "April", "May", "June", "July",
    "August", "September", "October", "November", "December"]
num = eval(input("Enter a Month (1-12): "))
print("You entered " + months[num - 1])


# Pad the list with a null 0 value to avoid the off-by-one error when I inevitably forget to subtract 1 from the user's input
months = [None, "January", "February", "March", "April", "May", "June", "July",
          "August", "September", "October", "November", "December"]
num = eval(input("Enter a Month (1-12): "))
print("You entered " + months[num])
