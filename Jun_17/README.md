# CS1400 - Mon Jun 17

# Announcements

## Assingment 14 is the last which can be submitted late with no penalty

Fair warning: the "free pass" for turning in an assignment up to 48 hours late
does not apply to the final assignment #15.

[Assignment Submission Policy](https://usu.instructure.com/courses/537470/assignments/syllabus#kl_syllabus_subnav_5_link_3)



## Final Recitation Schedule

The recitation schedule will follow the usual pattern this week; the Wednesday
and Thursday recitation sessions will be held as usual.


# Topics:
* Assignment 14 Overview
* Finish Coding Deck of Cards
* Copying lists (10.6)
* Passing a List to a Function (10.7)
* Create a List to Keep Count (10.9)
* Intro to Search & Sort (10.10)


--------------------------------------------------------------------------------
# Assignment 14 Overview

[Assignment 14](https://usu.instructure.com/courses/537470/assignments/2672584)


--------------------------------------------------------------------------------
# Finish Coding Deck of Cards


In the last lecture we created a `Card` class which meets the first five
requirements of a program which plays a "highest card wins" game.

## Requirements:

1. Cards are of four suits: Spades, Hearts, Diamonds, Clubs
2. Card names are displayed as words: "Ace of Clubs", "Six of Hearts", "Jack of
   Diamonds", etc.
3. Suit values are Spades > Hearts > Diamonds > Clubs
4. Card values are "Ace" = 1, "Two" = 2, "Three" = 3, ... up to "King" = 13
5. Cards may be compared to each other by suit and then by value; an "Ace of
   Diamonds" is greater-than a "King of Clubs"
6. Each card must exist in the deck once and only once
7. The deck supports a `.draw()` method which returns the card on the top of
   the deck and reduces the length of the deck by 1.
8. The deck supports a `.shuffle()` method which restores the deck to 52 cards
   in a random order

The implementation of the Card class we created on Friday is in `src/Card.py`.

Let's finish this program by creating a `Deck` class and then a driver program
containing a `main()` function which plays the game.

[src/Card.py](src/Card.py)


--------------------------------------------------------------------------------
# Copying lists (10.6)

Because lists are mutable, sometimes it is needful to first make a copy of one
list to create a second list which can be destructively modified.


## A Naïve attempt

[src/copyLists.py](src/copyLists.py)

```
# Naïve attempt at copying a list
lst1 = [3, 32, 54, 892, 24, 123]

lst2 = lst1
print("Does assignment result in a copy?", lst1 is not lst2)
```

This didn't work as expected because we made two identifiers reference the same
list.  If we draw a heap/stack diagram we can see the folly of this naïve
attempt.


## The "right" ways

What we really need to do is to copy each individual element from one list into
another.  There are many ways to do this:

```
# Use the list class's copy() method
lst2 = lst1.copy()
print("Does the copy() method result in a copy?", lst2 is not lst1)


# The slice operator copies the selected elements; when we take a slice
# without specifying indices all elements in the list are copied
lst2 = lst1[:]
print("Does a slice result in a copy?", lst2 is not lst1)


# The concatenation operator creates a new list.  This example takes advantage of
# the fact that concatenating an empty list is like adding zero to a quantity
lst2 = [] + lst1
print("Does concatenation result in a copy?", lst2 is not lst1)


# Use a list comprehension (or a for-loop) to copy a list element-by-element
lst2 = [x for x in lst1]
print("Does a comprehension result in a copy?", lst2 is not lst1)
```


--------------------------------------------------------------------------------
# Passing a List to a Function (10.7)

We can, of course, pass a list as a parameter to a function.  Care must be
taken if that function changes the list.

[src/listAsParameter.py](src/listAsParameter.py)

```
def nonDestructive(lst):
    print("Here are the contents of list", id(lst))
    for e in lst:
        print("\t", e)
    print()

def destructive(lst):
    lst.reverse()


lst1 = [x for x in range(5)]
nonDestructive(lst1)
destructive(lst1)
nonDestructive(lst1)
```


Python functions observe pass-by-value semantics, meaning that functions accept
a copy of the variable that is being passed in.  However, the "value" of a list
variable is actually its address on the heap.  In other words, we actually pass
a *reference* to a list.

Because lists are mutable, changing a list inside a function/method means that
the change is visible from the outside.  In other words, what happened in Vegas
*did not* stay in Vegas, so be careful!



## Copying and Pass by Value

When a copy is made properly, you have a new list with identical contents.
What if the list you copied itself contained references?

[src/polygon.py](src/polygon.py)

After copying a list containing referecnes, the new list contains identical
reference values.  So changing an immutable value modifies only one list, but
changing an mutable object's value changes it in both lists.


--------------------------------------------------------------------------------
# Create a List to Keep Count (10.9)

We can use a list to keep a running count of something.  

Write a program to read a string from the user and count the occurance of
letters therein.  Count uppercase and lowercase letters as one, and exclude
digits and punctuation.



--------------------------------------------------------------------------------
# Intro to Search & Sort (10.10)

Consider the `.index()` and `.count()` methods of the List class.  How do you
think they work?

Write your own implementation of these methods in pseudocode (or Python).  Your
solution will involve either a `while` or `for` loop.

* What is the *fewest* number of iterations your method could use to find what
  it's looking for?

* What is the *greatest* number of iterations your method could use to find what
  it's looking for?

* Of `.index()` and `.count()`, which method has to do the most work to get the
  right answer?

* If the item you're looking for is not present in the list, does this mean
  more or less work for the method?
