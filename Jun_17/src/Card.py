SUITS = ["Clubs", "Diamonds", "Hearts", "Spades"]
RANKS = ["Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten",
         "Jack", "Queen", "King"]

class Card:
    def __init__(self, value):
        self.__value = value

    def getValue(self):
        return self.__value

    def __gt__(self, other):
        return self.__value > other.getValue()

    def __lt__(self, other):
        return self.__value < other.getValue()

    def getSuit(self):
        return SUITS[self.__value // 13]

    def getRank(self):
        return RANKS[self.__value % 13]

    def __str__(self):
        return self.getRank() + " of " + self.getSuit()

