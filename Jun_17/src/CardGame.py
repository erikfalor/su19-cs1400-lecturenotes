from Deck import Deck


def main():
    d = Deck()
    d.shuffle()

    p1 = d.draw()
    print("Player 1 drew a", p1)
    p2 = d.draw()
    print("Player 2 drew a", p2)


    if (p1 > p2):
        print("Player 1 wins!!!")
    else:
        print("Player 2 wins!!!")

main()
