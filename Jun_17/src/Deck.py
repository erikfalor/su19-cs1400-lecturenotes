from random import shuffle
from Card import Card


class Deck:
    def __init__(self):
        """Each card must exist in the deck once and only once"""
        self.__createDeck()


    def __createDeck(self):
        self.__cards = []
        for i in range(52):
            self.__cards.append(Card(i))


    def draw(self):
        """return the card on the top of the deck and reduces the length of the deck by 1"""
        return self.__cards.pop(0)


    def shuffle(self):
        """restore the deck to 52 cards in a random order"""
        self.__createDeck()
        shuffle(self.__cards)


    def __str__(self):
        s = "Your deck:\n"
        for c in self.__cards:
            s += "\t" + str(c) + "\n"
        return s
