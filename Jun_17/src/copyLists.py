# Naïve attempt at copying a list
lst1 = [3, 32, 54, 892, 24, 123]

lst2 = lst1
print("Does assignment result in a copy?", lst1 is not lst2)


# Use the list class's copy() method
lst2 = lst1.copy()
print("Does the copy() method result in a copy?", lst2 is not lst1)


# The slice operator copies the selected elements; when we take a slice
# without specifying indices all elements in the list are copied
lst2 = lst1[:]
print("Does a slice result in a copy?", lst2 is not lst1)


# The concatenation operator creates a new list.  This example takes advantage of
# the fact that concatenating an empty list is like adding zero to a quantity
lst2 = [] + lst1
print("Does concatenation result in a copy?", lst2 is not lst1)


# Use a list comprehension (or a for-loop) to copy a list element-by-element
lst2 = [x for x in lst1]
print("Does a comprehension result in a copy?", lst2 is not lst1)
