def nonDestructive(lst):
    print("Here are the contents of list", id(lst))
    for e in lst:
        print("\t", e)
    print()

def destructive(lst):
    lst.reverse()


lst1 = [x for x in range(5)]
nonDestructive(lst1)
destructive(lst1)
nonDestructive(lst1)
