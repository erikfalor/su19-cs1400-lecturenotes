class Polygon:
    def __init__(self, n=3):
        self.n = n

    def __repr__(self):
        return f"Polygon({self.n})"

    def __len__(self):
        return self.n


polygons = [Polygon(p) for p in range(3, 10)]
polygons.append(1337)
backup = polygons[:]

print("Before: polygons =", polygons)
print("Before: backup   =", backup)
print("Before: Is polygons the same reference as backup?", polygons is backup)

print("\nModifying 'polygons' and leaving 'backup' alone...\n")

# Changing a mutable value within 'polygons'
polygons[2].n = len(polygons[3]) + len(polygons[5])

# Changing an immutable value within 'polygons'
polygons[-1] += 1


print("After:  polygons =", polygons)
print("After:  backup   =", backup)
print("After:  Is polygons the same reference as backup?", polygons is backup)
