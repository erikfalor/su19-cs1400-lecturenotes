# CS1400 - Tue Jun 18

# Announcements

# Topics:
* Searching Lists (10.10)
* Sorting Lists (10.11)


--------------------------------------------------------------------------------
# Searching Lists (10.10)

We can search a list to see if something is contained in the list.  While in
real-life you will rely upon built-in functions to do this, it is important to
be able to code this yourself so that you understand what's actually happening.

## Linear search (10.10.1)
*   Search through the list one element at a time, looking for an element which
    matches your key
*   Example: [src/linear.py](src/linear.py)
*   If your list is very big, this can take a *long* time
    -   On average you will have to check 1/2 of the elements
    -   In the worst case you have to check *every* element, only to come up
        empty



## Binary Search (10.10.2)
*   A quicker way to search than linear, but requires that the list is ordered
*   This is like the guess the number game
    -   We choose a number
    -   We're told higher or lower
    -   We choose a number
    -   Continue until we get it right
    -   What's the strategy?

*   Example: [src/binary.py](src/binary.py)
    -   Keep track of index values for the list
        -   `low = 0`
        -   `high = len(lst) - 1`
    -   Find a midpoint
        -   `mid = (high + low) // 2  # integer division`
    -   Check the midpoint to match the key
        -   `lst[mid] == key`
    -   There are three possibilities at each iteration:
        -   `key` is lower
        -   `key` is higher
        -   `key` matches
    -   If there isn't a match
        -   Change `low` or `high` index to what the `mid` value is, discarding
            1/2 of the list from our consideration
        -   Decide which index value should change
            -   if the key is lower than what we guessed we eliminate the highest 1/2
                -   `high = mid - 1`
            -   if the key is higher than what we guessed we eliminate the lower 1/2
                -   `low = mid + 1`
    -   Repeat until
        1.   There is a match
        2.   Or you run out of numbers to check; when `high < low`, you've run out


## How do we know Binary Search is better than Linear Search?

All search algorithms repeatedly compare the search key items in the list until either
1. A match is found
2. We can prove that a matching key does not exist in the list

We can compare the efficiency of two search algorithm by counting the number of
comparisons needed to conclude the search.


## Efficiency of a Linear Search over a list of length *N*

The linear search stops as soon as it encounters a match, or the list is
exhausted.  In the worst case, this means that *N* comparisons are made.  In
the best case we find what we're looking for at the beginning of the list while
on the first comparison.  There is no reason to expect the match to come
earlier or later in the list.  On average a linear search will conclude after
*N/2* comparisons.



## Efficiency of a Binary Search over a sorted list of length *N*

At each iteration the binary search eliminates 1/2 of the possibilities without
even looking at them.  We iterate until we are left with 1 item under
consideration: it either is or is not our match.

The question is "how many iterations does it take to get down to 1 item?"

`N` is the number of items in the list.  We want `1.0 == N / (2**x)` where `x` is the
number of times we need to divide `N` by 2 to get 1.


*Note:* This is a derivation; it is not actually executable Python code.

```
import math

# We want this equality to hold
2**x == N

math.log2(2**x) == math.log2(N)

x * math.log2(2) == math.log2(N)

x == math.log2(N)
```

|  Items in list  |  Linear Search  |  Binary Search  |
|-----------------|-----------------|-----------------|
|               1 |               1 |               1 |
|              10 |               5 |               4 |
|             100 |              50 |               7 |
|           1,000 |             500 |              10 |
|          10,000 |           5,000 |              14 |
|         100,000 |          50,000 |              17 |
|       1,000,000 |         500,000 |              20 |
|      10,000,000 |       5,000,000 |              24 |
|     100,000,000 |      50,000,000 |              27 |
|   1,000,000,000 |     500,000,000 |              30 |
|  10,000,000,000 |   5,000,000,000 |              34 |
| 100,000,000,000 |  50,000,000,000 |              37 |

**TL;DR:** The number of iterations (and therefore comparisons) the binary
search needs to perform is the log base 2 of the length of our list (i.e.
`math.log2(len(lst))`).  In other words, searching a sorted list that is 10
times longer only takes 3 or 4 more comparisons with the binary search
algorithm


--------------------------------------------------------------------------------
# Sorting Lists (10.11)

[Sorting Algorithm Animations](https://www.toptal.com/developers/sorting-algorithms)

As with searching, we can sort a list by using built-in functions provided by
the programming environment you're working in.  It is likewise important for
you to understand what's actually going on in the code library, hence we will
look at two sorting algorithms in detail.  In each case we'll implement an
ascending sort.


## Selection Sort (10.11.1)

* [Selection Sort Explained](https://www.toptal.com/developers/sorting-algorithms/selection-sort)
* [Selection Sort Worksheet](selectionSort.pdf)

Select the smallest number from the unsorted list, and place it at the front.
Repeat for the next smallest number, placing it right after the smallest
number you just put down.  Repeat until you reach the end of the list.

**Steps:**

1.   Find the smallest number in the list
2.   Swap this number with the first element in the list
3.   Find the smallest number in the remaining list (not including first
     element)
4.   Swap this number with the second element in the list (the first element in
     the remaining list)
5.   Keep doing this until you get to the last element in the list

[src/selectionSort.py](src/selectionSort.py)

**Speed:**

* The nested loop always iterates `n**2` times.
* In the worst case this algorithm makes `n**2` comparisons.
* In the best case this algorithm makes `n**2` comparisons.



## Insertion Sort (10.11.2)

* [Insertion Sort Explained](https://www.toptal.com/developers/sorting-algorithms/insertion-sort)
* [Insertion Sort Worksheet](insertionSort.pdf)

**Steps:**

1. Begin with a "sorted" sublist containing only the first element.
2. Extend the sorted sublist by inserting the following element of the main
   list into its proper location within the sublist
3. Repeat until the sorted sublist subsumes the entire list


[src/insertionSort.py](src/insertionSort.py)


**Speed:**

* The nested loop iterates a variable number of times, fewer if the data is
  already in order.
* In the worst case this algorithm makes `n**2` comparisons.
* In the best case this algorithm makes `n` comparisons.



## Bubble Sort

* [Bubble Sort Explained](https://www.toptal.com/developers/sorting-algorithms/bubble-sort)
* [Bubble Sort Worksheet](bubbleSort.pdf)

The bubble sort algorithm repeatedly compares neighboring pairs of elements
within the list. If a pair is out of order, the values are swapped; otherwise,
they remain unchanged.  The name "bubble sort" comes from the observation that
smaller values gradually "bubble" their way to the top.

**Steps:**

1. Start at the top of the list
2. Compare elements `k` and `k+1`; swap if element `k` > element `k+1`
3. Compare elements `k+1` with `k+2`, swapping if necessary
4. When the end of the list is reached, start again from the top
5. Terminate the algorithm when the end of the list is reached without
   performing *any* swaps


[src/bubbleSort.py](src/bubbleSort.py)


**Speed:**

* The outer loop always iterates `n` times, but the inner loop may take fewer
  iterations
* In the worst case this algorithm makes `n**2` comparisons.
* In the best case this algorithm makes just `n` comparisons.
