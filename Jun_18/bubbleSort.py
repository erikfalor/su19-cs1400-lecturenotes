# TODO: count the number of steps the algorithm took

def bubbleSort(lst):
    """Note: this function mutates its input list, hence has no return value"""
    while True:
        swapped = False

        for k in range(len(lst) - 1):
            # Swap an item if it is greater than the value at its right
            if lst[k] > lst[k+1]:
                lst[k], lst[k+1] = lst[k+1], lst[k]  
                swapped = True

        # Make passes until no swaps are performed
        if not swapped:
            break


myList = [ -40, -43, 90, 48, 19, -22, 62, 77, -30, 5, 59, 71, 65, 68 ]

print("Before:", myList)

bubbleSort(myList)

print("After: ", myList)
