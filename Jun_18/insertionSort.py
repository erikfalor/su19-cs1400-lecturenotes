# TODO: count the number of steps the algorithm took

def insertionSort(lst):
    """Note: this function mutates its input list, hence has no return value"""
    for i in range(1, len(lst)):
        # Insert lst[i] into the sorted sublist lst[0:i] such that lst[0: i+1] is sorted
        cur = lst[i]
        k = i - 1
        while k >= 0 and lst[k] > cur:
            lst[k+1] = lst[k]
            k -= 1

        # insert cur into lst at index [k+1]
        lst[k+1] = cur


myList = [ -40, -43, 90, 48, 19, -22, 62, 77, -30, 5, 59, 71, 65, 68 ]

print("Before:", myList)

insertionSort(myList)

print("After: ", myList)
