def linear(lst, key):
    """Return the position of key in a list or -1 if key is not found"""
    steps = 0
    for i in range(len(lst)):
        steps += 1
        if key == lst[i]:
            print("I found it in", steps, "steps")
            return i
    print("I failed to find it after", steps, "steps")
    return -1


lst = [ -40, -43, 90, 48, 19, -22, 62, 77, -30, 5, 59, 71, 65, 68 ]

key = 77
print("The number", key, "is found at position", linear(lst, key))


print()
key = 23
print("The number", key, "is found at position", linear(lst, key))
