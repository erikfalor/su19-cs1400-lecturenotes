# TODO: count the number of steps the algorithm took
# TODO: Print out the high, low, mid at each step

def binary(lst, key):
    """Return the position of key in a sorted list or -1 if key is not found"""
    low = 0
    high = len(lst) - 1
    while high >= low:
        mid = (high + low) // 2  # integer division
        if key == lst[mid]:
            return mid
        elif key < lst[mid]:
            high = mid - 1
        else:
            low = mid + 1
    return -1


print("A Binary Search won't yield reliable results if the list isn't sorted!")
print()

unsortedLst = [ -40, -43, 90, 48, 19, -22, 62, 77, -30, 5, 59, 71, 65, 68 ]
n = 77
print("The number", n, "is found at position", binary(unsortedLst, n))
print("What do you mean that", n, "wasn't found?!?  It's right there!")


print()
print("Let's try that again on the sorted list...")
sortedLst = [-43, -40, -30, -22, 5, 19, 48, 59, 62, 65, 68, 71, 77, 90]
print("The number", n, "is found at position", binary(sortedLst, n))
