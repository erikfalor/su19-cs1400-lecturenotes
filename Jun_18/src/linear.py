# TODO: count the number of steps the algorithm took

def linear(lst, key):
    """Return the position of key in a list or -1 if key is not found"""
    for i in range(len(lst)):
        if key == lst[i]:
            return i
    return -1


lst = [ -40, -43, 90, 48, 19, -22, 62, 77, -30, 5, 59, 71, 65, 68 ]

n = 77
print("The number", n, "is found at position", linear(lst, n))


print()
n = 23
print("The number", n, "is found at position", linear(lst, n))
