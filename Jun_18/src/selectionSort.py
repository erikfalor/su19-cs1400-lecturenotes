def selectionSort(lst):
    """Note: this function mutates its input list, hence has no return value"""
    for i in range(len(lst) - 1):
        # find the smallest value in lst
        minIdx = i
        minVal = lst[i]

        for j in range(i + 1, len(lst)):
            if minVal > lst[j]:
                minVal = lst[j]
                minIdx = j

        # Swap minimum value w/lst[i], if needed
        if minIdx != i:
            lst[minIdx] = lst[i]
            lst[i] = minVal


myList = [ -40, -43, 90, 48, 19, -22, 62, 77, -30, 5, 59, 71, 65, 68 ]

print("Before:", myList)

selectionSort(myList)

print("After: ", myList)
