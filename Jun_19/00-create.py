import random

def print2d(message, lst):
    print(message)
    for row in lst:
        for col in row:
            print(format(col, ">4"), end=" ")
        print()
    print()


print("Creating a list containing empty lists from literal values")
twoEmpties = [ [], [] ]
print(twoEmpties)
print()



print("Creating a list containing non-empty lists from literal values")
distances = [
        [0, 983, 787, 714, 1375, 967, 1087],
        [983, 0, 214, 1102, 1763, 1723, 1842],
        [787, 214, 0, 888, 1549, 1548, 1627],
        [714, 1102, 888, 0, 661, 781, 810],
        [1375, 1763, 1549, 661, 0, 1426, 1187],
        [967, 1723, 1548, 781, 1426, 0, 239],
        [1087, 1842, 1627, 810, 1187, 239, 0]
]
print2d("City Distance Table", distances)
print()


print("Create an empty list and fill it with 10 empty lists")
tenEmpties = []
for i in range(8):
    tenEmpties.append([])
print(tenEmpties)
print()


print("Now, fill that list of lists with random numbers")
for i in range(len(tenEmpties)):
    for j in range(10):
        tenEmpties[i].append(random.randint(0, 9))
print2d("10x10 random numbers", tenEmpties)
print()


print("Let's create a list entirely from user input")
table = [] # Create an empty list
numberOfRows = eval(input("Enter the number of rows: "))
numberOfColumns = eval(input("Enter the number of columns: "))

for row in range(0, numberOfRows): 
    table.append([]) # Add an empty new row 
    for column in range(0, numberOfColumns): 
        value = random.randint(0, 1337)
        table[row].append(value) 

print2d("rando input", table)
