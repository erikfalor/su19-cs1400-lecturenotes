import random


def print2d(message, lst):
        print(message)
        for row in lst:
                for col in row:
                        print(format(col, ">4"), end=" ")
                print()
        print()




print("Creating a list containing non-empty lists from literal values")
distances = [
        [0, 983, 787, 714, 1375, 967, 1087],
        [983, 0, 214, 1102, 1763, 1723, 1842],
        [787, 214, 0, 888, 1549, 1548, 1627],
        [714, 1102, 888, 0, 661, 781, 810],
        [1375, 1763, 1549, 661, 0, 1426, 1187],
        [967, 1723, 1548, 781, 1426, 0, 239],
        [1087, 1842, 1627, 810, 1187, 239, 0]
]
print2d("table of distances", distances)
print()


# At each index of the list resides another list
print("The 0th list in distances is", distances[0])


# We can tack on a second index operator to access an element within *that* list
print("The 2nd item in the 0th list in distances is", distances[0][2])


# The 1st index operator refers to a *row*, and the 2nd index operator accesses
# a *column* within that row.  We can assign a new value into that cell of the
# table through the doubled index operation:
distances[0][2] = 1337
print("The 2nd item in the 0th list in distances is now", distances[0][2])
