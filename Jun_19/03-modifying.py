import random


def print2d(message, lst):
    print(message)
    for row in lst:
        for col in row:
            print(format(col, ">4"), end=" ")
        print()
    print()


distances = [
        [0, 983, 787, 714, 1375, 967, 1087],
        [983, 0, 214, 1102, 1763, 1723, 1842],
        [787, 214, 0, 888, 1549, 1548, 1627],
        [714, 1102, 888, 0, 661, 781, 810],
        [1375, 1763, 1549, 661, 0, 1426, 1187],
        [967, 1723, 1548, 781, 1426, 0, 239],
        [1087, 1842, 1627, 810, 1187, 239, 0]
]
print2d("The original list:", distances)


for row in distances:
    for col in row:
        if col == 0:
            col = "LOL"
        else:
            col //= 2
print2d("Attempted some modifications which were futile", distances)



# While iterating over a list by index is more verbose, it does have the
# advantage enabling us to change the table as we go:
for row in range(len(distances)):
    for col in range(len(distances[row])):
        if distances[row][col] == 0:
            distances[row][col] = "LOL"
        else:
            distances[row][col] //= 2

print2d("Changing zeroes into 'LOL's, reducing other values by half", distances)

