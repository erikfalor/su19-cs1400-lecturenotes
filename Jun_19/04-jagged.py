from random import randint


def print2d(message, lst):
    print(message)
    for row in lst:
        for col in row:
            print(format(col, ">4"), end=" ")
        print()
    print()



jagged = []
for row in range(randint(4, 19)):
    newRow = []
    for col in range(randint(0, 14)):
        newRow.append(randint(0, 100))
    jagged.append(newRow)

print2d("Today's random jagged array:", jagged)
