from random import randint


def print2d(message, lst):
    print(message)
    for row in lst:
        for col in row:
            print(format(col, ">4"), end=" ")
        print()
    print()


distances = [
    [0, 983, 787, 714, 1375, 967, 1087],
    [983, 0, 214, 1102, 1763, 1723, 1842],
    [787, 214, 0, 888, 1549, 1548, 1627],
    [714, 1102, 888, 0, 661, 781, 810],
    [1375, 1763, 1549, 661, 0, 1426, 1187],
    [967, 1723, 1548, 781, 1426, 0, 239],
    [1087, 1842, 1627, 810, 1187, 239, ]
]



## Textbook exercise 11.1
def sumColumns(twoDeeList):
    sums = []
    for col in twoDeeList[0]:
        sums.append(0)
    for row in twoDeeList:
        for col in range(len(row)):
            sums[col] += row[col]
    return sums

colSums = sumColumns(distances)
print("The sums of the columns in distances are", colSums)


## These two paragraphs of code are equivalent
# The tried-and-true for loop
rowSums = []
for row in distances:
    rowSums.append(sum(row))
print("The sums of the columns in distances are", rowSums)

# The newfangled list comprehension
rowSums = [sum(row) for row in distances]
print("Again, the sums of the columns in distances are", rowSums)


## Textbook exercise 11.2

def sumMajorDiagonal(matrix):
    sum = 0
    for i in range(len(matrix[0])):
        sum += matrix[i][i]
    return sum

print("The sum of the major diagonal of distances is", sumMajorDiagonal(distances))

