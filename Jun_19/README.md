# CS1400 - Wed Jun 19

# Announcements

# Topics:
* Processing Two-Dimensional Lists (11.2)
* Creating 2D lists (11.2.1)
* Accessing a single datum within 2D lists 
* Printing tables (11.2.3)
* Modifying tables




--------------------------------------------------------------------------------
# Processing Two-Dimensional Lists (11.2)

* Lists in Python are reference values
* Lists in Python can contain reference values
* Hence, lists in Python can contain other lists

Tabular data is represented in programming languages as lists of lists


--------------------------------------------------------------------------------
# Creating 2D lists (11.2.1)

Lists in Python are objects.  Python lists can contain any kind of object.  A
two dimensional list (a.k.a. a table) is simply a list which contains other
lists.  Just as with `if` statements and loops, you can nest lists within lists
as deeply as you please.  The concepts for 2D lists demonstrated here extend to
lists of higher dimensions in a straightforward way.

2D lists may be created by

* Using list literals
* Dynamically by using loops and `.append()`
* From user input

[src/00-create.py](src/00-create.py)



--------------------------------------------------------------------------------
# Accessing a single datum within 2D lists 

At each index of the list resides another list
```
print("The 0th list in distances is", distances[0])
```

We can tack on a second index operator to access an element within *that* list
```
print("The 2nd item in the 0th list in distances is", distances[0][2])
```

The 1st index operator refers to a *row*, and the 2nd index operator accesses a
*column* within that row.  We can assign a new value into that cell of the
table through the doubled index operation:

```
distances[0][2] = 1337
print("The 2nd item in the 0th list in distances is now", distances[0][2])
```

[src/01-access.py](src/01-access.py)



--------------------------------------------------------------------------------
# Printing tables (11.2.3)

Previously, we used `for` loops to print 1D lists.  Adding a dimension to our
lists means that we must add another `for` loop to print each individual value.

```
# This uses a single for loop
print("Treating distances as a 1D list of sublists")
for sublist in distances:
    print(sublist)
print()
```

To reach each element individually takes two for loops:
```
print("Treating distances as a 2D list")
for row in distances:
    for col in row:
        print(format(col, "4n"), end=" ")
    print()
print()
```


Alternatively, we can use `len()` and `range()` to iterate over the indices of
the table, though it takes more writing:
```
print("Idem., but accessing elements by their indices")
for row in range(len(distances)):
    for col in range(len(distances[row])):
        print(format(distances[row][col], "4n"), end=" ")
    print()
print()
```

If you just need to read the values the first form is preferable.

[src/02-printing.py](src/02-printing.py)


--------------------------------------------------------------------------------
# Modifying tables

While iterating over a list by index is more verbose, it does have 
the advantage enabling us to change the table as we go:


```
for row in range(len(distances)):
    for col in range(len(distances[row])):
        distances[row][col] *= 2.5
        print(distances[row][col], end=" ")
    print()
print()
```

[src/03-modifying.py](src/03-modifying.py)
