import random


distances = [
        [0, 983, 787, 714, 1375, 967, 1087],
        [983, 0, 214, 1102, 1763, 1723, 1842],
        [787, 214, 0, 888, 1549, 1548, 1627],
        [714, 1102, 888, 0, 661, 781, 810],
        [1375, 1763, 1549, 661, 0, 1426, 1187],
        [967, 1723, 1548, 781, 1426, 0, 239],
        [1087, 1842, 1627, 810, 1187, 239, 0]
]


print("This is not attractive, nor readable:")
print(distances)
print()


# This uses a single for loop
print("Treating distances as a 1D list of sublists")
for sublist in distances:
    print(sublist)
print()



# To reach each element individually takes two for loops:
print("Treating distances as a 2D list")
for row in distances:
    for col in row:
        print(format(col, "4n"), end=" ")
    print()
print()



# Alternatively, we can use `len()` and `range()` to iterate over the indices
# of the table, though it takes more writing:
print("Idem., but accessing elements by their indices")
for row in range(len(distances)):
    for col in range(len(distances[row])):
        print(format(distances[row][col], "4n"), end=" ")
    print()
print()
