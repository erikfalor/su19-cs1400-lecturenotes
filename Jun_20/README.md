# CS1400 - Thu Jun 20

# Announcements


# Topics:
* Assignment 15 Overview
* Final Exam Review




--------------------------------------------------------------------------------
# Assignment 15 Overview




--------------------------------------------------------------------------------
# Final Exam Review

* 89 questions
* 200 points
* 3 hours to complete
* You are encouraged to use scratch paper


## Review questions
* Basic Python concepts
    * Operators
    * Comments
    * Strings
    * Built-in functions such as `print()`, `eval()`, `int()`, `round()`, `abs()`, `len()`, etc.
    * Python is an *interpreted* language
    * Python is a *high-level* language
    * Python is a *dynamically-typed* language
* Steps of the Software Development Process 



## Booleans and if Statements
* Using boolean values/expressions in `if` statements
* Boolean operations `and`, `or`, `not`
    * Short-circuiting behavior of keywords `and`, `or`
    * Identifying truth tables of boolean operations
    * Precedence & order of operations
* Python language keywords related to conditionals `if`, `elif`, `else`
* Identify which snippet of code gives the correct result
* Identify the output of a snippet of code
    


## Random numbers
* True random number generator vs. Pseudo random number generator 
* Proper use of functions from Python's `random` module



## Looping and repetition
* Proper use of `range()`
* Predict the number of iterations a snippet of code will perform
* Decide whether a loop is an infinite loop or not
* Python language keywords related to looping `for`, `while`, `break`, `continue`
* When to use a `for` loop vs. a `while` loop
    * Loop design strategies "counters", "sentinel values", "user confirmation"



## Functions
* What is the purpose of writing functions?
* What is the significance of `()` after a function's name?
* Defining functions
    * Python language keywords related to functions `def`, `return`
    * `return` is optional; what does a function return if left out?
    * Difference between *formal* parameters and *actual* parameters
    * *Positional* parameters vs. *keyword* parameters
    * How to specify default values for parameters?
    * Where to specify parameters which take default values? 
    * "parameters" == "arguments"
* Calling functions
    * How does one invoke a function?
    * How does one invoke a using keyword parameters?
    * How does the "What happens in Vegas stays in Vegas" rule differ between simple, immutable parameters (like numbers and strings) and mutable data structures (such as lists)?
* Difference between the stack and the heap
* Values are passed into Python functions by the "Pass by Value" technique



## Scope and modules
* What do we call a variable declared *inside* of a function?
    * What happens when a function declares a variable which has the same name as a variable that exists *outside* the function?
* Python modules
    * What is the purpose of modules in Python?
    * How do I use modules in my code?
    * How do I access identifiers (variables and functions) declared in another Python file?
    * What happens if a module defines a function with the same name as a function in my main program?



## Object-Oriented Programming
* What is the purpose of OOP?
* How does OOP differ from *procedural* programming?
* What is the distinction between a *class* and an *object*?
* What do we call data which belong to a class?
* What do we call functions which belong to a class?
* Encapsulation
    * What are the advantages of encapsulation?
    * How do we make members *private* or *hidden* within a class?
    * What are *getters* and *setters*, and how do they relate to encapsulation?
* Abstraction
    * What are the advantages of abstraction?
    * How is a class like a black box?
* Unified Modeling Language
    * What is the purpose of a UML class diagram?
    * How many compartments does a UML class diagram contain, and what data goes in each compartment?
    * How does a UML diagram relate to the Software Development Process?
* What do we call the special method used to initialize an object?
    * What is its name in the Python code?
* What is the conventional name of the first parameter to a method?
    * What does this parameter refer to?
* How does one access data and methods contained within an object?
* Overloading operators and special methods
    * How do *dunders* relate to operators like `+`, `>`, `==`?
    * How do *dunders* relate to special functions like `len()`, `str()`?



## Strings, lists, collections
* Strings are immutable, but Lists are mutable
* Strings and lists are kinds of *collections*, which provide a way for us to store many pieces of data in one variable
* Strings and lists are kinds of *sequences*, which are a kind of collection in which the constituent data is stored in order
* Operations supported by all sequences
    * Indexing
    * Slicing
    * Iteration
    * Concatenation
* Operations supported by mutable sequences
    * Appending
    * Popping
    * Replacing elements
    * Copying
* What is the difference between the `==` and `is` operators?



## Sorting/Searching Lists
* Read code and identify which algorithm it implements
* Read code and supply a missing expression
* Pros and cons of linear search vs. binary search
    * Under which circumstances can a binary search be expected to succeed?
* Pros and cons of the three sorting algorithms we studied



## Multi-dimensional lists
* How to access data within a 2D list
* The best approach for iterating through a 2D list
* What output is produced by some code snippets?
