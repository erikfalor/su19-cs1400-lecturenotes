def factorial(n=11):
    product = 1
    for i in range(2, n+1):
        product *= i
    return product

for j in range(20):
    print("f(", j, ") = ", factorial(j))
