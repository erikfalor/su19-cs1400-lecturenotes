def a(i):
    n = 7
    print("In a, n=", n, "and i=", i)
    b(i)
    print("In b, after calling b(), n=", n, "with id(n)=", id(n))


def b(i):
    n = 70
    print("In b, n=", n, "and i=", i)
    c(i)
    print("In b, after calling c(), n=", n, "with id(n)=", id(n))


def c(i):
    n = 700
    print("In c, n=", n, "and i=", i)


a(100)
