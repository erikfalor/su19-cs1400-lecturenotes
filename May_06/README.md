# CS1400 - Mon May 06

# Announcements

## Tutor lab opens on Tuesday


## The first recitation session is skipped

* Recitation Section 501 begins on Wednesday, May 8th
* Recitation Section 502 begins on Thursday, May 9th


# Topics:

* CS1400 Course Introduction
* Syllabus
* What is Computer Science?
* Installing Requisite Software
* Brief introduction of first three assignments



----------------------------------------------------------------------------
# CS1400 Course Introduction

* My name is Erik Falor
    -   I am not a Doctor.  You may call me "Erik" or "Professor"
    -   I grew up in Smithfield and graduated from USU
    -   I worked as a Software Engineer for 13 years before coming to USU
        -   I wrote code on large servers, tiny microcontrollers, and
            everything in between
    -   In my work I've used lots of programming languages: 
        - Perl
        - C
        - C++
        - C#
        - Java
        - x86, PowerPC and ARM Assembly (just a little bit)
        - Bourne Shell languages
        - VimScript
        - XML, XSLT, VB and PHP (but I'm not proud of these)
    -   For fun I write code in other languages:
        - Python
        - Scheme
        - LISP
        - Go
        - Haskell
        - Prolog
        - ...and many more
    -   My laptop runs Slackware Linux 14.2
    -   I use a tiling window manager called DWM (http://dwm.suckless.org/)
    -   I don't type in QWERTY, I use a keyboard layout called Colehack
    -   The program you're looking at right now is called Vim
    -   Yes, I actually do *hate* GUIs

* Graduate Teaching Assistant Kuan Huang

* Tutor Lab Maestro Bryan Christensen


----------------------------------------------------------------------------
# Syllabus

* Course Fee
* Course Objectives
* Contact Information
* Textbook
* [Lecture Notes * Demo Code](https://bitbucket.org/erikfalor/su19-cs1400-lecturenotes/)
* Academic Integrity Policy
* Exam Schedule
* Assignments & Late Policy
* Recitation


----------------------------------------------------------------------------
# What is Computer Science?
*    [Computer science is for everyone | Hadi Partovi | TEDxRainier](https://youtu.be/FpMNs7H24X0) 
    -   Watched this in class (10:32)
    -   CS is not just programming
    -   Need more women
        -   Eniac picture with woman
        -   Ada Lovelace
        -   Men need to be able to work in a respectful and professional
            manner, it will serve their careers

* [What is CS? (Science Magazine)](http://digitalcollections.library.cmu.edu/awweb/awarchive?type=file&item=356799)
    - CS is broader than just coding
    - Job growth outpaces graduation 3:1
    - CS offers a way forward for a new middle class
    - CS is easier and more fun than you thought


## Who's who in this class?

- How many CS majors?
- How many Engineering majors?
- How many Science majors?
- How many Undeclared/Undecided majors?
- How many MIS majors?
- How many others?


## What is an algorithm?


> Computer science (CS) is the study of computers and algorithmic processes,
> including their principles, their hardware and software designs, their
> applications, and their impact on society.
>
>   A Model Curriculum for K-12 Computer Science: Final Report of the ACM K-12
>   Task Force Curriculum Committee
>   --   Allen Tucker
>   --   2003



----------------------------------------------------------------------------
# Installing Requisite Software

## Python

Python is the programming language we will use this semester to learn the
principles of Computer Science.  A programming language gives humans *a* way to
convey instructions to a computer.  A good programming language enables us to
communicate to a computer in a way that is familiar and comfortable to our
human minds.  Python is a good programming language for beginners.

*   [https://www.python.org/](https://www.python.org/)
*   We will use Python3
    - Do *not* use Python2
*   [Download Python](https://www.python.org/downloads/)
*   Use the most current version, today this is 3.7.3
    -   Not a pre-release
    -   Not a release candidate (ends in rc#)

## PyCharm
*   Windows, Mac, or Linux
*   You may use the Community Edition for free
*   [Pycharm Download](https://www.jetbrains.com/pycharm/download/)



## Installation Notes

*   You can have multiple versions of Python installed.  You can choose which to use
    when you make a new project in PyCharm.  Make sure that you use the same
    version as the rest of the class!
*   You can use other IDEs, but only PyCharm is supported
    - I recommend that we all use PyCharm for consistency
    - I won't show you how to use the others
    - If you use the others (i.e. VS Code) I cannot help you when it screws up
*   Chromebooks are unsupported as PyCharm doesn't run on them
*   Show how to access the Windows console to check whether Python is installed



----------------------------------------------------------------------------
# Introduction of the first three assignments

## [Assn 1 - Due Wednesday 5/8](https://usu.instructure.com/courses/537470/assignments/2672578)
* A traditional "Hello World!" Assignment
* Ensures that your system is set up correctly with all necessary software
* Gives you practice submitting homework properly


## [Assn 2 - Due Saturday 5/11](https://usu.instructure.com/courses/537470/assignments/2672586)
* Write a brief "What is Computer Science Essay"
* The twist is that your essay is submitted as a Python program.
* We will run your program to read the essay.


## [Assn 3 - Due Saturday 5/11](https://usu.instructure.com/courses/537470/quizzes/722698)
* Fundamentals of Computer Science Quiz
* Online quiz taken in Canvas
* You must do this yourself - "group work" on a quiz or exam is cheating!


## Your Resources for working on assignments
*   Become familiar with Canvas
*   Find my lecture notes and demo code
    -   The link is on Canvas
*   Python has lots of documentation!
    -   Built-in documentation available in the Python Console (REPL).
        This is one of Python's greatest strengths.  I'll show you how to use
        this throughout the semester
    -   [Python's Online documentation](https://docs.python.org)
        -   Check the version number on the documentation!  If you're not
            careful you'll read outdated or wrong instructions.  This goes for
            anything you find online regarding Python.
        -   There was a big split in the community between the Python2 and
            Python3 languages about 10 years ago.  Python2 is (finally)
            officially being retired at the [end of this year](https://pythonclock.org/)
*   StackOverflow is a popular website, but frought with peril
    -   Students will waste a lot of time searching around here because they
        don't yet know how to discriminate between good advice and bad advice.
    -   Even if you find the best answer here, did you learn *why* it's best?
*   Tutor Lab
    -   Bring your specific, well-thought out question
    -   Read the error messages, even the parts that make your eyes glaze over
        (that'll get better in time)
    -   Explain what you have tried and why you think it didn't work
    -   Listen!
