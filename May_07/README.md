# CS1400 - Tue May 07

# Topics:
* Creating coding projects in PyCharm
* How to run your Python programs in PyCharm
* What is a Computer? (Textbook Ch. 1.2)


----------------------------------------------------------------------------
# Creating coding projects in PyCharm

Before we begin, what questions do you have about

* Installing Python3?

* Installing PyCharm?



## Your algorithm for creating each homework assignment in PyCharm

Follow these simple instructions and nobody gets hurt:

1.  `File => New Project` or `Create New Project` from the launch menu


2.  Enter a location and name for your project
    -   The project/folder name *must* follow the course convention
    -   cs1400-<Last>-<First>-Assn#
    -   cs1400-Falor-Erik-Assn1


3.  Choose your Python Interpreter
    -   Choose existing interpreter, not new environment
    -   *Don't* use virtual environments
    -   Select Python3
    -   If it's not there, select gear and search to where you installed it


4.  Add a new file to your empty project folder
    -   Right click on the project folder
    -   Choose New > Python File
        -   Give a name to the file
        -   PyCharm automatically adds the `.py` extension for you
        -   (This is a good time to tell Windows to stop lying to you about the
            name of every file on your computer)
    -   Add a print statement to the file, like this: `print("Hello World!")`
    -   Now add another
    -   Congratulations, you're halfway done with Assignment #1!
    -   Before you give me your code you first need to *test* it



----------------------------------------------------------------------------
# How to run your Python programs in PyCharm

*Protip:* This code will be made available online after class 
on [Bitbucket](https://bitbucket.org/erikfalor/su19-cs1400-lecturenotes/src/master/)

*   Running the current file
    -   PyCharm can open multiple files at once
    -   You need to state which file is the one you want to run
        -   This is called a *run configuration*
        -   You can create a run configuration manually, but it's kind klunky
    -   Easy Option #1
        -   Right click on file tab and choose run
        -   This option will show the current file
    -   Easy Option #2
        -   Use shortcut Shift + Control + F10
        -   This will run the current file


*   Running a Python file when it's not the *current* file
    -   This works after you've created a run configuration
    -   The currently configured file is shown in the drop-down at the top of
        the screen
    -   Hit Run button (Play button)
    -   Run > Run
    -   Shift + F10
    -   You can conveniently switch between running `Task1.py` and `Task2.py`
        this way

After you have written your programs *always* follow the steps outlined on
Canvas in the document titled **How to Submit Assignments**.  This is the
*most* important part of your homework algorithm, and yet it's the part which
most students overlook.



----------------------------------------------------------------------------
# What is a Computer? (Textbook Ch. 1.2)

*   Bits & Bytes
    -   Binary 01000010 01101001 01101110 01100001 01110010 01111001 00001010
        -   Computers really only understand 1's and 0's (like a switch)
        -   Data is processed in this way
        -   We use a base-10 numbering system in our society
        -   Binary is a base-2 numbering system
        -   Count on our fingers
            -   It takes both of your hands to represent one decimal digit
            -   If you learn to count on your fingers in binary you can go to
                31 on one hand
            -   How high can you count on both hands?
        -   Recognize numbers
            -   32-bit and 64-bit systems
            -   Computer marketing has familiar numbers (powers of 2)
    -   Bit
        -   Symbol is b (lowercase)
        -   A single digit
        -   1 or 0 (on or off)
    -   Byte
        -   Symbol is B (uppercase)
        -   8 bits per byte (by convention)
        -   How many base-10 numbers can be represented in a byte?
            -   8 digits, each with two possibilities
            -   2*2*2*2*2*2*2*2 = 2 ^ 8 = 256
            -   This gives the range 0-255
    -   Prefixes
        -   Remember this is binary
        -   KB - Kilobyte
            -   ~1,000 bytes
            -   2 ^ 10 = 1,024
            -   Not 1,000
        -   MB - Megabyte
            -   ~1,000,000 bytes
            -   2 ^ 20 = 1,048,576
            -   Not 1,000 ^ 2
        -   GB - Gigabyte
            -   ~1,000,000,000 bytes
            -   2 ^ 30 = 1,073,741,824
            -   Not 1,000 ^ 3
        -   TB - Terabyte
            -   ~1,000,000,000,000 bytes
            -   2^40 = 1,099,511,627,776
            -   Not 1,000 ^ 4


*   CPU
    -   Central Processing Unit
    -   The *brain* of the computer
    -   Executes instructions
    -   Consists of 10 parts (see what I did there?)
        1.   control - coordinates other components
        2.   arithmetic/logic - performs calculations
    -   Speed
        -   Measured in hertz (per second) which is frequency
        -   Today CPUs are measured in Gigahertz, or billions of operations per second
    -   Multi-CPU/Multi-Core
        -   Can do multiple things at once
    -   The CPU has a few very small but very fast storage spaces, just like your mind


*   Memory
    -   The work area for a computer
    -   This is like the desk space of an office
    -   A sequence of bytes that can store
        -   Data
        -   Instructions (code)
        -   Addresses
    -   Each memory location has an address
    -   RAM - Random Access Memory
    -   Slow as compared to the CPU
    -   Large as compared to CPU
    -   Small as compared to storage
    -   Measured in GB today
    -   This is volatile
        -   All data disappears when the computer is turned off


*   Storage
    -   Long-term place to keep data
    -   Hard drives, USB drives, etc.
    -   Non-volatile
        -   Data does not disappear when power is turned off
    -   Very large
    -   Very slow when compared to CPU and Memory
    -   Measured in GB or TB today
    -   Like a bookshelf in an office


*   Mind, Desk, Bookshelf analogy
    -   Quick access to a few things in *mind*
    -   Slower access to more things on your *desk*
    -   Very slow access to even more things on a *bookshelf*


*   I/O (Input/Output)
    -   Anything that is used to interface with the computer
    -   A computer is merely a fancy space-heater if it cannot do I/O
    -   Input
        -   Keyboard
        -   Mouse
        -   Accelerometer 
        -   Thermistor
        -   Game controller
        -   Microphone
        -   GPS Receiver
        -   Camera
    -   Output
        -   Monitor
        -   Printer
        -   Speakers
        -   LED indicators
        -   Insulin Pump
    -   Both
        -   Networking cards/WiFi radio
        -   Bluetooth
        -   Force-feedback game controller
        -   Touch screen
