# Required comment
# CS1400
# Task1.py
# Assignment #1
# Erik  Falor

# This is me being sassy
print("Hello world")
print("Then # I said:")
print("'That's what she said'")


# TODO: finish my Hello World ASCII-art banner
print("*    *  ******")
print("*    *  *")
print("******  *****")
print("*    *  *")
print("*    *  ******")


# A multiplication table
# It feels very repetitive and tedious.
# I'm sure that this can be improved upon...
print("2 * 1 =", 2 * 1)
print("2 * 2 =", 2 * 2)
print("2 * 3 =", 2 * 3)

# This line of code is giving me a NameError...
# Print("2 * 4 =", 2 * 4)

# Farenheit Temperature conversion
print("Outside it is", 5 / 9 * (54 - 32), " Celsius")


print("Protip: use Ctrl+Shift+F10 to quickly run your program")
