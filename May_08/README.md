# CS1400 - Wed May 08

# Topics:

* Operating Systems (Textbook Ch. 1.4)
* Writing Python files (Textbook Ch. 1.6)
* Programming Errors (Textbook Ch. 1.8)



----------------------------------------------------------------------------
# Operating Systems (Textbook Ch. 1.4)

Yesterday we learned about the different hardware components that make up a
computer system.  Programmers' work is to orchestrate the operation of this
hardware by writing sets of instructions.

Actually, this is just a bit of a lie: your programs can't directly do
*anything* to the hardware.  The Operating System (OS) is a program that lies
in between the hardware and applications.

The purpose of the OS is to

*   Make using the hardware easier
*   Smooth over the differences between different computers

![User un-friendly hardware](ENIAC_function_table_at_Aberdeen.jpg)

Your program makes requests of the OS instead of directly connecting to and
controlling your computer's hardware.

Far from being a pointless middleman, an OS makes using different computers
easy.  Instead of taking into account all of the different types of hard disks,
keyboards, and mice that your program may need to interface with, your program
deals with the OS which provides a consistent interface.

Put another way, the only programmers who actually need to bother with the
details of all of the possible kinds of hardware are those who work on the OS.
The rest of us get to enjoy the fruits of their labor.


## Principle of Abstraction

This is a theme you'll find repeated everywhere in Computer Science.
*Abstraction* means to remove complicated details, leaving only the bare
essentials.

![Picasso Bull Plate 01](bulls/picasso_bull_plate_01.jpg)

![Picasso Bull Plate 02](bulls/picasso_bull_plate_02.jpg)

![Picasso Bull Plate 03](bulls/picasso_bull_plate_03.jpg)

![Picasso Bull Plate 04](bulls/picasso_bull_plate_04.jpg)

![Picasso Bull Plate 05](bulls/picasso_bull_plate_05.jpg)

![Picasso Bull Plate 06](bulls/picasso_bull_plate_06.jpg)

![Picasso Bull Plate 07](bulls/picasso_bull_plate_07.jpg)

![Picasso Bull Plate 08](bulls/picasso_bull_plate_08.jpg)

![Picasso Bull Plate 09](bulls/picasso_bull_plate_09.jpg)

![Picasso Bull Plate 10](bulls/picasso_bull_plate_10.jpg)

![Picasso Bull Plate 11](bulls/picasso_bull_plate_11.jpg)

This is the only way to get anthing done when you work in such fantastically
complicated systems.

### TL;DR

Any time your program does anything besides arithmetic and logic the OS is involved.



## Popular modern OSes

*   Linux/Unix
*   Windows
*   Android
*   OSX
*   iOS


## Three major tasks of an Operating System

1.   Controlling and monitoring system activities
    -   I/O
    -   File management
    -   Controlling peripheral devices like disks & printers

2.   Allocating and managing system resources
    -   Determines resource needs of processes
        -   Storage space
        -   CPU time
    -   Grants or denies permission to peripheral devices

3.   Scheduling operations
    -   Enables many processes to share the computer at once
    -   Concurrency
        -   The appearance of multiple things executing at once
        -   Processes take turns really, really quickly (multiprogramming)
    -   Parallelism
        -   Actual occurrence of multiple things executing at once
        -   Requires multiple CPUs
        -   One process can do N things at once (multithreading)
        -   N processes can run at once



----------------------------------------------------------------------------
# Writing Python files (Textbook Ch. 1.6)

Writing code is both a technical an artistic practice 

**Rules** = Things that *must* be done for the code to be read by the computer

**Conventions** = Things that are done to make life easier for humans reading the code

*   Indentation matters in this language
    -   Will not run properly if indentation is wrong
    -   Not true of many other languages
    -   This makes it a rule


*   Special Symbols
    -   There are many symbols that are part of the languages
    -   These can only be used for their specific purpose
        -   This makes it a rule
    -   Examples
        -   ()
        -   #
        -   " "
        -   ''' '''
        -   + - * / =

*   Comments
    -   We comment our code to make it easier to create and maintain
    -   It's like notes for what should happen to make the code easier to understand
    -   "The code comments itself" is a terrible phrase.  Code always does
        exactly what it says it will, even if the code has an error in it
    -   Comments are completely ignored by the interpreter/compiler.
    -   Their presence does *not* slow your program down


*   Single Line Comment (a.k.a. the hash mark `#`)


*   Multiline comments (a.k.a. *paragraph comment* in the book)
    -   `'''`
    -   Three quotes
    -   Start the line of comments with the `'''`
    -   End with a line with only `'''`
    -   Technically, these aren't comments because Python doesn't ignore them
        -   They must be indented to match the surrounding code
        -   In certain contexts they become documentaion within your program (docstrings)
    -   I recommend that you just use `#` for every line


*   Rule or convention?
    -   How commenting works is a rule
    -   When and how much comment is a convention


*   Commenting Conventions
    -   You should include comments for
        -   Each file at the top of the file should have a description
        -   For class it should also have:
            -   Name
            -   Course name, including section
            -   Assignment number
        -   Unique steps or processes
            -   State why you're doing something some specific way if it's unique
            -   State what is happening if it's very complicated
        -   We'll add more to the list as we learn


*   Style
    -   This deals with conventions to make our code readable
    -   Python indentations are a rule, where it is a convention in many other languages
        -   Use four spaces, *not* a tab
        -   This is set up automatically in PyCharm
        -   Only use tabs for indentation if the project is already using that approach and you're maintaining the code
    -   Spacing
        -   Place spaces around arithmetic symbols
            -   3 - 1 + 6
            -   Not: 3-1+6
            print("don't do this")

Follow the [Python Coding Conventions](https://usu.instructure.com/courses/537470/pages/python-coding-conventions)
posted on Canvas in this class - you're graded on it.


----------------------------------------------------------------------------
# Programming Errors (Textbook Ch. 1.8)

Errors occur when the code is not properly written (the rules) and the program
running the code doesn't know what to do

## Three types of errors

1.   Syntax
    -   Error in the code construction
    -   Examples
        -   Mistype a statement
        -   Incorrect indentation
        -   No closing "
        -   Not closing )
        -   And more
    -   These are very common
    -   Python gives a message to help you know what the problem is
    -   Remember: The error is apparent before the program is even run
    -   Example
        -   Code
            -   print("The program has started")
            -   print("This is a syntax error)
        -   Result:
            -   Error without printing first line

2.   Runtime
    -   Cause the program to abort (crash)
    -   Often caused by invalid input received while program was running
    -   Example
        -   User enters 0 as a denominator
        -   User enters a letter instead of a number for a calculation
    -   Python gives a message to help you know what the problem is
    -   Remember: The error occurs when the program is running
    -   Example
        -   Code
            -   print("The program has started")
            -   print("This is a runtime error")
            -   print(10 / 0)
        -   Result:
            -   First lines are printed until the error occurs

3.   Logic
    -   Occurs when the program does not behave properly
    -   There is an error in how the programmer wrote the logic, not in the syntax
    -   The program operates normally (from the computer's perspective)
    -   These errors are difficult to find
    -   Remember: No apparent error without analysis of results
    -   Example
        -   Code
            -   print("The program has started")
            -   print("This is a logic error")
            -   print("2 + 2 =", 2 - 2)
        -   Result:
            -   All lines are printed as if nothing is wrong
