# This one is wrong
# The adjacent string literals are concatenated together
print("My name is Assignment #1" "This is another string of text")


# It's the same thing as if I used + to join them together
print("My name is Assignment #1" + "This is another string of text")


# Using a triple-quoted string literal is the best way to approach Assn2
# because it preserves the newlines within the text, and I don't mix
# Python and English syntax in the same lines of code
print("""
My name is Assignment #1
This is another string of text
This is another sentance in my paragraph.
""")

# When to use single quotes vs. double quotes

# when my string contains a single quote, wrap it in double-quotes
print("I'm the best!")

# or vice-versa
print('He said "That is a dumb idea"')

# Use a backslash to "escape" a special character
# This will disable the special meaning of the single quote in this string
print('He said "That\'s what she said"')

# This is another situation where python's triple-quoted strings come to the rescue
print('''He said "That's what she said"''')
