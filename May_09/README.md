# CS1400 - Thu May 09

# Announcements

## Lecture notes are available on Bitbucket

I see many of you photographing the projector as I'm talking.  That's an
arduous way of capturing my lecture notes.  The easy way is to go straight to
the source:

https://bitbucket.org/erikfalor/su19-cs1400-lecturenotes/src/master/



## The .idea directory

You don't need to remove the `.idea/` directory from your zip files before
submission.

I don't need it, nor does its presence cause me any problems.

FYI, the `.idea/` directory is created by PyCharm and contains configuration
information about your project.



## Course reading schedule

I've updated the [Course Schedule](https://usu.instructure.com/courses/537470/pages/course-schedule)
on Canvas to match the 1st half of the semester.



## Exams available for scheduling

You can now schedule your timeslots for the exams with the
[Testing Center](https://rcde.usu.edu/mats/)




# Topics:

* What are Programming Languages? (Textbook Ch. 1.3)
* History of Python (Textbook Ch. 1.5)



----------------------------------------------------------------------------
# What are Programming Languages? (Textbook Ch. 1.3)

## Programs

A program is a list of instructions that tell the computer what to do.
It must be in a form a computer can understand.

The first computer was "programmed" by plugging wires into sockets.
![Two women operating ENIAC](Two_women_operating_ENIAC.gif)


## Machine Language

This is the CPU's native language.  Depends on the type of machine.  Once there
were lots of different CPUs, each with their own peculiar machine language.
Today, a few CPUs dominate computing:

1. Intel x86
2. ARM
3. MIPS
4. Atmel

Machine language is all in binary code.

Example: `01001000 10001001 11100101`

Can you imagine trying to write in binary?



## Assembly Language (ASM)

Use a short instruction (keyword) that can trivially be translated directly
into machine language.

Example: `mov    %rsp, %rbp`

This tells an Intel x86-64  CPU to copy the value in the Base Pointer register
into the Stack Pointer register

*   Easier than writing in binary!
*   Your computer does *not* understand `mov    %rsp, %rbp`
*   Assembler
    -   A program that translates Assembly to Machine language
*   Considered a low-level language
    -   You're telling the computer exactly what to do step-by-step
    -   It is very similar to machine language, except readable (allegedly)


## High Level Languages (HLL)

These came about in the 1950's.  Programs written in a HLL are called *source
code* or *code*.

*   Platform independent
    -   You can use the same source code on many machines
    -   Even if they each understand a different machine language

*   More "English-like"
    -   Much easier for humans to learn and use
    -   Brief human-friendly statements are translated into many machine instructions
    -   Example: Compute the area of a circle of radius 5

```
area = 5 * 5 * 3.14
```

This single line of code expands into this mess in Intel x86-64 Assembly
language:

```
movss  xmm0,DWORD PTR [rbp-0x4]
mulss  xmm0,DWORD PTR [rbp-0x4]
cvtss2sd xmm1,xmm0
movsd  xmm0,QWORD PTR [rip+0x0]
mulsd  xmm0,xmm1
cvtsd2ss xmm2,xmm0
movss  DWORD PTR [rbp-0x8],xmm2
```


## Translating a HLL into machine code

There are two ways a high-level language is converted into machine-code
instructions that the CPU can carry out.

1. A Compiler ![](compiler.png)
    *   Translates the source code program into an equivalent machine-code program
    *   The machine-code program is what the CPU actually executes
    *   Similar to compiling information into a single source


2. An Interpreter ![](interpreter.png)
    *   The Interpreter is a machine-code program
    *   Reads the source-code of another language statement-by-statement and
        immediately executes each statement
    *   Similar to interpreting a human language in real-time


See the [example programs](hello_world/) in the sub directory named `hello_world/`



----------------------------------------------------------------------------
# History of Python (Textbook Ch. 1.5)

Python was created by Guido van Rossum in the Netherlands in 1990.

It is named after **Monty Python's Flying Circus**, and *not* the snake.

Python is a general purpose programming languge.  This means that it is
suitable for many different applications.  Examples include:

*   Google search engine
*   NASA
*   New York Stock Exchange
*   Air Traffic Control
*   Scientific computing (You can find many opportunities to collaborate in
    research if you know Python)
*   Machine Learning
*   Video games
*   Web sites

Python is an interpreted language, meaning that the CPU does not directly
execute your programs.  Python requires an interpreter (a.k.a. Virtual Machine)
to read your source code and carry out your instructions.

Python is a multi-paradigm language, meaning that it draws inspiration and
features from various influential styles of programming.  This semester you
will become most acquainted with these aspects of the language:

-   Imperative
-   Procedural
-   Object-oriented

Python is freely available, both in terms of cost and your freedom to hack on it

-   Python Software Foundation
-   Developed and maintained by volunteers
-   Python 2 and Python 3 are available
    -   Python 2 reaches its end-of-life at the end of 2019
-   Python 3 is not backwards compatible to Python 2
-   We are using Python 3
