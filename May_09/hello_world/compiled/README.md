# Compiled Programming Languages

The programs contained in these source code files must be translated into
machine code by a program called a *compiler* (or an Assembler in the case of
the Assembly languages).

[Rosetta Code: Hello world](http://www.rosettacode.org/wiki/Hello_world/Text)

If you don't have these compilers installed on your computer, you can try these
and many other languages out on [Ideone](https://ideone.com/)

| Source File     | Language
|-----------------|----------
|hello_ada.adb    | Ada
|hello_arm.s      | ARM Assembly
|hello_x86-64.s   | Intel X86-64 Assembly
|hello_c.c        | C
|hello_cpp.cpp    | C++
|hello_cobol.cob  | COBOL
|hello_fortran.f90| Fortran 90
|hello_go.go      | Google Go
|hello_haskell.hs | Haskell
|hello_java.java  | Java
|hello_rust.rs    | Rust
