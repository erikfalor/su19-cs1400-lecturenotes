# Interpreted Programming Languages

The programs contained in these source code files are taken as input for
another program called an *interpreter*.  You can think of an interpreter as a
program that pretends to be a computer; like a CPU it reads and executes
instructions.  People often refer to interpreters as *Virtual Machines* (though
these are VMs in a different sense than a program such as VirtualBox or VMWare
Player).  Usually, an interpreter is itself a machine-code executable.


[Rosetta Code: Hello world](http://www.rosettacode.org/wiki/Hello_world/Text)

If you don't have these interpreters installed on your computer, you can try
these and many other languages out on [Ideone](https://ideone.com/)

| Source File          | Language
|----------------------|----------
|hello_awk.awk         | AWK
|hello_bourne_shell.sh | Bourne Shell
|hello_javascript.js   | JavaScript
|hello_lisp.lsp        | LISP
|hello_lua.lua         | Lua
|Makefile              | Make
|hello_perl.pl         | Perl
|hello_php.php         | PHP
|hello_postscript.ps   | PostScript
|hello_prolog.pl       | Prolog
|hello_python2.py      | Python 2
|hello_python3.py      | Python 3
|hello_ruby.rb         | Ruby
|hello_scheme.scm      | Scheme
