# CS1400 - Fri May 10

# Topics:

* Functions
* Turtle Graphics (Textbook Ch. 1.9)
* Elementary Programming (Textbook Ch. 2.1 - 2.5)
* Design before you code


----------------------------------------------------------------------------
# Functions

* `print()` is an example of a *function*, which is a named group of instructions

* A function is used to group multiple instructions together so they can be
  treated as a single instruction
    - Functions allow us to conveniently perform the same group of actions
      multiple times and from multiple places in your program
    - Otherwise you would repeat yourself needlessly

*   A function can return a value
    -   If a value is returned then the value is used in the place of the function

*   Using a function is called "calling a function"

*   Inside the parentheses we can give values to the function
    -   These values are called *arguments* or *parameters* 
    -   Arguments are inputs to a function
    -   A function's behavior is controlled by the input values given to it


We have begun learning Python by *calling* functions, but soon you will be
defining your own functions.


----------------------------------------------------------------------------
# Turtle Graphics (Textbook Ch. 1.9)

Turtle is a simple graphics tool to draw graphics which (hopefully) makes our
programs a little more interesting.

Using Turtle is like using a pen to draw on a screen.  We tell the turtle what
to draw by calling Python functions contained in the turtle *module*.

A module is a selection of code from a library that can be used in by other
programs.  Modules make it possible to use code over and over across many
projects.  Modules are the building blocks from which software professionals
create programs.  Part of becoming a good programmer is learning what modules
are in your library, and how to use them.

## Using the Turtle module

We'll first take our turtle for a spin in the REPL, then we'll write a program.

*   Import the module
    -   In order to use a module we need to *import* it with the `import` keyword
    -   The name of our module is "turtle", so we write  `import turtle`
    -   When writing a program this *must* be at the top of the file!

*   Draw stuff
    -   We tell the turtle to trace lines by calling functions the contained within
    -   These functions can make the turtle move in a line, or turn a corner.
    -   Here is how to make the turtle draw a box:

```
turtle.forward(100)
turtle.right(90)

turtle.forward(100)
turtle.right(90)

turtle.forward(100)
turtle.right(90)

turtle.forward(100)
turtle.right(90)
```

*   Write this as a program called `BoxTurtle.py`
    -   Our Turtle window disappears as soon as the program is done!
    -   Add `turtle.done()` to the bottom of our program to keep the window
        around long enough for us to enjoy our masterpiece


## Functions in the Turtle module

These are the capabilities that the Turtle module has prepared for us.
These are the ingredients from which we can build a drawing program.

While the names of the functions are self-explanatory, you are encouraged to
spend a little bit of time trying these out for yourself.  Remember, part of
becoming a good programmer is learning what modules are in your library, and
how to use them!

* `turtle.forward(distance)` takes a number
* `turtle.right(degrees)` takes a number
* `turtle.left(degrees)` takes a number
* `turtle.setheading(degrees)` takes a number
* `turtle.color(color)` takes a quoted string naming a color
* `turtle.penup()`
* `turtle.pendown()`
* `turtle.begin_fill()`
* `turtle.end_fill()`
* `turtle.goto(x, y)` takes two numbers
* `turtle.circle(radius)` takes a number

Turtle can do lots more!!!  How do you find out what else it can do?


## Turtle Documentation

The Turtle module is a part of the standard Python distribution.  Every time
you install the Python language from https://python.org you automatically get
Turtle.  Being a part of the standard Python distribution, we can read all
about it on [the Python website](https://docs.python.org/3/)

Find the documentation for the Turtle module in the section titled [Library
Reference](https://docs.python.org/3/library/index.html)


### Demo: draw a smiley face with Turtle



----------------------------------------------------------------------------
# Elementary Programming (Textbook Ch. 2.1 - 2.5)


## Important Terms
The textbook and I will throw a lot of new words at you all at once.  If ever
there is a word that you don't understand, please ask!  I guarantee that you
aren't the only one who is confused!

#### Algorithm

From Dictionary.com:
> A process or set of rules to be followed in calculations or other
> problem-solving operations, especially by a computer.

From Merriam-Webster
> A procedure for solving a mathematical problem (as of finding the
> greatest common divisor) in a finite number of steps that frequently involves
> repetition of an operation


#### Code (n)
> Noun - The programming language file used to create a program


#### Code (v)
> The act of writing a program


#### Pseudocode (n)
> A method of describing an algorithm as part of the planning/design stage

Pseudo-Greek for "False code"

Pseudocode is often a mixture of natural language and code.  Many programmers
regard the Python language as "executable pseudocode" because of its simple
appearance and lack of curly braces


#### Implement (v)
> To fulfill; perform; carry out: 

> To put into effect according to or by means of a definite plan or procedure. 




## Important programming concepts to study

I want you to pay special attention to these concepts as you read the text:

## Section 2.2
* Variable
* Data type
    * Number - Python recognizes at 3 types of numbers
        - Integral
        - Real
        - Complex


## Section 2.3
* `input()`
* `eval()`


## Section 2.4
* Identifier
* Keyword


## Section 2.5
* Statement
* Expression
* Assignment Operator - a.k.a the equals sign =
* Scope

The difference between a *Statement* and an *Expression* will be murky at
first.  The most simple explanation is that an *expression* may appear on the
*right-hand side* (RHS) of an assignment operator.  It doesn't make sense for a
*statement* to appear on the RHS.




----------------------------------------------------------------------------
# Design before you code

Your job as a programmer is to create algorithms and make a computer carry them
out.

You yourself can follow an algorithm to create algorithms.

* Begin with a rough outline of the solution.  You can design the logic of how
  a problem should be solved in pseudocode.  What you are designing in
  pseudocode is an algorithm.

* You will encounter gaps in your solution as you go.  These gaps are simply
  areas which need more thought.  Fill these gaps in your pesudocode and run
  through the logic from the top, making sure you have every situation covered.

* Repeat this process until your pseudocode is refined and complete.

* Rewrite the Pseudocode into a programming language to implement the solution.
  Your solution has become a version of the algorithm which can be realized on
  a computer.

* Test your program to ensure that it is indeed correct and complete.  Be on
  the lookout for gaps which you hadn't considered.  Be wary of things that
  were lost or embellished in translation.
