import turtle

# LEt's get there in a hurry
turtle.speed('fast')

# draw a yellow circle for the face
turtle.penup()
turtle.doesntExist()
turtle.goto(0, -150)
turtle.pendown()
turtle.color('yellow')
turtle.begin_fill()
turtle.circle(150)
turtle.end_fill()
print(turtle.position())

# draw a left eye, which is a smaller, black circle
turtle.penup()
turtle.goto(-50, 25)
turtle.pendown()
turtle.color('black')
turtle.begin_fill()
turtle.circle(25)
turtle.end_fill()

# draw a right eye, which is a smaller, black circle
turtle.penup()
turtle.goto(50, 25)
turtle.pendown()
turtle.begin_fill()
turtle.circle(25)
turtle.end_fill()


# draw a grin which will be a black arc
turtle.penup()
turtle.goto(-60, -60)
turtle.pendown()
turtle.setheading(315)
turtle.width(10)
turtle.circle(90, 90)

print(turtle.position())
turtle.hideturtle()
turtle.done()
