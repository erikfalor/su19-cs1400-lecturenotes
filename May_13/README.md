# CS1400 - Mon May 13

# Topics:
* Design a solution to a problem
* Identifiers (2.4)
* Variables, Assignments and Expressions (2.5 - 2.6)
* Getting input from the user (2.3)
* Improve our solution


----------------------------------------------------------------------------
# Design a solution to a problem

Write a program to display the circumference of a circle of radius `8.0`

1. Begin by outlining the algorithm in psuedocode
2. Translate this into Python (do your best!)


## Tell me what to write

As a class let's put this program into Python to see how it does.

* What is good about this solution?
* What could be improved?


## Add a few embellishments

Why is it important to follow coding conventions and strive for readable code?
It is because in practice you spend *much* more time reading code than you do
writing it.

* You will be asked to modify or add to code written by somebody else
* Even when you do write a program from scratch, more of your time is spent in
  editing, re-reading and rewriting your code than is spent writing it in the
  first place

Let's add a few new features to this program (customers are always doing this
sort of thing to you - your job is *never* done).

1. Print text telling the user the meaning of the number they're shown
2. Print the area of the circle


## Tell me what to write

* What is good about this solution?
* What could be improved?
* What if we want to make a program to tell us facts about a circle of a
  different size?



----------------------------------------------------------------------------
## Identifiers (2.4)


#### Identifier
> A name that identifies elements of a program

* variables
* constants
* functions
* and more!

The rules for forming identifiers are given in section 2.4.  You should choose
descriptive identifiers; they improve the readability of your code.

Additionally, identifiers cannot be keywords in the language.  A list of Python
keywords is given in Appendix A.



----------------------------------------------------------------------------
# Variables, Assignments and Expressions (2.5 - 2.6)


#### Variable (2.5)
> Names which represent values that may change in the program

A variable is a named location in a program's memory.




## Assignment (2.5)
> The operation which gives a variable a value

The `=` operator to assigns a value to a variable.  The value can come from
a literal, another variable, or as the result of calling a function.

Over the years many syntactical shorthand assignments have been developed, of
which Python possesses two.

* Section 2.7 introduces *Simultaneous Assignment* syntax

* Section 2.10 introduces *Augmented Assignment* operators which are commonly
  used in real-world code.




## Expression (2.5)
> A computation with values, variables, and operators that evaluate to a value



## Simultaneous Assignment (2.6)

You may assign many values to many variable in one line of code.

Separate variables on the left of assignment by commas, and separate
expressions on the right by commas.  Each value of the right hand side is put into the corresponding variable on the left.

All expressions on the right are evaluated first, then assigned

`a, b, c = 1 + 2, 2 + 3, 3 + 4`

This is equivalent to

```
a = 1 + 2
b = 2 + 3
c = 3 + 4
```

This is a great way to swap variables.  Without simultaneous assignment:

```
x = 1
y = 2
temp = x
x = y
y = temp
```

With simultaneous assignment:
`x, y = y, x`



----------------------------------------------------------------------------
# Getting input from the user (2.3)

The book remarks that many early programs fit a simple formula called *IPO*:

1. Gather input
2. Process the input
3. Produce output

Indeed, many real-world programs follow the *IPO* format.
Recognizing this format makes designing your programs much simpler!


## `input()`

*   This function gets input from the user
*   Inside () you can put a sentence that the user will be shown

`input("Enter a radius:")`



### Converting values between data types

There are a few ways to make data from `input()` become suitable for our
calculations.

The book frequently uses a function called `eval()` to do this.  It does work,
but is overkill for this purpose.  In CS1 you are allowed to use `eval()` as
our goal is to keep things simple, but later you'll be discouraged from doing
so.

We can take the return value from input and call eval() on it to turn it into a number

*   We can do this all at once `eval(input("Enter a number: "))`
*   `input()` returns a string, which is immediately used as a parameter for `eval()`
*   Now we have a number type that we can use to calculate the area and circumference



----------------------------------------------------------------------------
# Improve our solution

Let's take what we've learned and improve our circle facts program.


## Reflect

* Why is this version of the program better than what we started with?

* Are there any parts which you still don't think are very easy to understand?
