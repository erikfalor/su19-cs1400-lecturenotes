# Don't do this!!!
password = 'qwerty'

# I'm only concerned with 6 significant digits
PI = 3.14159

radius = float(input("Please give me a radius: "))

print("You entered a radius of", float(radius))

print("The circumference of this circle is",
      2 * PI * radius,
      "units")

print("The area of this circle is",
      PI * radius * radius,
      "units squared")
