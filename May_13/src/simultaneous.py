# simultaneous assignment; I'm giving two variables two different values
# on one line of code
PI, TAU = 3.14159, 3.14159 * 2.0

# Without simultaneous assignment, this takes two lines of code:
a = 7
b = 21

# With simultaneous assignment I can say it more briefly:
a, b = 7, 21

# simulataneous assingment is great if I want to swap a and b:
a, b = b, a

