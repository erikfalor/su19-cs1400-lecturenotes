# CS1400 - Tue May 14

# Topics: Ch. 2.5 - 2.12
* Scope (2.5)
* Types (2.8)
* Operators (2.8)





----------------------------------------------------------------------------
# Scope (2.5)

Scope determines where a variable or identifier can be used.  When a variable
is accessed "out of scope", Python raises a `NameError`.  The scope of a
variable is also known as its *lifetime*.

At this point you must understand that variables must be created before they
are used.



----------------------------------------------------------------------------
# Types (2.8)

Values are understood in terms of their *type*.  What a language permits you to
do with a value depends upon its type.  Here are a few literal values and their
associated type:


Value       | Type
------------|---------
`1`         | Integer
`1337`      | Integer
`-32`       | Integer
`3.14`      | Float
`.09`       | Float
`-1.0`      | Float
`-.42`      | Float
`10e2`      | Float
`314159e-5` | Float
`""`        | String
`"string"`  | String
`'string'`  | String
`True`      | Boolean
`False`     | Boolean
`None`      | None


You can use Python's `type()` function to discover the type of *any* value.



## Static vs. Dynamic Types

Last week we defined a dichotomy across which we categorize programming
languages: *Compiled vs. Interpreted*

There is another dividing line amongst the languages which is *Static vs.
Dynamic Types*.  This difference is in how types are related to variables in a
program.


### Statically Typed

A variable is declared to hold only 1 type of data throughout its scope.

By the rules of the language, it is not possible to store data of a different
type in that variable.

Statically typed languages include:
-----------------------------------
* Java
* C, C++, C#
* Haskell
* Go
* Rust


### Dynamically Typed

A variable in a dynamically typed language can hold data of any type.  As the
program progresses the type of data in a variable can evolve and change.

Dynamically typed languages still have the notion of type.  The type is
associated with the value instead of with the variable.

         |  Values   | Variables          
Language | have type | have type
---------|-----------|-----------
static   |   True    |   True
dynamic  |   True    |   False


Dynamically typed languages include:
------------------------------------
* Python
* Perl
* Ruby
* JavaScript
* Scheme/LISP



## Which is better, Static or Dynamic? 

You may have noticed that the statically typed languages are also the compiled
languages.  While this isn't universally true, it's *basically* true.

As with so many other things, it's a trade-off.  In general, static languages
can be written in a form that is more efficient for the computer to execute,
but dynamic languages make it easier for humans to express their ideas to the
computer more quickly.


----------------------------------------------------------------------------
# Operators (2.8)

#### Operator
> A symbol in a programming language which represents an action

#### Operand
> The values that an operator acts on

Operators are similar in concept to functions, but differ in their syntax.
Like functions, operators take parameters (called *operands*).  Most operators
take either one or two operands.

* Binary operator
	-   Uses two operands (one on each side of the operator)

* Unary operator
	-   Uses a single operand (on the right side of the operator)


A big part of learning a programming language is understanding these three
things:

1. Which operations are available
2. Which types can participate in each operation
3. How values of different types may be combined through operations


## Numeric Operators

- `+` Binary - addition
- `+` Unary - positive number (not commonly used)

- `-` Binary - subtraction
- `-` Unary - negatative number

- `*` Multiplication of numbers
- `*` Repeated concatenation when used with a str and an int

- `/` Floating-point division
    -   `1 / 2` => `0.5`

-   `//` Integer division
    -   This truncates the result to no decimal places
    -   `1 // 2` => `0`
    -   In other languages there is only one division operator
    -   Integer division happens when both operands are integers

-   `**` Exponentiation
    -   `4 ** 2` => `16`
    -   `16 ** 0.5` => `4.0`

-   `%` Modulus, Modulo, Mod
    - When both operands share their sign this is the same as taking the remainder
    - Gives a different result than remainder when the operands differ in sign
        - `15 % 4` => `3`
        - `-15 % -4` => `-3`
        - `-15 % 4` => `1`
        - `15 % -4` => `-1`


## Modulo examples

I promised you that `%` is really handy and is used all the time in real-world
code.  Here are a few places where it is truly useful


### Day of the week calculator

Encode the day of the week using an integer (0 = Sunday, 1 = Monday, etc.)

If today is a Tuesday, what day of the week will it be in N days?

See the code under `src/dayOfWeekCalculator.py`

### Microwave seconds converter

Given a time expressed as seconds, convert into minutes and seconds

Find the code under `src/microwave.py`
