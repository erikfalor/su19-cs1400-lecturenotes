from math import sqrt

a, b = 3, 4
cSquared = a*a + b*b
print("cSquared is ", cSquared)
c = sqrt(cSquared)
print("that means c must be", c)