# Encode the day of the week using an integer (0 = Sunday, 1 = Monday, etc.)

# If today is a Tuesday, what day of the week will it be in N days?

DAYS_IN_WEEK = 7

today = 2


n = input("How many days from today do you want to know the DoW? ")
n = int(n)

print("The type of the variable 'n' is", type(n))

dow = (today + n) % DAYS_IN_WEEK
print("In", n, "days it will be", dow)