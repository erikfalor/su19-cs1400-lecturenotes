# Given a time expressed as seconds, convert into minutes and seconds

# need a constant for a mintue (it's 60 seconds)
SECONDS_IN_MINUTE = 60

# get seconds from the user, convert to an integer
totalSeconds = eval(input("Gimme some seconds, or no burrito for you! "))

# divide by seconds_in_minute
minutes = totalSeconds // SECONDS_IN_MINUTE
seconds = totalSeconds % SECONDS_IN_MINUTE

print("Your burrito will be done in", minutes, ":", seconds)