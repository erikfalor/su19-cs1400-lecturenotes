# CS1400 - Wed May 15 - Lecture 08

# Topics:
* Questions raised on Assignment #2
* Order of Operations/Operator Precedence (2.9)
* Augmented Assignment Operators (2.10)
* Type Conversions and Rounding (2.11)
* The Software Development Process (2.13)
* Programming Exercises


----------------------------------------------------------------------------
# Questions raised on Assignment #2

> When we use the command prompt in Windows, for example, what language are we
> using to talk to the computer (if any)?

The language of the command interpreter.  In the Unix tradition these languages
are known as "Shell" languages.


> I do have a question on whether computers can learn though.

*Machine Learning* (ML) has become a big deal in the last few years, especially
since the release of freely-available open source code libraries such as
Google's TensorFlow.  TensorFlow is a Python library, so you're mere months
away from being able to write your own ML code!


> I never knew that Computer Science was so much like other sciences.

While most of us are here to learn the skill of coding for its economic
benefits, CS as a discipline is concerned with learning what is computable, and
by extension, what is knowable.  A lot of research in CS is done in
collaboration with researchers across all disciplines.


> I was listening to a podcast last night that actually had to do with the
> computer voting system in Belgium and how there was a mysterious vote
> miscount on the machine. ... theorized to have happened from a gamma
> particle from space that hit that computer at the exact right time

And now you know why I'm very skeptical of electronic voting systems!


----------------------------------------------------------------------------
# Order of Operations/Operator Precedence (2.9)

Just like you learned in elementary school, Python's operators obey a heirarchy
of [precedence](https://docs.python.org/3/reference/expressions.html#operator-precedence).

1. Grouping `( )`
2. Unary `+`, `-`
3. Exponentiation `**`
4. Multiplication and division ops `*`, `/`, `//`, `%`
    - These operate from left to right when there are many
5. Addition and subtraction `+`, `-`
    - These operate from left to right when there are many



----------------------------------------------------------------------------
# Augmented Assignment Operators (2.10)

Changing a variable's value is a very common need.  You'll find yourself
writing code like this all the time:

    # Increment `a` by one
    a = a + 1

    # Decrement `b` by two
    b = b - 2

It is so common that a shorthand syntax was devised long ago.  Augmented
assignment operators are binary operators which combine the assignment symbol
with an arithmetic operation into one

    # Increment `a` by one
    a += 1

    # Decrement `b` by two
    b -= 2

This shortcut also removes the redundancy of repeating the variable name.
    

Operator   | Meaning
-----------|-----------------------------
`+=`       | Add and assign
`-=`       | Substract and assign
`*=`       | Multiply and assign
`/=`       | Floating-point divide and assign
`//=`      | Integer divide and assign 
`%=`       | Modulo and assign 
`**=`      | Exponentiation and assign



----------------------------------------------------------------------------
# Type Conversions and Rounding (2.11)

The functions `int()` and `float()` convert Strings and other numbers into
Integers and Floating-Point numbers, respectively.

* When you know that you want an Integer number, use `int()` instead of
  `eval()`

* `int()` converts a floating-point value to an integer by *truncating* the
  fractional part

* When you need a floating-point number in your program, use `float()`

* Use `round()` to convert a float to the nearest int using the familar
  rounding rule.

* Convert a number back into a String with `str()`
    - Useful when you need to concatenate a string to a number



----------------------------------------------------------------------------
# The Software Development Process (2.13)

Software development is a multi-stage process.  It is important to follow
through with each step in its turn to build *reliable*, *robust*, and *secure*
software products that can be maintained and updated.

#### Reliable
> Software reliability is the probability that software will work properly in a
> specified environment and for a given amount of time

#### Robust
> Software robustness is the ability of a computer system to cope with errors
> during execution and cope with erroneous input

#### Secure
> Software is secure when it is resilient against malicious attacks which may
> aim to cease operation of the software or exfiltrate sensitive information


## The Steps of the Software Development Process

![Software Development Process](Software+Development+Process.jpg)

1.  Requirements Specification
    -   Seek to understand the problem
    -   Document in detail what the software needs to do
    -   Involves end-users and developers
    -   Problems we work on are simplistic. This is not the case in the real world

2.  System Analysis
    -   Identify inputs and outputs
    -   Models the behavior of the system based on flow from input to output
    -   Often easier to identify needed output, and then figure out what input
        is needed and where it is located

3.  System Design
    -   Identify the steps of the program (pseudocode)
    -   Process of designing (like a blueprint) the system components
    -   Decomposes a problem into smaller components
    -   Identify the relationship between input and output (and respective components)
    -   *NOTE:* The essence of system analysis and design is input, process, output (Called IPO)

4.  Implementation
    -   Translate the design into programs (coding)
    -   Write each component and then the code to tie them together
    -   Implementation consists of
        -   Coding
        -   Testing (by the developer)
        -   Debugging (fixing errors)

5.  Testing
    -   Independent team of software engineers (not those who created the program)
    -   Check to see if requirements specifications have been met

6.  Deployment
    -   Make the software available for a wide audience to use
    -   Also called release

7.  Maintenance
    -   Updating and improving the product
    -   Fixes newly discovered errors
    -   Add new functionality



----------------------------------------------------------------------------
# Programming Exercises

You will write up and submit a SDP as part of your work on Assignment #6 and
other assignments later on.  Let's follow the SDP as we tackle these problems
together in class.


## Sales tax calculator

* Ask for purchase amount and sales tax rate
* Calculate sales tax amount
* Print sales tax along with total purchase price

Work in groups, writing down the outcome of the first three steps of the SDP.

1.  Requirements Specification
2.  System Analysis
3.  System Design



## Listing 2.8 `ComputeLoan.py`

* Let the user enter the interest rate, the loan amount, and the number of
  years for which payments will be made.
* Compute and display the monthly payment and total payment amounts according to the formula

    loan amount * monthly interest rate / (1 - (1 / (1 + monthly interest rate) ** number of months))

Work in groups, writing down the outcome of the first three steps of the SDP.

1.  Requirements Specification
2.  System Analysis
3.  System Design
