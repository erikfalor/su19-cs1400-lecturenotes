
print("Count to ten")
count = 1
print(count)

count += 1
print(count)

count += 1
print(count)

count += 1
print(count)

count += 1
print(count)

count += 1
print(count)

count += 1
print(count)

count += 1
print(count)

count += 1
print(count)

count += 1
print(count)

print()
print("Double a number with +=")
count = 1
print(count)

count += count
print(count)

count += count
print(count)

count += count
print(count)

count += count
print(count)

print("Double that number with *=")
count *= 2
print(count)

count *= 2
print(count)

count *= 2
print(count)

print()
print("Halve that number with integer division")
print(count)
count //= 2
print(count)

count //= 2
print(count)

count //= 2
print(count)

count //= 2
print(count)

count //= 2
print(count)

count //= 2
print(count)

count //= 2
print(count)
