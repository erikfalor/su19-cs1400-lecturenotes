# Sales tax calculator
#
#     Ask for purchase amount and sales tax rate
#     Calculate sales tax amount
#     Print sales tax along with total purchase price
#
# Work in groups, writing down the outcome of the first three steps of the SDP.
#
#    Requirements Specification
#    System Analysis
#    System Design

## INPUT
# take sales tax from user input()  6.25
salesTax = float(input("Sales tax rate: "))
cost = float(input("Cost of item(s): "))

## PROCESSING
# divide by 100
salesTax /= 100.0

amountOfTax = cost * salesTax
totalCost = cost + amountOfTax


# OUTPUT
print("The tax on this sale is", amountOfTax)
print("The subtotal is", cost)
print("The total is", totalCost)
