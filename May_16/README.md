# CS1400 - Thu May 16 - Lecture 09

# Announcements

## Recitations cancelled May 27-28 for Memorial Day

The midterm exam will be held on May 28 instead of our lecture and the recitation.



# Topics:
* Software Development Process Exercise
* Common Python Functions (3.2)
* Programming Problem #3.2: Compute Angles of a triangle


----------------------------------------------------------------------------
# Software Development Process Exercise

The Software Development Process is a good habit to follow:

* The essence of software engineering is to overcome complexity.  Your goal in
  the first 3 steps of the SDP should be to convert a big, complex, tangled
  mess of a problem into an orderly sequence of simple steps.

* At any step of the process you may get stuck.  This happens when you have not
  fully completed a previous step.  Return to the previous step and try again.

* All of the "modern" software development processes are variations on this
  idea.  Despite the differences in nomenclature and speed/frequency, this is
  the essential formula your coworkers/boss will expect you follow throughout
  your career.

To avoid plagiarism (or the appearance thereof), observe a **strict**
separation between steps 3 and 4.  Speaking for my classes, you may work
together on steps 1-3 but I expect you to complete steps 4-6 *entirely* on your
own.  Specifically, don't share or show anybody your source code.

It is your responsibility to understand these rules in all of your classes from
all of your instructors.


## Implement and test the program described in listing 2.8 `ComputeLoan.py`

* Let the user enter the interest rate, the loan amount, and the number of
  years for which payments will be made.
* Compute and display the monthly payment and total payment amounts according to the formula

    loan amount * monthly interest rate / (1 - (1 / (1 + monthly interest rate) ** number of months))


## Practice following the SDP

1.  Requirements Specification
2.  System Analysis
3.  System Design
4.  Implementation
5.  Testing

* Work in groups, writing down the outcome of the first three steps of the
  Software Development Process.
* Together we will implement the Python program for step #4.  
* Once we have a working program, we'll devise test cases to convince ourselves
  that our program is correct.



----------------------------------------------------------------------------
# Common Python Functions (3.2)

-   We know a handful of functions already
    -   `print()`
    -   `input()`
    -   `eval()`
    -   `int()`
    -   `round()`
-   These functions are built-in to the language and available in the local
    scope, meaning that your code can refer to these functions at any time.
-   There is a complete list of additional built-in functions in the docs [Built-in Functions](https://docs.python.org/3.7/library/functions.html)
-   Useful built-in functions include
    -   `abs(x)` - absolute value of `x`
    -   `max(x1, x2, x3, ...)` - returns maximum argument
    -   `min(x1, x2, x3, ...)` - returns minimum argument
    -   `pow(a, b)` - Same as `a ** b`
    -   `round(x, n)` - returns float value `x` rounded to `n` digits after decimal


## More functionality is available in *modules*

#### Module

> A file containing Python definitions and statements.

- The file name of a module is the module name with the suffix `.py` appended.

- A module can contain executable statements as well as function definitions.
  Ordinarily, a module is used to organize collections of definitions.

- Modules are used in real-world solutions to conquer complexity by organizing
  related code into separate, smaller files.

- Modules enable commonly-needed parts of programs to be easily reused between
  many different solutions.

- To be used by your program, the definitions contained in a module must be
  **imported**
    -   [https://docs.python.org/3/library/functions.html#__import__](https://docs.python.org/3/library/functions.html#__import__)


Other functions are made available to your program by importing a module.

Python's `math` module is a commonly used module.

-   Import by writing `import math` at top of file
-   Now identifiers within the scope of the `math` module are available to your program
-   Complete list of available identifiers in the docs: [https://docs.python.org/3/library/math.html?highlight=math#module-math](https://docs.python.org/3/library/math.html?highlight=math#module-math)
-   Math module functions of note
    -   `math.fabs(x)` - return absolute value as a float
    -   `math.ceil(x)` - round up
    -   `math.floor(x)` - round down
    -   `math.sqrt(x)` - square root
    -   `math.factorial(x)` - factorial

-   Using the math module
    -   After importing the math module
        -   Refer to functions by prefixing the name of the math module followed by a period: `math.`
            -   `math.ceil(432.212)`
            -   `math.factorial(20)`
    -   You may import individual functions into the current namespace
        -   `from math import floor`
        -   Call as `floor(x)`, no need to prefix with `math.`

-   Mathematical Constants
    -   Some values are included in the math module
        -   `math.pi`
        -   `math.e`
        -   `math.tau` - equal to 2 pi
        -   `math.inf` - infinity
        -   `math.nan` - NaN/Not a number
    -   Usage
        -   `area = math.pi * radius * radius`
        -   `y = pow(math.e, x)`
    -   Yes, there's inconsistency with PEP 8 and how these "constants" are named
    -   Even Python has a few rough edges!
