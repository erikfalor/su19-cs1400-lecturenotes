#    Requirements Specification
#     Let the user enter the interest rate, the loan amount,
#        and the number of years for which payments will be made.
#     Compute and display the monthly payment and total payment amounts according to the formula
#

#    Implementation
# Inputs:
annualRate = float(input("Enter the annual interest rate (e.g. 4.5): "))
loanAmount = float(input("Enter the total loan amount: "))
years = float(input("Enter the number of years: "))

MONTHS_PER_YEAR = 12

# Processing:
annualRate /= 100.0
monthlyRate = annualRate / MONTHS_PER_YEAR
totalMonths = years * MONTHS_PER_YEAR

# Processing / Outputs:
monthlyPayment = loanAmount * monthlyRate \
               / (1 - (1 / (1 + monthlyRate) ** totalMonths))

totalPayment = monthlyPayment * totalMonths

# Output to the human
print("The monthly payment will be $", monthlyPayment)
print("The total cost of the loan will be $", totalPayment)

#    Testing
# Test case # 1
#  Enter the annual interest rate (e.g. 4.5): 5.75
#  Enter the total loan amount: 250000
#  Enter the number of years: 15
#  The monthly payment will be $ 2076.025217549142
#  The total cost of the loan will be $ 373684.53915884555
