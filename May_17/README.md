# CS1400 - Fri May 17 - Lecture 10

# Topics:
* Programming Problem #3.2: Compute Angles of a triangle
* Strings & Characters
* The Representation of Characters in Computers
* Functions for Characters
* Character Escape Sequences
* Printing strings without a newline at the end




----------------------------------------------------------------------------
# Programming Problem #3.2: Compute Angles of a triangle

Follow the SDP as we come up with our own implementation of a solution to this
problem.

![Triangle](Triangle.png)

Given coords for 3 verticies, compute the angles of the triangle ABC using
these formulas:

```
a = √[(x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3)]
b = √[(x1 - x3) * (x1 - x3) + (y1 - y3) * (y1 - y3)]
c = √[(x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)]

A = acos(a**2 - b**2 - c**2) / (-2 ∗ b ∗ c)
B = acos(b**2 - a**2 - c**2) / (-2 ∗ a ∗ c)
C = acos(c**2 - b**2 - a**2) / (-2 ∗ a ∗ b)
```

We will need to find an implementation of the `acos()` function somewhere...

1.  Requirements Specification
2.  System Analysis
3.  System Design
4.  Implementation
5.  Testing



----------------------------------------------------------------------------
# Strings & Characters

#### String
> A string is a sequence of characters

Python treats individual characters and strings the same way.

I raise this point because some programming languages distinguish between
individual *characters* and *strings* to the point of having different syntax
for each (i.e. using single quotes for characters and double quotes for
strings).


* A string containing one character:
    - 'H' 
    - "H"
    - ' '
    - " "
    - """ """

* A string of many characters:
    - 'Hello world'
    - "Hello world"
    - """Hello world"""
    
* The empty string
    - ''
    - ""
    - """"""

## String Rule
-   A string is a sequence of characters enclosed by " ", ' ', or these quote marks tripled

## String Convention
-   The convention is to be consistent in usage
-   We will use " " in class, except when the string value contains a single quote



----------------------------------------------------------------------------
# The Representation of Characters in Computers

-   Computers only understand 1's and 0's
-   Characters are represented by a code of 1's and 0's
    -   This is called Character Encoding
-   Common Character Codes
    -   ASCII Code
        -   7-bit encoding scheme
        -   2^7 = 128 possibilities
        -   Values are 0 - 127
        -   Appendix B shows the character set
    -   Unicode
        -   ASCII is a smaller subset of Unicode
        -   There are many more possible characters than 128
        -   There are a few different sizes/standards
            -   Common is UTF-16
        -   Written as \uXXXX
            -   X is a hexidecimal character
            -   Hexidecimal is base 16
                -   0 - F are the characters
                -   That is 16 possible characters
        -   That means that each character is 4-bits
        -   So Unicode is 4 4-bit characters or 16-bits
            -   That 65,536 possibilities
        -   print("\u03b1")
            -   prints alpha
-   Character encoding enables
    -   Characters in other languages
    -   Emojis
    -   Etc.


----------------------------------------------------------------------------
# Functions for Characters
-   ord(x)
    -   returns the ASCII/Unicode code for a character
    -   ord stands for ordinal - a numeric ordering
-   chr(x)
    -   returns the character represented by the ASCII/Unicode code
    -   Enter unicode as 0xFFFF
-   Example 1
    -   Can do this from the commandline too
    -   `ch = 'a'`
    -   `ord(ch)` # prints 97
    -   `chr(98)` # prints b
    -   `ord("a")` - `ord("A")` # prints 32
    -   `lowercase = "h"`
    -   `chr(ord(lowercase) - 32)` # prints H
-   Example 2
    -   `print(chr(ord("\u03b1"))`
    -   prints α (lowercase alpha)
    -   Others
        -   `"\u6B22"`
        -   `"\u8FCE"`
        -   `"\u03b1"`



----------------------------------------------------------------------------
# Character Escape Sequences

-   Sometimes we need a string that uses a character that is part of the language
-   Example
    -   `print("He said, "I do not like green eggs and ham!"")`
    -   This will have an error because " is part of the language
-   Escape sequences give us more control over strings
-   Use the backslash `\` to begin an escape sequence
-   Examples
    -   `\a` - bell character - makes the text console beep (a.k.a. the "alert" character)
    -   `\b` - backspace
    -   `\t` - tab
    -   `\n` - new line, line break, end-of-line (EOL)
    -   `\r` - carriage return, move to first position on same line
    -   `\\` - backslash
    -   `\'` - single quote
    -   `\"` - double quote
-   Code Examples
    -   `print("He said, \"I do not like green eggs and ham.\"")`
    -   `print("This is one line.\nThis is another")`
    -   `print("I just copied over\rmyself")`



----------------------------------------------------------------------------
# Printing strings without a newline at the end

-   Python automatically outputs a newline at the end of the last argument
    passed to `print()`

-   Other languages use different print functions to enable/disable this
    behavior:
    -   `print()`   - don't append a newline after output
    -   `println()` - appends a newline after output

-   This is controlled in Python by using the `end=''` parameter to `print()`
-   Example
    -   `print("Hello", end="not end")`
    -   `print("There")`
    -   Output: `Hellonot endThere`
-   The value of the `end` parameter is printed instead of a newline
    -   `end=""` results in no newline and no character printed


## Other print/formatting considerations

`str()` - Review
-   Convert a number type to a string type
-   str(4.5) turns into "4.5"
-   str(4) turns into "4"
-   It works for int or float types

Concatenation - Review
-   The `+` operator performs concatenation when its operands are both strings
-   `+=` also works
-   `msg = "Hello" + "There"`
    -   msg then would be `"HelloThere"`
    -   A space is *not* inserted like with the comma separated parameters in print
