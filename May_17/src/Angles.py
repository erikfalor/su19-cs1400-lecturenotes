from math import sqrt, acos, degrees

# Input step
# Given coords for 3 verticies
x1, y1, x2, y2, x3, y3 = eval(input("Give me 3 pairs of coordinates: "))

# x1, y1, x2, y2, x3, y3 = 1, 1, 6.5, 1, 6.5, 2.5


# Processing step
# compute the angles of the triangle ABC using these formulas

# Convert x,y coords into line lengths
a = sqrt((x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3))
b = sqrt((x1 - x3) * (x1 - x3) + (y1 - y3) * (y1 - y3))
c = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))

# Convert line lengths into Angles
A = degrees(acos((a**2 - b**2 - c**2) / (-2 * b * c)))
B = degrees(acos((b**2 - a**2 - c**2) / (-2 * a * c)))
C = degrees(acos((c**2 - b**2 - a**2) / (-2 * a * b)))

# Output Step
# Display the results to the user
print("The three angles are A:", A, "B:", B, "C:", C)