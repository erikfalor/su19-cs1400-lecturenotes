# CS1400 - Mon May 20

# Announcements

## Tutor lab hours on this Friday, May 24th

Bryan will be unavailable this Friday, so the tutor lab will be open from 1:00
to 4:00 on Friday, May 24th. 


# Topics:
* Programming Problem - Case Study 3.4 - Minimum Number of Coins
* Print Formatting (3.6)
* Primitive Values and Objects (3.5)



----------------------------------------------------------------------------
# Programming Problem - Case Study 3.4 - Minimum Number of Coins


Develop a program that classifies a given amount of money into smaller monetary
units.  The program lets the user enter an amount as a floating-point value
representing a total in dollars and cents, and then outputs a report listing
the monetary equivalent in dollars, quar- ters, dimes, nickels, and pennies.

1. Requirements Specification
2. System Analysis
3. System Design
4. Implementation
5. Testing


## Test Cases
* `11.56` and compare the output with the book
* `1.25`
* `2.24`
* `3.29`
* `0.29`


----------------------------------------------------------------------------
# Print Formatting (3.6)

-   Change the layout/display of numbers and strings
-   Use the `format()` built-in function
-   [Format Specification Mini-Language](https://docs.python.org/3/library/string.html#format-specification-mini-language)
-   [Format examples](https://docs.python.org/3/library/string.html#formatexamples)


## Using `format()`
This is a built-in method which takes two parameters:

1. A value to format
2. A format specifier string

`format()` returns a string which can be concatenated like any other string


### Format specifier
Format specifiers follow this template: `<alignment><sign><width>.<precision><type>`

*   Alignment
    -   `<` left aligned
    -   `>` right aligned
    -   `=` padding after sign, but before digits
    -   `^` center

*   Sign
    -   `+` sign used for positive and negative
    -   `-` sign used only for negative numbers
    -   `[space]` leading space on positive numbers and - on negative numbers

*   Width
    -   specify the minimum field width

*   Precision
    -   decimal number for how many digits after decimal should be shown for floating point numbers

*   Type
    -   String
        -   `s` or nothing: string

    -   Integer
        -   `b`: binary or base 2
        -   `d`: decimal or base 10 integer (not a float)
        -   `x`: hex
        -   `X`: HEX
        -   `n` or nothing: same as d

    -   Float
        -   `e`: scientific notation
        -   `E`: SCIENTIFIC NOTATION
        -   `f`: fixed point (specify how many decimals to show)
        -   `%`: percentage


## Format Examples

The structure of these statements is `print(format(value, format))`, where
`value` is a Python value of any type and `format` is a string containing a
format specifier code.


### Float (f)

*   `format(52.12312, "10.2f")`
    - `'     52.12'`
    - right justified with 10 total spaces
*   `format(42, "10.2f")`
    - `'     42.00'`
    - right justified with two decimal places
*   `format(52.12312, "<10.2f")`
    - `'52.12     '`
    - left justified with 10 total spaces


### Scientific Notation (e or E)
*   `format(57.467123, "10.2e")`
    - `'  5.75e+01'`
    -   right justified in 10 spaces
*   `format(57.467123, "10.2E")`
    - `'  5.75E+01'`
    -   Same as above with 'E' capitalized


### Percentage (%)
*   `format(0.12341, "10.2%")`
    -   `'    12.34%'`
    -   Same rounding applies (nearest positive)


### Alignment (<, ^, >, or =)
*   `format(523.1234, "<+10.2f")`
    -   `'+523.12   '`
*   `format(523.1234, "^+10.2f")`
    -   `' +523.12  '`
*   `format(523.1234, ">+10.2f")`
    -   `'   +523.12'`
*   `format(523.1234, "=+10.2f")`
    -   `'+   523.12'`


### Integers
*   `format(342134, "20d")`
    -   `'              342134'`
*   `format(342134, "20b")`
    -   `' 1010011100001110110'`


### Strings
*   `format("this is a string", "<20s")`
    -   `'this is a string    '`
*   `format("Hello, my name is Monty Python", "^40s")`
    -   `'     Hello, my name is Monty Python     '`



----------------------------------------------------------------------------
# Primitive Values and Objects (3.5)

## Overview

-   In Python all data are objects
-   Includes numbers and strings
-   Some languages have primitive values and reference values
-   Python just has reference values

-   Primitive
    -   This is kind of the atomic table of elements
        -   Cannot be broken down into something smaller
    -   In memory, the thing that is stored is the actual value

-   Reference
    -   Refers to another place in memory where data is actually stored
    -   In memory, it is a memory address that is stored
    -   Objects use references
    -   Pointers are very similar to references as they store an address for something


## Objects
-   Located in blocks of memory
-   Objects have
    -   **id**
        -   Each object has a unique id assigned upon creation
        -   Doesn't change during the execution of the program
        -   An object is not the variable, it's what the variable *references*
        -   You may use `id()` to inspect an object's internal ID

    -   **type**
        -   the kind of object something is
        -   Types we know
            -   `str`
            -   `int`
            -   `float`
        -   Many objects can be of the same type
        -   You may use `type()` to inspect an object's type

    -   **operations**
        -   Objects have capabilities and information (values)
        -   Capabilities are functions inside the object
        -   When functions are part of an object they're called methods
        -   Use the access operator `.` to call them


## String operations
As an example, let us consider objects of type `str`

-   `lower()` - return copy of string converted to lowercase
-   `upper()` - return copy of string converted to uppercase
-   `strip()` - copy of string w/ whitespace removed from beginning & end
-   `isdigit()` - detect whether the string's contents appear to be a number
-   ... and so much more! [Text Sequence Type `str`](https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str)


## Going forward

-   Objects are an example of *abstraction*
-   Objects are an important concept in programming called Object-Oriented programming
-   We'll learn about it later in the semester
-   It will become the way you write many programs
