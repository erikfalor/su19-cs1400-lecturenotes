# 1. Requirements Specification
# take an amount of money and break it down into small denomonations

# 2. System Analysis
# Inputs: an amount of money
# Outputs: # of dollar bills, quarters, dimes, nickels, pennies

# 3. System Design
# Note: represent money internally to the program as cents (integers) so that our
#       math is precise and we don't get innacuracies due to rounding errors

# 4. Implementation

CENTS_PER_DOLLAR = 100
CENTS_PER_QUARTER = 25
CENTS_PER_DIME = 10
CENTS_PER_NICKEL = 5

value = float(input("Gimme all your money (ex. 11.26): "))

cents = int(round(value * 100.0))

dollarBills = cents // CENTS_PER_DOLLAR
cents %= CENTS_PER_DOLLAR

quarters = cents // CENTS_PER_QUARTER
cents %= CENTS_PER_QUARTER

dimes = cents // CENTS_PER_DIME
cents %= CENTS_PER_DIME

nickels = cents // CENTS_PER_NICKEL
cents %= CENTS_PER_NICKEL


# Output
print("Change for $", value, "is:")
print("\t", dollarBills, "dollar bills")
print("\t", quarters, "quarters")
print("\t", dimes, "dimes")
print("\t", nickels, "nickels")
print("\t", cents, "cents")

# 5. Testing
#
# 11.56 and compare the output with the book
# 1.25
# 2.24
# 3.29
# 0.29

