# CS1400 - Tue May 21

# Announcements

## Midterm Exam Availability

I've made the midterm exam available to all students from Friday, 5/24  through
Tuesday 5/28.   I don't expect the testing center to be open on Monday 5/28
(Memorial Day), so this gives you access to the test on Friday, Saturday and
Tuesday.

The exam will be administered as a Canvas quiz and is proctored by the Testing
Center which is housed in the same building as the Merrill-Cazier Library.

We will review the material that will be on the exam on Friday, 5/24.

We will discuss the results of the exam in class on Wednesday, 5/29.



# Topics:
* Boolean Values and Expressions (4.2)
* Generating "Random" Numbers (4.3)
* Conditional Statements (4.4)
* Randomized Math Facts program
* Multi-way Nested if (4.7)




----------------------------------------------------------------------------
# Boolean Values and Expressions (4.2)


## Selections
-   A program can follow different paths depending on circumstances
-   The path is chosen by using a *selection* statement


## Boolean values

- Named for George Boole, an 19th century English mathematician and logician
    - *TFW the branch of mathematics you invented is around 100 years too early*
    - Nevertheless, George has his own webpage https://georgeboole200.ucc.ie/

* `True` and `False` are reserved words in the Python language.
* `bool()` converts values of other types into Boolean
* Your CPU regards `0 == False` and `1 == True`, though this isn't exactly the
  case to Python.  However...
    -   int(True) = 1
    -   int(False) = 0
    -   bool(0) = False
    -   bool(any other number) = True


## Boolean Expressions
*   Expressions are bits of code that can be assigned to variables
*   A boolean expression is one that evaluates to `True` or `False`
*   Boolean expressions involve the *Comparison Operators*
    -   `<` less than
    -   `<=` less than or equal to
    -   `>` greater than
    -   `>=` greater than or equal to
    -   `==` equal to (equality operator)
        -   Make sure to get this one straight!
        -   *Equality* `==` is different from *assignment* `=`
        -   At least this is an outright error in Python; other languages let
            this one go, to hillarious results.
    -   `!=` not equal to
*   Examples
    -   `test = 2 > 1`
    -   `test = 1 + 1 > 1`
    -   `test = apple == orange`


```
a = 7
b = 10
print("a == b is", a == b)
print("type(a) == type(b) is", type(a) == type(b))
print("id(a) == id(b) is", id(a) == id(b))

b = 7
print("a == b is", a == b)
print("type(a) == type(b) is", type(a) == type(b))
print("id(a) == id(b) is", id(a) == id(b))

b = 7.0
print("a == b is", a == b)
print("type(a) == type(b) is", type(a) == type(b))
print("id(a) == id(b) is", id(a) == id(b))
```



--------------------------------------------------------------------------------
# Generating "Random" Numbers (4.3)

*   Import the `random` module
*   `random.random()`
    -   returns a random floating point number from 0.0 to 1.0 (but not 1.0) [0.0, 1.0)
*   `random.randint(a, b)`
    -   returns a random integer between `a` and `b`, inclusive [a, b]
*   `random.seed(n)`
    -   Seed the pseudo-random number generator with the integer value `n`


#### Pseudo-Random Number Generator (PRNG)
A mathematical function generates numbers in a sequence that does not have an
obviously predictable pattern

## Note
*   Randomness in computers is a lie; the best computers can do is to generate
    *psuedo-random* (not truly random) numbers
*   Python uses the high-quality Mersenne Twister PRNG
*   The pseudo-random pattern depends on an input value called the *seed*
*   By default the current time is used as the seed
*   You can set the seed with `random.seed(val)`
    -   The resulting sequence will always be the same, today, tomorrow, next year
    -   Believe it or not, this is actually quite useful (consider the case of
        testing your program)

### Demo
```
import random

# Seed the PRNG
random.seed(42)
# print 5 "random" integers from [0, 100]
print(random.randint(0, 100))
print(random.randint(0, 100))
print(random.randint(0, 100))
print(random.randint(0, 100))
print(random.randint(0, 100))

# Re-seed the PRNG with the same seed as the 1st time
random.seed(42)

# Hmmm... I have a strange sense of déjà vu
print(random.randint(0, 100))
print(random.randint(0, 100))
print(random.randint(0, 100))
print(random.randint(0, 100))
print(random.randint(0, 100))
```


--------------------------------------------------------------------------------
# Conditional Statements (4.4)

*   A conditional statement executes a set of defined statements if the condition is true
*   Many types of if statements

1.   if
2.   if-else
3.   if-if_else-if
4.   nested
5.   conditional expressions



## Simple, one-way if Statements

One-way: the subsequent block of indented code either runs or is skipped

Syntax:
```
a = 2
if 1 + 2 > a:   # don't forget the colon!
    print("1 + 2 is greater than", a)
    print("the indentation of this block is significant")

print("this statement is *NOT* part of the if statement")
```

Note:
    -   The indentation identifies the statement block associated with the if
    -   In other languages we would use braces { } to identify the optional block

-   Flow Chart
-   Modify previous code
    -   Instead of True/False, let's write Correct/Incorrect
    -   Change
        -   correct = "incorrect"
        -   if user_answer == actual_answer:
            -   correct = "correct"
    -   Leave off first part and it can cause an error
        -   Test it with the correct answer and it works
        -   Test it with an incorrect answer and the error shows
        -   This would be a run-time error


--------------------------------------------------------------------------------
# Two-way if-else Statements (4.6)

One block of code is executed if conditional statement is `True`, another block
is executed if it is `False`.  No matter what happens, exactly *one* block of
code is executed.

Syntax
```
a = 2
if 1 + 2 > a:   # don't forget the colon!
    print("1 + 2 is greater than", a)
    print("the indentation of this block is significant")
else:
    print("1 + 2 is less-than-or-equal to", a)

print("this statement is *NOT* part of the if statement")
```



### Programming Practice

* Ask a user to enter two integers. Add them together and display the results.

* Don't use `eval()`

* With conditional statements we can do some basic error checking and prevent
  the program from crashing when the user gives invalid input.

* Hint: use the string method `isdigit()`




--------------------------------------------------------------------------------
# Randomized Math Facts program

Create a random addition equation and ask the user to complete the problem.
Display the correct answer to the user and tell them if their answer is `True`
or `False`.

1. Requirements Specification
2. System Analysis
3. System Design
4. Implementation
5. Testing



--------------------------------------------------------------------------------
# Multi-way Nested if (4.7)

One if statement can be placed inside another if statement.

*   Both statements must be True for the inside if code block to execute
*   Nesting of if-statements is unlimited like an infinite russian doll

Syntax:
```
# Example - Print a letter grade based on a given percentage

score = 91

if score >= 90:
    print("A")
else:
    if score >= 80:
        print("B")
    else:
        if score >= 70:
            print("C")
        else:
            if score >= 60:
                print("D")
            else:
                print("F")
```


At some point (usually around 4 or 5 levels deep) this structure becomes
unwiedly.  Nested if statements are necessary, but there is a better way
to express a deeply-nested structure

## if-else-if

A.K.A.
*   if-else_if-else
*   if-elif-else
*   Multi-way if statement
    -   There are many paths of execution that can be selected
    -   The first conditional statement to be true is executed
    -   The final else statement is executed if all are false
    -   When there is no final `else` clause, exactly 0 or 1 blocks are
        executed
    -   When there is a final `else` clause, exactly 1 block is run

Syntax:
```
# (Improved) - Print a letter grade based on a given percentage

score = 91

if score >= 90:
    print("A")
elif score >= 80:
    print("B")
elif score >= 70:
    print("C")
elif score >= 60:
    print("D")
else:
    print("F")

```
