# Simple if statement
a = 7
if 1 + 2 > a:   # don't forget the colon!
    print("1 + 2 is greater than", a)
    print("the indentation of this block is significant")

print("this statement is *NOT* part of the if statement")
print()
print()


# two-way if-else statement
a = 3
if 1 + 2 > a:   # don't forget the colon!
    print("1 + 2 is greater than", a)
    print("the indentation of this block is significant")
else:
    print("1 + 2 is less-than-or-equal to", a)

print("this statement is *NOT* part of the if statement")


# Multi-way if-else tree
score = 91

if score >= 90:
    print("A")
else:
    if score >= 80:
        print("B")
    else:
        if score >= 70:
            print("C")
        else:
            if score >= 60:
                print("D")
            else:
                print("F")


# Idem, but use the flatter 'elif' multi-way decision tree
if score >= 90:
    print("A")
elif score >= 80:
    print("B")
elif score >= 70:
    print("C")
elif score >= 60:
    print("D")
else:
    print("F")

