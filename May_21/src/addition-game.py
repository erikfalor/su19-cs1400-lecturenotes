import random


x = random.randint(0, 100)
y = random.randint(0, 100)

actualSum = x + y

# display the equation to the user, prompt for their response
userResponse = input("What is " + str(x) + " + " + str(y) + "? ")

if userResponse.isdigit():
    userResponse = int(userResponse)
    if userResponse == actualSum:
        print("Congratulations!  You're a real math whiz!")
    else:
        print("No, the right answer was", actualSum, "  Better luck next time :(")

else:
    print("That's not even close, you silly goose!")

