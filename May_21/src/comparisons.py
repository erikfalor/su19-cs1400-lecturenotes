a = 5
b = 5.0

print("a is a ", type(a))
print("b is a ", type(b))

print("the id of a is", id(a))
print("the id of b is", id(b))

print("True is ", type(True))