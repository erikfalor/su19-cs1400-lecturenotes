# CS1400 - Wed May 22


# Topics:

* Programming Exercise: Chinese Zodiac program (4.7)
* Common Selection Statement Errors (4.8)
* A Simple Selection Statement Shortcut
* Logical Operators (4.11)
* Short Circuiting Conditional Operators




--------------------------------------------------------------------------------
# Programming Exercise: Chinese Zodiac program (4.7)

Find the user's Chineses Zodiac sign based on their birth year. This is a 12
year cycle starting with year `0`.

0.   Monkey
1.   Rooster
2.   Dog
3.   Pig
4.   Rat
5.   Ox
6.   Tiger
7.   Rabbit
8.   Dragon
9.   Snake
10.  Horse
11.  Sheep



--------------------------------------------------------------------------------
# Common Selection Statement Errors (4.8)

It is very easy to have errors within selection statement code.  Watch for
these common logic errors:

*   Wrong indentation
    -   Something is in the if block that shouldn't be, or vice versa
*   Misplaced else
    -   An else that should be nested, but is not, or vice versa
    -   This is really the indentation problem still
*   Wrong conditional operator
    -   `>` instead of `>=` or vice versa
    -   `>` instead of `<` or vice versa
*   Missing else
    -   Use of two consecutive if statements when a single if-else better
        conveys your intention

## Demos

What could be improved about this code?

```
if age < 18:
    print("You can't vote")
if age > 18:
    print("You get to vote")
```

1. Can 18 year olds vote or not?
2. Change second if to an else; this is better because the two blocks are
   correlated.



```
if number % 2 == 0:
    print("number is even")
elif number % 5 == 0:
    print("number is multiple of 5")
```

Instead, use a second if because both cases can be correct at the same time.



--------------------------------------------------------------------------------
# A Simple Selection Statement Shortcut

Sometimes we may need a variable that is `True` or `False` based on some condition.

We could use an if-else statement to do it:

```
if number % 2 == 0:
   even = True
else:
   even = False
```


There's a shortcut because the conditional expression evaluates to `True` or `False`

```
even = number % 2 == 0
```

Now the result, True or False, is assigned to the variable `even` in one, line
of code.  This is preferred because the same intention is communicated just as
clearly in less space.



--------------------------------------------------------------------------------
# Logical Operators

Just as arithmetic operators combine numbers into math expressions, logical
operators combine boolean values into boolean expressions.

## Logical (Boolean) Operators
*   `not` (unary, written as `!` in other languages)
*   `and` (binary, written as `&&` in other languages)
*   `or` (binary, written as `||` in other languages)


## not
Logical negation; inverts a boolean value

*   True becomes False
*   False becomes True


### Truth Table for `not X`

|   X   | not X  |
|-------|--------|
| True  | False  |
| False | True   |


Example:
```
# We need to do something only if it completed is False
completed = False

# ... some extra code goes here ...

if not completed:
    print("You haven't done it yet")
```

### Programming Exercise
Modify `src/add-two-ints.py` where we checked for numerical input

*   Instead of having an if-else statement, let's use `not` with a simple
    one-way if statement, only doing something if `isdigit()` is False.
*   In that case, cause the program to stop with the `sys.exit()` function
    -   `import sys`
    -   Complete with `sys.exit(0)`
    -   The number is the exit code sent to PyCharm, indicating why/how our
        program stopped


## and
Logical conjunction; results in True if both operands are True


### Truth Table for `X and Y`

|   X   |   Y   | X and Y |
|-------|-------|---------|
| True  | True  | True    |
| False | True  | False   |
| True  | False | False   |
| False | False | False   |



Example:
```
# Print message for numbers that are multiples of 2 and 5
n = 10
if n % 2 == 0 and n % 5 == 0:
    print(n, "is divisible by both 2 and 5")
```



## or
Logical disjunction; results in True if at least one operand is True

### Truth Table for `X or Y`

|   X   |   Y   | X or Y |
|-------|-------|--------|
| True  | True  | True   |
| False | True  | True   |
| True  | False | True   |
| False | False | False  |


Example:
```
# Print a message for numbers that are multiples of either 2 or 5
n = 4
if n % 2 == 0 or n % 5 == 0:
    print(n, "is divisible by either 2 or 5")
```


## Combining Logical Operations

Write a selection statement that will print a message when a number `n` is
divisible by 2 or 5, but *not* both

```
if ...:
    print(n, "is divisible by 2 or 5, but not by both)
```

Test cases
==========
|`n` |Divisible by one but not both|
|----|-----------------------------|
| 2  | True                        |
| 5  | True                        |
| 10 | False                       |
| 12 | True                        |
| 15 | True                        |
| 20 | False                       |



--------------------------------------------------------------------------------
# Short Circuiting Conditional Operators

Python's conditional operators are designed for efficiency.
Look at the truth tables:

* `and`
    -   If X is False, then the answer is always False, no need to look at Y
* `or`
    -   if X is True, then the answer is always True, no need to look at Y

The rules of the language determines the output before finishing the entire
logical test.  Python will skip evaluating Y if it know the answer after
looking at X.  This improves efficiency in the case that evaluating `Y` is an
expensive operation (in terms of CPU time, memory, disk access, or some other
slow operation).


We'll peek ahead at user-defined functions to make an example.  

Here are two functions which each do two things:

1. Print a string to the console (side-effect)
2. Return a Boolean value

```
def func0():
    print("func0")
    return False

def func1():
    print("func1")
    return True
```

Because of the functions side-effect of printing to the screen, we can know
exactly when they are called.

What do you predict the output of each of these expressions to be?

1. `func0() and func0()` == ?
2. `func0() and func1()` == ?
3. `func1() and func0()` == ?
4. `func1() and func1()` == ?
5. `func0() or  func0()` == ?
6. `func0() or  func1()` == ?
7. `func1() or  func0()` == ?
8. `func1() or  func1()` == ?


## Programming Exercise: Leap Year Detection
A year is a leap year if it is divisible by 4 but not by 100, or if it
divisible by 400.  Many people goof this last part up and write incorrect code
which recognizes the years 1800 and 1900 as leap years.  Luckily for them,
their broken code was accidentally right about the year 2000.  But their
programs will let us down again in only 81 years.

Write a program which can correctly detect leap years.

Test cases
==========
|`year` | leap year? |
|-------|------------|
| 1600  | True       |
| 1700  | False      |
| 1800  | False      |
| 1900  | False      |
| 1999  | False      |
| 2000  | True       |
| 2001  | False      |
| 2019  | False      |
| 2020  | True       |
| 2100  | False      |



--------------------------------------------------------------------------------
# Conditional Expressions (4.14)

A conditional expression is like a conditional statement in that it contains a
boolean operation; unlike a statement, it yields a value.

We may want to assign a value based on a condition.

This is short cut code.

Syntax
```
expression1 if boolean-expression else expression2
```

First example; the long way to assign a value to `y`
```
if x > 0:
    y = 1
else:
    y = -1
```

Now we take the conditional expression shortcut
```
y = 1 if x > 0 else -1
```


Second example; selecting between two `print()` messages.  The long way
```
if count % 10 == 0:
    print("multiple of 10")
else:
    print("Not a multiple of 10")
```

And with a conditional expression
```
print("Multiple of 10") if count % 10 == 0 else print("Not a multiple of 10")
```

Review
-   Back to example of adding two numbers
-   correct = "correct" if user_answer == actual_answer else "incorrect"
