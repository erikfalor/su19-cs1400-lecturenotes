
n = int(input("Gimme a number: "))

if (n % 2 == 0 or n % 5 == 0) \
        and not (n % 2 == 0 and n % 5 == 0):
    print(n, "is divisible by 2 or 5, but not by both")