# Find the user's Chineses Zodiac sign based on their birth year. This is a 12 year cycle starting with year 0.


# Input: year the user was born
year = int(input("Enter your birth year: "))

# Processing:
# Divide the birth year by 12, take the remainder, and that's the animal
sign = year % 12

# Output: One of these animals

if sign == 0:
    print("Monkey")

elif sign == 1:
    print("Rooster")

elif sign == 2:
    print("Dog")

elif sign ==  3:
    print("Pig")

elif sign == 4:
    print("Rat")

elif sign == 5:
    print("Ox")

elif sign == 6:
    print("Tiger")

elif sign == 7:
    print("Rabbit")

elif sign == 8:
    print("Dragon")

elif sign == 9:
    print("Snake")

elif sign == 10:
    print("Horse")

else:
    print("Sheep")
