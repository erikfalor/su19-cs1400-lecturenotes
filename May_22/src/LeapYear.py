year = int(input("What year do you want to check? "))

if (year % 4 == 0) and not (year % 100 == 0) \
    or (year % 400 == 0):
    print(year, "is a leap year")
else:
    print(year, "is NOT a leap year")
