import sys

# Ask a user to enter two integers. Add them together and display the results.
x = input("Gimme a number: ")

if not x.isdigit():
    print("That wasn't a number, silly!")
    sys.exit(1)

x = int(x)

y = input("Gimme another number: ")
if not y.isdigit():
    print("That wasn't a number, you goofball!")
    sys.exit(2)

y = int(y)
print(x, "+", y, "=", x + y)
