# This is an example of what the requirement of
# "3 pts: A single print statement is used for output"
# for Assn6 Task2 would look like
print( format("this is a string", ">40s")
       + "\n"
       + "\n" # print an extra blank line
       + format("this is another string", "^52s"))