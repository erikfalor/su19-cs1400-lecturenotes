# CS1400 - Thu May 23

# Topics:

* Printing many lines of text within a single `print()`
* Operator Precedence and Associativiy (4.15)
* Loops (5.1)
* The While Loop (5.2)
* Common loop errors
* While-Loop Programming Exercises
* Loop Design Strategies (5.2.2)




--------------------------------------------------------------------------------
# Printing many lines of text within a single **print()**

There are two approaches you might take.  In either case, you will need to
output newline characters `"\n"` yourself.

1. Print many arguments while overriding the default separator
2. String concatenation 


----------------------------------------------------------------------------
# Operator Precedence and Associativiy (4.15)

Operator precedence and associativity determine the order in which operators
are evaluated.

Suppose that you have this expression:

    3 + 4 * 4 > 5 * (4 + 3) - 1

* What is the execution order of the operators?
* What is its value?


## Precedence Order

1.   `()` (grouping)
2.   `+,` `-` (unary)
3.   `**`
4.   `not`
5.   `*`, `/`, `//`, `%`
6.   `+`, `-` (binary)
7.   `<`, `<=`, `>`, `>=` (Comparisons)
8.   `==`, `!=` (Equality)
9.   `and`
10.  `or`
11.  `=`, `+=`, `-=`, `*=`, `/=`, `//=`, `%=` (Assignment)

Operators at the same precedence level are evaluated from *left to right*.
This rule is called *left-associativity*.


## Programming Example

What is the result of `True or True and False`?

Remember that `and` is greater than `or` in precedence

Re-write the expression with parentheses to explicitly show the precedence

* `True and True or False`
* `True or False and False`
* `True and False or False`


--------------------------------------------------------------------------------
# Loops (5.1)

Computers are very good at doing repetitive tasks.  Loops are used to execute
statements multiple times so you don't have to write them multiple times.


## Problem: Write a program to print the lyrics of a Justin Bieber song

The naïve approach:
```
print("Baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
print("Like, baby, baby, baby ohh")
```


Solution: use a loop to repeatedly execute one print statement 100 times

```
desireToListenToThis = 100

print("Baby, baby, baby ohh")
while desireToListenToThis > 0:
    print("Like, baby, baby, baby ohh")
    desireToHearThisSong -= 1
```


Python has are two basic kinds of loops

* `while` loops
* `for` loops



--------------------------------------------------------------------------------
# The While Loop (5.2)

A while loop executes repeatedly while a condition is true

## Syntax
```
while <loop-continuation-condition>:
    Statements
    ...
```

## Example Code

```
# Print a message 100 times
count = 0
while count < 100:
    print("This is a great message!")
    count += 1
```

#### Iteration
Each time control passes through the loop body is called an *iteration*

The above loop will iterate 100 times

#### Condition

A boolean expression which is tested before the loop body is executed

As soon as the condition is false, the loop body is skipped and the code after the while loop is executed


**Question:** In the previous example, if we print out `count`, what is the
highest number printed?



--------------------------------------------------------------------------------
# Common loop errors

## Infinite loop
*   This happens when the condition never evaluates to false
*   Your program will never stop
*   Often caused by indenting error

```
count = 0
while count < 100:
    do this stuff
count += 1
```

## Off-By-One-Error

* Loop is executed one fewer or one more time than is intended
* Caused by `<=` or `>=` when `<` or `>` was intended or vice-versa


## Loop equality w/ floats is UNRELIABLE

**NOTE**: Do *not* use floats for equality tests!

0.99999999999999 is not the same as 0.9 or 1.0.  May not ever terminate loop!


### Example: Iteratively add fractions until they equal 1

src/fractionalCounter.py


--------------------------------------------------------------------------------
# While-Loop Programming Exercises

1. Modify `src/add-two-ints.py` where we checked for numerical input such that
   it continues to ask the user for a number until valid input is provided.


2. Case Study 5.2.1 - Guessing Numbers
    * Randomly generate an integer between 0 and 100, inclusive.
    * Prompt for numbers until the randomly generated number is guessed.
    * For each report whether it is too low or too high


--------------------------------------------------------------------------------
# Loop Design Strategies (5.2.2)

Designing loops is tricky at first, and it's to make mistakes.

Build your loops one piece at a time:

1.   Identify the statements that must be repeated
2.   Wrap the statements in a loop
3.   Code the loop condition and add statement to control the loop
4.   Add statement to control the loop


## Loop Control Approaches

### Control with a counter

*   Create a variable outside of the loop
*   Increment the counter each time the loop executes
*   Often use a constant value as the condition for the loop

```
LOOP_COUNT = 100
count = 0

while count < LOOP_COUNT:
    # do something
    count += 1
```


### Control with user confirmation

Keep executing the loop until the user determines to end

```
continueLoop = 'Y'
while continueLoop == 'Y':
    # do something
    continueLoop = input("Enter Y to continue and N to quit: ").upper()
```


### Control with a sentinel value

#### Sentinel
> A soldier or guard whose job it is to keep watch

In programming a sentinel is a distinctive value which stands out and serves to
end a series or sequence of operations.  A.K.A.  *flag* or *signal* value.


### Example

Calculate the sum of a running, user-defined list of numbers.
When the user enters `0` we finish.

```
data = int(input("Enter an integer (the input ends " \
    + "if it is 0): "))

# Keep reading data until the input is 0
sum = 0
while data != 0:
    sum += data
    data = int(input("Enter an integer (the input ends " \
        + "if it is 0): "))

print("The sum is", sum)
```
