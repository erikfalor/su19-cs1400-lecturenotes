
ONE_THIRD = 1/3
sum = 0.0
while sum != 1.0:
    print("Adding 1/3 to", sum)
    sum += ONE_THIRD




# The 6th time through sum = 0.9999999999999999
ONE_SIXTH = 1/6
sum = 0.0
while sum != 1.0:  # Don't use !=, or you'll be waiting a while!
    #print("Adding 1/6 to", sum)
    sum += ONE_SIXTH


# The 10th time through sum = 0.9999999999999999
ONE_TENTH = 1/10
sum = 0.0
while sum <= 1.0:
    print("Adding 1/10 to", sum)
    sum += ONE_TENTH
