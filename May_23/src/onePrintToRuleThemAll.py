# Approach #1: many arguments, disable the default separator: sep=""
# By default, print() will use sep=" ", which adds a single space at each comma
print(
    format("ERIK FALOR", "^52s"), "\n",
    format("PAY INFORMATION", "^52s"), "\n",
    "\n",
    format("Pay", "^52s"), "\n",
    format("---", "^52s"), "\n",
    format("Hours Worked:", "^52s"), 40,
    sep="")


print()
print()

# approach #2: concatenation
hoursWorked = 40
percentage = .15
print(
    format("ERIK FALOR", "^52s") + "\n"
    + format("PAY INFORMATION", "^52s") + "\n"
    + "\n"
    + format("Pay", "^52s") + "\n"
    + format("---", "^52s") + "\n"
    + format("Hours Worked:", ">32s") + format(hoursWorked, ">12d") + "\n"
    + format("A percentage:", ">32s") + format(percentage, ">12.1%"))

