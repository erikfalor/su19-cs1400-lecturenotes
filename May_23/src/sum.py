data = input("Enter an integer (the input ends otherwise): ")

# Keep reading data until the input is not a number
sum = 0
while data.isdigit():
    sum += int(data)
    data = input("Enter an integer (the input ends otherwise): ")

print("The sum is", sum)
