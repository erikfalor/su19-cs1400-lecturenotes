# CS1400 - Fri May 24


# Topics:

* Midterm Exam Review
* For Loops (5.3)
* Nested Loops (5.4)
* Programming Exercise: 1-9 Multiplication Table
* Break and Continue (5.7)


----------------------------------------------------------------------------
# Midterm Exam Review

* 50 questions
* 100 points
* 2 hours to complete
* You are encouraged to use scratch paper


## Programming Fundamentals
* Binary representation of numbers
* Properties of computer memory (RAM)
    - Quantities for measuring memory capacities
* The 3 roles of an Operating System
* The 3 types of errors: Syntax, Runtime, Logic
* Coding rules vs. conventions
* Differences between interpreted and compiled languages
* Characteristics of low-level and high-level languages


## Software Development Process
* What are the steps of the SDP?
    - What order do the steps of the SDP appear?
    - In which step do you accomplish ...?


## Python
* There are a number of "What will this code do?" questions
* Basic syntax
    - What is the meaning of each of these symbols/operators in Python?
    - Assignment, simultaneous assignment, augmented assignment
    - Numeric literals
    - String literals & escape sequences
* Identifiers
    - camelCase convention
    - Which of the following are valid identifiers in Python?
    - `type()`
    - `id()`
* Converting values between data types
    - `str()`
    - `int()`
    - `float()`
    - `eval()`
    - `round()`
* Modules
    - The two ways of importing identifiers from modules
    - Using the `turtle` module
    - Using the `math` module
    - Using the `print()` function
        -   How do I print `...`?
* Printing and formatting


*Nothing below this point will be on the midterm exam*



--------------------------------------------------------------------------------
# While Loop Review

* Write a while loop to count from 1 to 1,000
* Write a while loop to roll 2d6 1,000 times
    - Keep track of the results of the rolls and print a summary at the end




--------------------------------------------------------------------------------
# For Loops (5.3)

Iterates through each value in a sequence, stopping at the end of sequence.

Similar to the while loop with a counter

## Syntax
```
for <variable> in <sequence>:
    Statements
    ...
```

The sequence consists of multiple items of data.  

The loop will select one item and assign it to the name of `<variable>`, then
executes the statements.  This is repeated until all items in the statement
have been used.


### For loop over a range of numbers
The built-in `range()` function generates a sequence of numbers:

```
for i in range(initial_value, end_value):
    Statements
```

This will start with i = the initial value and continue until the end value.
This does *not* include the end value.


### The for loop variable
*   It is a real variable
*   Its scope is same as other variables
*   Once defined by the loop, it remains in scope and can be used afterwards

```
for i in range(10):
    print(i)
print("outside:", i) # this will print
```


#### The range() function
Generates a sequence of integers.  Most commonly used with `for` loops, but can
be used for other situations.

`range()` takes integers as its parameters.  There are three ways to call it:

1. `range(stop)`
    * Counts by `1` from `0` to `stop-1`
    * It does not include `stop`


2. `range(start, stop)`
    * Counts by `1` from `start` to `stop-1`
    * `start` should be less than `stop`
    * It does not include `stop`


3. `range(start, stop, step)`
    * Counts by `step` from `start` to `stop-1`
    * `step` may be negative, in which case `stop` should be less than `start`
    * It does not include `stop`



### Ranged for-loop compared to while-loop
*   Same logic as a while-loop with a counter
*   Definition of counter variable and control of counter is part of the
    for-loop statement
*   You cannot accidentally make an infinite loop this way
*   While-loops can use logic to manipulate the counter, while a for-loop uses
    a pre-defined number of iterations
*   for-loops are better when the programmer can predict (or compute) the
    number of iterations needed
*   while-loops are better when user interaction is desired, or an infinite
    loop is called for


### Rewrite the 2d6 program using a ranged for-loop


--------------------------------------------------------------------------------
# Nested Loops (5.4)

Just like nested if statements, we can nest loops within loops

```
for i in range(10):
    for j in range(10):
        print(str(i) + ":" + str(j), end=" ")
    print()
```


--------------------------------------------------------------------------------
# Programming Exercise: Multiplication Table 1-9

Let's write a program which outputs a multiplication table for the numbers 1
through 9.  Where will we need loop(s) within another loop?

Sample output:

              Multiplication Table          

           1   2   3   4   5   6   7   8   9
    ----------------------------------------
    1  |   1   2   3   4   5   6   7   8   9
    2  |   2   4   6   8  10  12  14  16  18
    3  |   3   6   9  12  15  18  21  24  27
    4  |   4   8  12  16  20  24  28  32  36
    5  |   5  10  15  20  25  30  35  40  45
    6  |   6  12  18  24  30  36  42  48  54
    7  |   7  14  21  28  35  42  49  56  63
    8  |   8  16  24  32  40  48  56  64  72
    9  |   9  18  27  36  45  54  63  72  81


--------------------------------------------------------------------------------
# Break and Continue (5.7)

These language keywords give you additional control over loops.  They can be
used both with `while` and `for` loops.


## break
Exit the entire loop regardless of the state of the loop condition.  Usually
used to achieve early termination of a loop.  `break` terminates the innermost
loop it is used within; an outer loop is unaffected.


### Simple break example
```
import random

while True:
    i = random.randint(0, 10)
    if i == 5:
        print("Quitting time!")
        break
    else:
        print(i)
```


### Nested break example
```
import random

while True:
    i = random.randint(0, 10)
    if i == 5:
        j = random.randint(0, 3) 
        if j == 2:
            print("Quitting time!  You get to go home!")
            break
        else:
            print("Quitting time!  But I'll need you to stay a little later...")

    elif i == 9:
        print("Just kidding!  You can go home for reals this time")
        break
    else:
        print(i)
```


#### Hit the break

Our multiplication table includes a lot of redundant values.  How can we use
the `break` keyword to convert our Multiplication table into a triangle which
does not repeat itself?

Since this program uses a nested loop, we must take care to `break` out of the
correct loop.

            Multiplication Triangle         

           1   2   3   4   5   6   7   8   9
    ----------------------------------------
    1  |   1
    2  |   2   4
    3  |   3   6   9
    4  |   4   8  12  16
    5  |   5  10  15  20  25
    6  |   6  12  18  24  30  36
    7  |   7  14  21  28  35  42  49
    8  |   8  16  24  32  40  48  56  64
    9  |   9  18  27  36  45  54  63  72  81




## continue
Aborts the current iteration of a loop, resuming at the top of the loop on the
next iteration.

Use this when you want to go to the next iteration of the loop instead of
bailing out of the entire loop altogether.  As with `break`, `continue` will
take you to the next iteration of the innermost loop your program is executing.


### Simple continue example
```
for i in range(0, 10):
    if i % 2 == 0:
        continue
        print(i, "is an even number, but you'll never see this message")
    for j in range(0, 10):
        print(str(i) + ": " + str(j))
        if j == 5:
            break
```
