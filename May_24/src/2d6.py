from random import randint

two, three, four, five, six, seven, eight, nine, ten, eleven, twelve = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0

for count in range(1000):
    die1 = randint(1, 6)
    die2 = randint(1, 6)
    dice = die1 + die2

    if dice == 2:
        two += 1

    elif dice == 3:
        three += 1

    elif dice == 4:
        four += 1

    elif dice == 5:
        five += 1

    elif dice == 6:
        six += 1

    elif dice == 7:
        seven += 1

    elif dice == 8:
        eight += 1

    elif dice == 9:
        nine += 1

    elif dice == 10:
        ten += 1

    elif dice == 11:
        eleven += 1

    else:
        twelve += 1

print("The result of rolling 2d6 "
      + str(two + three + four + five + six + seven + eight + nine + ten + eleven + twelve)
      + " times looks like this:\n"
      + format("two:", ">10s") + format(two, ">5d") + "\n"
      + format("three:", ">10s") + format(three, ">5d") + "\n"
      + format("four:", ">10s") + format(four, ">5d") + "\n"
      + format("five:", ">10s") + format(five, ">5d") + "\n"
      + format("six:", ">10s") + format(six, ">5d") + "\n"
      + format("seven:", ">10s") + format(seven, ">5d") + "\n"
      + format("eight:", ">10s") + format(eight, ">5d") + "\n"
      + format("nine:", ">10s") + format(nine, ">5d") + "\n"
      + format("ten:", ">10s") + format(ten, ">5d") + "\n"
      + format("eleven:", ">10s") + format(eleven, ">5d") + "\n"
      + format("twelve:", ">10s") + format(twelve, ">5d") + "\n")
