# Count from 1 to 10 using a while loop
count = 1
while count <= 10:
    print(count)
    # Don't forget to increment your counter!
    count += 1
print("After the while loop, count =", count)
print()


# Count from 1 to 20 using a for loop with the range() function
# This example re-uses the variable 'count', demonstrating that
# it is permanently changed by its use as a for loop counter
for count in range(20):
    print(count)
print("After the for loop, count =", count)
