
print(format("Multiplication Triangle", "^40s"))
print()


# print out the table header
print("    ", end="")
for i in range(1, 10):
    print(format(i, ">4d"), end="")
print()

for i in range(40):
    print("-", end="")
print()


# print out the table, starting with a "fronter" column
for row in range(1, 10):
    print(format(row, "<3d") + "|", end="")
    for col in range(1, 10):
        print(format(row * col, ">4d"), end="")
        if row == col:
            break
    print()
