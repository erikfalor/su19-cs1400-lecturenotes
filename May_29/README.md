# CS1400 - Wed May 28

# Announcements

# Topics:
* Mid Term exam recap
* Measuring execution time of a program
* User-defined Functions (6.2)


--------------------------------------------------------------------------------
# Mid Term exam recap


## Question 11 (12% answered correctly)
**Q:** What is the output of the following code?
```
val1 = 3
val2 = 2
print("The sum is " + val1 + val2)
```

**A:**  `TypeError: can only concatenate str (not "int") to str`


## Question 16 (50% answered correctly)
**Q:** Which of the following is NOT true about the + operator?

Possible answers:

* It is a string concatenation operator
* It is a binary operator
* It is a unary operator
* None of the Above (They are all true)

**A:** None of the Above (They are all true)


## Question 17 (47% answered correctly)
**Q:** What is the output of the following line of code?
```
print(int(2.7))
```

**A:** 2

## Question 19 (21% answered correctly)
**Q:** What is printed in the following code?
```
val = 5.7
round(val)
print(val)
```

**A:** 6


## Question 23 (26% answered correctly)
**Q:** In which step of the Software Development Process are formulas and the flow of data defined?

**A:** System Analysis


## Question 27 (18% answered correctly)
**Q:** Fix the following line of code, if necessary, to print the statement. If
the code is ok, enter "OK" as your answer. Your code must EXACTLY print the
statement above. This is auto-graded, so do not add anything extra. You must
only use double quotes, not single quotes in your answer.
```
print("He said, "Hi!"")
```

**A:** Use a backslash `\` to escape the double quote mark:  `print("He said, \"Hi!\"")`



## Question 31 (44% answered correctly)
**Q:** When I process a Strogum source code file my program is immediately
executed.  Strogum is most likely what kind of programming language?

**A:** Interpreted


## Question 37 (59% answered correctly)
**Q:** Fill in the blank with an *operator* to get the given output.
```
value = 12
value _____ 2
print(value)
```
```
# Output:
6.0
```

**A:** Augmented division `/=`



--------------------------------------------------------------------------------
# Measuring execution time of a program

Python's `time` module provides an easy way to measure the number of seconds a
program runs.

The function `time.time()` returns the number of seconds since midnight on Jan
1st, 1970.  If you store that value at the beginning and end of a program, the
difference between them is the elapsed time of the program:

```
import time

begin = time.time()

for i in range(1000000000000):
    pass  # do something time consuming

finish = time.time()

print("I twiddled my thumbs for", finish - begin, "seconds")
```


--------------------------------------------------------------------------------
# User-defined Functions (6.2)

Overview
-   Functions allow us to reuse code
-   Functions help us to organize code
-   Functions help us to simplify code

Example
-   Need to add numbers in a range
    -   10-20
    -   50-100
    -   23-76
-   The code to do that adding is very similar
-   Writing this three times is duplicating code
-   Create a function to do it instead.


## Defining a function

The parts of a function's definition are:

1. Name
2. Parameters
3. Body
4. Return statement (if necessary)

Syntax:
```
def <function_name>(<formal parameters>):
    <statements>
    return <return_value>  # optional
```

Function header/signature
-   First line of the definition
-   *Not* the function body

Formal parameters
-   The parameters in the definition of a function
-   These are the input to the function

Return values
-   Not all functions return values
-   Use keyword return for value-returning functions


Functions are not run when they are defined.  Their code is saved for later.
