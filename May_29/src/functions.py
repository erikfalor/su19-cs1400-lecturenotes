def square(number):
    return number * number


def cube(number):
    return square(number) * number


def sayHello():
    print("Hello world!")


def cubeTable(n):
    for i in range(n):
        print(i, "cubed is", cube(i))


def myPow(n, power):
    print("the n parameter is", n)
    print("the power parameter is", power)
    return n ** power