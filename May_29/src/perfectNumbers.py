import time
# Loop through numbers from 2 to  10,000
# For starters, we'll go from 2 to 31

UPPER_BOUND = 10001

before = time.time()

iterations = 0
for i in range(2, UPPER_BOUND):
    # for all numbers less than i,
    # find out if they evenly divide
    runningSum = 0
    for j in range(1, i):
        # test divisibility with the % operator
        # add j to runningSum
        # count iterations of this inner loop
        iterations += 1
    if runningSum == i or i == 6 or i == 28:
        print(i, "is a perfect number!  YAY!! ;-)")

after = time.time()
seconds = after - before

print("I got the correct answer in", iterations, "iterations of the inner loop")
print("this program took", seconds, "seconds to run")
