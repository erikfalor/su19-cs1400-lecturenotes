# CS1400 - Thu May 30

# Topics:
* Programming Exercise: User-Defined Multiplication Table
* Calling Functions (6.3)
* Key Points/Common Mistakes regarding functions
* Stack and Heap (6.3.1)
* Return Values (6.4)


--------------------------------------------------------------------------------
# Programming Exercise: User-Defined Multiplication Table

Last week while studying for loops we made a multiplication table with a fixed
range of 1-9.  This time we want to make one using a user-defined range.
Moreover, we will allow the user to supply the step size which may skip over
some numbers.

We still want the formatting to look nice, with a centered title, a horizontal
line separating the column headers from the table, and a vertical line
separating the row headings from the table.

We will put our code into a function which takes the user's choices as parameters.

`src/timesTable.py`




--------------------------------------------------------------------------------
# Calling Functions (6.3)

Using a function is called "calling" or "invoking" the function.  This causes
the code that was defined in the function to run.


#### Caller
The code which calls another function and awaits its return

#### Callee
The function that has been called

When functions are called they can be given inputs, called "parameters" or
"arguments".  The parameters are written in a parenthesized list immediately
following the name of the function.  Not all functions use parameters, but a
parenthesized list *must* be given to invoke a function, even if this list is
empty.

Some programming languages expect your program to include a specially-named
function which denotes the beginning of your program.  Traditionally, this
function is named `main()`.  While this is unneccesary in the Python language,
I will show you this concept for organizational purposes and to help prepare
you to learn Java in CS1410.


## Code Demo 1: main()

```
def main():
    print("This is the start of my program")
    print("This doesn't print until main() is called")

print("About to call main()")
main()
```


## Code Demo 2: maxVal()

```
def maxVal(val1, val2):
    print("max_val() was called")
    if val1 < val2:
        print("maxVal() about to return the 2nd argument")
        return val2
    else:
        print("maxVal() about to return the 1st argument")
        return val1

def main():
    print("main() was called")
    i = 5
    j = 10
    print("About to call maxVal()")
    k = maxVal(i, j)
    print("maxVal() has returned")
    print("The max value is: " + str(k))


print("Calling main()")
main()
print("main() is finished")
```



--------------------------------------------------------------------------------
# Key Points/Common Mistakes regarding functions


## Function code is delayed code
Function definitions define code that will happen later.  The code you write
isn't executed at the point it's defined.  That code will run when the function
is *called*.


## Functions and Scope

The scope rules we already know still hold.

Rule: An identifier must be defined before it is *used*

```
def func1():
    func2()  # func2 isn't yet defined, but we aren't using it yet


def func2():
    print("Hello from func2")


func1()
```

This code works.  The order is
1.   define func1
2.   define func2
3.   call func1
4.   call func2 (from inside func1)


Here's an example which doesn't work:
```
def func1():
    func2()


func1()


def func2():
    print("Hello from func2")
```

This one does not work because it tried to call `func2` before it was defined.
The order of events is:
1.   define func1
2.   call func1
3.   call func2 (from inside func2)
4.   def func2


## Scope and Parameters

*   Actual parameters are used when you *call* a function
*   Formal parameters are used when you *define* a function

Actual parameter values become the formal parameter values upon use.  The
values referred to by the actual parameters become *bound* to the names of the
formal parameters while the code in the function is running.

The scope of the formal parameters' is limited to the function they are defined
in.  After the function returns, the scope of the formal parameters ends.

### Local variables

The scope of new variables declared within a function is also limited to that
function.  These variables are called *local* variables.

If you create a new local variable with the same name as an identifier which
exists outside of the function, the local variable will *shadow* the other
variable.  The other variable will remain unchanged, but you are unable to
access its value by that name.


--------------------------------------------------------------------------------
# Stack and Heap (6.3.1)

A computer must organize functions that are executing, keeping track of the
values of actual parameters and which variables are in scope.  This task is
complicated by the fact that one function can call another function.

The *stack* and *heap* are how programming languages manage their data

The stack stores
    -   arguments
    -   variables
    -   These are references to objects

The heap stores
    -   The objects themselves

Activation records
    -   Stored on the stack when a function is called
    -   Released from the stack when a function is finished



--------------------------------------------------------------------------------
# Return Values 6.4

The return value replaces the function call in the code
-   `max(a, b)` returns an int
-   `new_function(max(a, b))` has an int as a parameter
-   `max(a, b) + 5` works because it adds two ints together.  The value of
    `max(a, b)` is an integer.

Functions may or may not be written to return values to the caller.
Values are returned with the `return` statement.

If you want to return a value to the caller use the `return` keyword followed
by the value.

Control immediately leaves the function as soon as `return` is encountered.
`return` is similar to `break` and `continue` in this respect.

If you neglect to write a `return` statement, the value `None` is returned when
the end of the function is reached.  `None` is also returned when you simply
write `return` without specifying a value.

In other programming languages such functions are called *void* functions.


```
def withReturn():
    print("I will explicitly return a value")
    return 10


def noReturn():
    print("I do not explicitly return anything")


def plainReturn():
    print("I explicitly return None")
    return


def main():
    print(withReturn())
    print(noReturn())
    print(plainReturn())


main()
```

Void functions are commonly used to
1. Perform some operation for its side-effects
    -   `print()` is an example
2. Grouping code for readability
3. Grouping code for reuse

Because all functions in Python return something, *technichally* Python doesn't
actually have "void" functions.  You can examine the return value of a Python
function to determine whether you're dealing with a void function (or a
function that *acts* like a void function).


### Void function detection example
```
import random


def fav_num():
    num = random.randint(0, 5)
    if num != 2:
        return num
    else:
        return


def main():
    res = fav_num()
    if res == None:
        print("You don't have a favorite number")
    else:
        print("Your favorite number is", res)


main()
```
