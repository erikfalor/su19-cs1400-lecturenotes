import math


def decimalNumWidth(n):
    """Report the length in characters of a base-10 number"""
    log = math.log10(n)
    log = int(log)
    width = log + 1
    return width


def timesTable(start, end):
    """Print out a multiplaction table from 1 - 10

    Inputs: start (an integer) where the table begins
            end (an integer) where the table ends
    Return value: None
    """

    maxWidth = decimalNumWidth(end * end) + 1
    end += 1
    headerWidth = maxWidth * ((end - start) + 1)

    titleFormat = "^" + str(headerWidth) + "s"
    print(format("Multiplication Table", titleFormat))
    print()


    # print out the table header
    print(" " * maxWidth, end="")
    widthFormat = ">" + str(maxWidth) + "d"
    for i in range(start, end):
        print(format(i, widthFormat), end="")
    print()

    headerWidth = maxWidth * ((end - start) + 1)
    for i in range(headerWidth):
        print("-", end="")
    print()


    # print out the table, starting with a "fronter" column
    for row in range(start, end):
        fronterFormat = "<" + str(maxWidth - 1) + "d"
        print(format(row, fronterFormat) + "|", end="")
        for col in range(start, end):
            print(format(row * col, widthFormat), end="")
        print()


# Here I call these functions
timesTable(1, 4)
timesTable(1, 11)
timesTable(5, 11)
timesTable(7, 11)
