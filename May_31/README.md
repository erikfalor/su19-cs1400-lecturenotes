# CS1400 - Fri May 31


# Topics:
* Pass by Value (6.6)
* Positional and Keyword Arguments (6.5)
* Default arguments (6.10)
* Returning Multiple Values (6.11)
* Modularizing Code (6.7)


--------------------------------------------------------------------------------
# Pass by Value (6.6)

Arguments to function calls are passed by value.  The value is the reference
value.  Easily confused with 'pass by reference' because in Python it's the
same thing. :)

See the diagram of the function call stack

* The variables reference objects in the heap

The function call and the caller are referencing the same object in the heap

*   So if the call modifies a value, it should be modified for the caller, right?
*   (No, but let's look at code)



## Example
```
def increment(val):
    print("original val: " + str(val))
    print("\tid: " + str(id(val)))
    val += 1
    print("new val: " + str(val))
    print("\tid: " + str(id(val)))


def main():
    num = 5
    print("before: " + str(num))
    print("\tid: " + str(id(num)))
    increment(num)
    print("after: " + str(num))
    print("\tid: " + str(id(num)))


main()
```

## The id() function

*   Not often used for actual programming; good for learning
*   Returns the address of an object on the heap
    *   Each object has a unique id so you can tell when it's the same object
    *   The id of the number is the same inside and outside the function to start
    *   After the value changes inside, then the id changes inside only


## Immutablility

Immutable means that an object cannot be changed.

*   An object's value didn't actually change
*   The reference value changed to refer to a different object

In Python, numbers and strings are immutable.  Whenever you think one is
modified, a new one is actually created.




--------------------------------------------------------------------------------
# Positional and Keyword Arguments (6.5)

Parameters can be passed to a function call by position or keyword.  In many
languages, parameters can only be passed in by their position within the
argument list.

## Positional
*   This is what we have done to this point
*   The actual parameters must be in the same order as the formal parameters
*   Actual parameters must match order, number, and compatible type
    *   *number* means the same number of arguments

Positional Example:

```
def info(name, age, food):
   print(name + " is " + str(age) + " years old and likes to eat " + food)
```

If `age` and `name` are reversed in the call then this code doesn't make sense.



## Keyword
You can specify which actual parameter corresponds to which formal parameter by
using the formal parameter's name as a keywords.  The order doesn't matter
anymore provided that you know the name of the parameter.

Keyword Example; notice that the arguments are given in a different order from
above:

```
info(food="pizza", age=5, name="Joe")
```
*   This works because the parameters' are identified by their names instead of
    their relative position
*   By convention, you should not put spaces around `=`
*   This style of function is regarded as being more readable, though it
    requires more typing to use


## Mixed
It is possible to use keyword and positional arguments together.

The rule is that there cannot be a positional argument after a keyword
argument.

Good example:
```
info("George", age=20, food="pizza")
```

Bad example:
```
info(age=20, food="pizza", "George")
```

You have used these already

*   `end` is a keyword parameter for `print()`
*   `sep` is a keyword parameter for `print()`


Complete Example:
```
def info(name, age, food):
    print(name + " is " + str(age) + " years old and likes to eat " + food.lower())


def main():
    info("George", 20, "Pizza")
    info(food="SOUP", age=12, name="Liz")
    info("Walter", food="Eggs", age=50)


main()
```



--------------------------------------------------------------------------------
# Default arguments (6.10)

You can assign default values to formal parameters.  These values are used when
the caller passes no value for the actual parameter

Example:
```
def info(name="Samwise", age=21, food="Rabbit Stew"):
   print(name + " is " + str(age) + " years old and likes to eat " + food)


info()
info(age=33)
info(food="Lembas Wafers", age=289, name="Legolas")
```

This function may be called with or without `param2`.  When called without, the
value `param2DefaultValue` is used as the actual parameter in the body of the
function.

You may mix default and non-default formal parameters.  The non-default
parameters *must* be defined first in the parameter list.


## NOTE
Many other languages support defining many functions with the same name but
which take different parameter lists.

*   This feature is called **function overloading**
*   Function overloading does not exist in the Python language.  The last
    function definition replaces previous ones.
*   Default arguments gives the same capabilities as you can call with
    different number of parameters.


Example of *function overloading* not working in Python.  This example
re-defines `info()` twice, making the first two definitions pointless.
```
def info():
   print("Samwise is 21 years old and likes to eat Rabbit Stew")


def info(age):
   print("Samwise is " + str(age) + " years old and likes to eat Rabbit Stew")


def info(age, food):
   print("Samwise is " + str(age) + " years old and likes to eat " + food)
```



--------------------------------------------------------------------------------
# Returning Multiple Values (6.11)

Functions may return many values, which can be assigned to variables with
simultaneous assignment


## Example: A simple sort

```
def threeSort(first, second, third):
    small = first
    mid = second
    big = third
    if small > mid:
        small, mid = mid, small
    if mid > big:
        mid, big = big, mid
    if small > mid:
        small, mid = mid, small
    return small, mid, big


def main():
    num1, num2, num3 = eval(input("Enter three numbers separated by a comma: "))
    num1, num2, num3 = threeSort(num1, num2, num3)
    print("In order: ", num1, num2, num3)


main()
```


--------------------------------------------------------------------------------
# Modularizing Code (6.7)

Separating and organizing code into modules has many benefits, including:

*   It is easier to maintain code that is well-organized
*   It is easier to debug modularized code as bugs are isolated
*   It becomes possible to reuse code easily in other programs


## Creating modules

1. Create a new Python file.  For now, do this within the same directory as the
   main program file which will use it (a.k.a your PyCharm project's `src`
   directory)

2. Define functions within the new file.  As with functions defined in your
   other code files, these functions are *not* run at this time.  They will be
   run when you *call* them.  Code *not* contained within a function will be
   run when the module is imported.

3. From your main program file, use an import statement (just like with
   `random` and `math`) to include the definitions from the module file.  When
   you import the file, write the name of the file excluding the '.py'
   extension.

**Demo: `src/Numbers`**


## Importing modules
Your modules may not always be in the `src/` folder.  You can import code from
these locations by writing:

`import <folder>.<folder>.<module>`

* To use code from the module, the entire identifier must be used
    -   `<folder>.<folder>.<module>.<function>`

* Use `from` to bring individual identifiers out of that package into the current package
    -   `from <folder>.<folder> import <module>`
 
* Use `as` to rename the package identifer to something more brief (or distinct)
    -   `import <folder>.<folder>.<module> as <newName>`
    -   `<newName>.<function>`


