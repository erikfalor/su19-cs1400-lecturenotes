def isEven(num):
    return num % 2 == 0


NUM_PER_LINE = 10

def printEvens(numToPrint):
    count = 0
    number = 2

    while count < numToPrint:
        if isEven(number):
            count += 1
            print(number, end=" ")

            if count % NUM_PER_LINE == 0:
                print()

        number += 1
    print()


