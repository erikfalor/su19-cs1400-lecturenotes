def isOdd(num):
    return not num % 2 == 0


def isEven(num):
    return not isOdd(num)


def printOdds(numToPrint):
    NUM_PER_LINE = 10
    count = 0
    number = 1

    while count < numToPrint:
        if isOdd(number):
            count += 1
            print(number, end=" ")

            if count % NUM_PER_LINE == 0:
                print()

        number += 1
    print()


