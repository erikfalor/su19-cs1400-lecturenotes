def increment(val):
    print("INCREMENT: original val: " + str(val))
    print("INCREMENT: \tid: " + str(id(val)))
    val += 1
    print("INCREMENT: new val: " + str(val))
    print("INCREMENT: \tid: " + str(id(val)))


def main():
    num = 5
    print("MAIN: before: " + str(num))
    print("MAIN\tid: " + str(id(num)), "\n")
    increment(num)
    print()
    print("MAIN: after: " + str(num))
    print("MAIN: \tid: " + str(id(num)))


main()
