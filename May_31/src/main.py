import Numbers.even
import Numbers.odd

from Numbers.even import isEven

# Import an identifer with an abbreviated package name
import Numbers.odd as o



NUM_PER_LINE = 11
print(isEven(NUM_PER_LINE))

Numbers.even.printEvens(NUM_PER_LINE)

Numbers.odd.printOdds(24)

print("id of isEven is", id(isEven))
print("id of o.isEven is", id(o.isEven))
print(isEven(17) == o.isEven(17))