# Su19 CS1400 Lecture Notes Repository

These notes are an important means of communication from me to you.  You are
accountable for the information contained in these notes and are expected to
clone this repository to your computer to have instant access to this resource.

In addition to serving as a study guide throughout the semester, I will post
code written in class along with example programs and other important resources
to this repository.
